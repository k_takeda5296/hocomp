# coding: utf-8
#
# rc1.py : 適応型レンジコーダ
#
#          Copyright (C) 2007 Makoto Hiroi
#
import time, sys, getopt, os.path
from rangecoder import *

# 出現頻度表
class Freq:
    def __init__(self, size):
        self.size = size
        self.count = [1] * size
        self.sum = size

    # 出現頻度表の更新
    def update(self, c):
        self.count[c] += 1
        self.sum += 1
        if self.sum >= MIN_RANGE:
            n = 0
            for x in xrange(self.size):
                self.count[x] = (self.count[x] >> 1) | 1
                n += self.count[x]
            self.sum = n

    # 記号の累積度数を求める
    def cumul(self, c):
        n = 0
        for x in xrange(c): n += self.count[x]
        return n

    # 符号化
    def encode(self, rc, c):
        temp = rc.range / self.sum
        rc.low += self.cumul(c) * temp
        rc.range = self.count[c] * temp
        rc.encode_normalize()
        self.update(c)

    # 復号
    def decode(self, rc):
        # 記号の探索
        def search_code(value):
            n = 0
            for c in xrange(self.size):
                if value < n + self.count[c]: return c, n
                n += self.count[c]
        #
        temp = rc.range / self.sum
        c, num = search_code(rc.low / temp)
        rc.low -= temp * num
        rc.range = temp * self.count[c]
        rc.decode_normalize()
        self.update(c)
        return c

# ファイルの読み込み
def read_file(fin):
    while True:
        c = getc(fin)
        if c is None: break
        yield c

# レンジコーダによる符号化
def encode(fin, fout):
    rc = RangeCoder(fout, ENCODE)
    freq = Freq(256)
    for x in read_file(fin):
        freq.encode(rc, x)
    rc.finish()

# レンジコーダによる復号
def decode(fin, fout, size):
    freq = Freq(256)
    rc = RangeCoder(fin, DECODE)
    for _ in xrange(size):
        putc(fout, freq.decode(rc))

# 符号化
def encode_file(name1, name2):
    size = os.path.getsize(name1)
    infile = open(name1, "rb")
    outfile = open(name2, "wb")
    putc(outfile, (size >> 24) & 0xff)
    putc(outfile, (size >> 16) & 0xff)
    putc(outfile, (size >> 8) & 0xff)
    putc(outfile, size & 0xff)
    if size > 0: encode(infile, outfile)
    infile.close()
    outfile.close()

# 復号
def decode_file(name1, name2):
    infile = open(name1, "rb")
    outfile = open(name2, "wb")
    size = 0
    for _ in xrange(4):
        size = (size << 8) + getc(infile)
    if size > 0: decode(infile, outfile, size)
    infile.close()
    outfile.close()

#
def main():
    eflag = False
    dflag = False
    opts, args = getopt.getopt(sys.argv[1:], 'ed')
    for x, y in opts:
        if x == '-e' or x == '-E':
            eflag = True
        elif x == '-d' or x == '-D':
            dflag = True
    if eflag and dflag:
        print 'option error'
    elif eflag:
        encode_file(args[0], args[1])
    elif dflag:
        decode_file(args[0], args[1])
    else:
        print 'option error'

#
s = time.clock()
main()
e = time.clock()
print "%.3f" % (e - s)
