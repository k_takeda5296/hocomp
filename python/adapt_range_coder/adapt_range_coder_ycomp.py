# coding: utf-8
#
# rc1.py : 適応型レンジコーダ
#
#          Copyright (C) 2007 Makoto Hiroi
#
import time, sys, getopt, os.path
from rangecoder import *

# 出現頻度表
class Freq:
    def __init__(self, size):
        self.size = size
        self.count = [1] * size
        self.sum = size

    # 出現頻度表の更新
    def update(self, c):
        self.count[c] += 1
        self.sum += 1
        if self.sum >= MIN_RANGE:
            n = 0
            for x in xrange(self.size):
                self.count[x] = (self.count[x] >> 1) | 1
                n += self.count[x]
            self.sum = n

    # 記号の累積度数を求める
    def cumul(self, c):
        n = 0
        for x in xrange(c): n += self.count[x]
        return n

    # 符号化
    def encode(self, rc, c):
        temp = rc.range / self.sum
        rc.low += self.cumul(c) * temp
        rc.range = self.count[c] * temp
        rc.encode_normalize()
        self.update(c)

    # 復号
    def decode(self, rc):
        # 記号の探索
        def search_code(value):
            n = 0
            for c in xrange(self.size):
                if value < n + self.count[c]: return c, n
                n += self.count[c]
        #
        temp = rc.range / self.sum
        c, num = search_code(rc.low / temp)
        rc.low -= temp * num
        rc.range = temp * self.count[c]
        rc.decode_normalize()
        self.update(c)
        return c

# ファイルの読み込み
def read_file(fin, bytePerInt):
    while True:
        c = 0;
        for i in range(0, bytePerInt):
            b = getc(fin)
            if b is None: return
            c = ((c << 8) | b)
        yield c

# レンジコーダによる符号化
def encode(fin, fout, maxint):
    rc = RangeCoder(fout, ENCODE)
    freq = Freq(maxint)
    bytePerInt = getc(fin) #最初のバイトは整数あたりのバイト数を示す
    for x in read_file(fin, bytePerInt):
        freq.encode(rc, x)
    rc.finish()

# レンジコーダによる復号
def decode(fin, fout, size, bytePerInt):
    maxint = 0
    for _ in range(0, bytePerInt):
        b = getc(fin)
        if b is None: break
        maxint = ((maxint << 8) | b)
    freq = Freq(maxint)
    rc = RangeCoder(fin, DECODE)
    count = 0
    for _ in range(0, size/bytePerInt): #sizeは必ずbytePerIntで割り切れる
        n = freq.decode(rc)
        for i in range(0, bytePerInt):
            sh = (bytePerInt - 1 - i)*8
            putc(fout, (n >> sh) & 0xff)
            count += 1



def output_maxinteger(name1, fout):
    infile = open(name1,"rb")
    bytePerInt = getc(infile)
    maxint = 0
    for x in read_file(infile, bytePerInt):
        maxint = max(maxint, x)
    maxint += 1
    for i in range(0, bytePerInt):
        sh = (bytePerInt - 1 - i)*8
        putc(fout, (maxint >> sh) & 0xff) #putcにより上も勝手に削られる？一応mask
    infile.close()
    return maxint


# 符号化
def encode_file(name1, name2):
    outfile = open(name2, "wb")
    bytePerInt = getc(open(name1, "rb")) #bytePerIntは1byteと仮定
    putc(outfile, bytePerInt)
    size = os.path.getsize(name1) - 1 #sizeは4byte以下を仮定. bytePerIntの1byte分を引く
    putc(outfile, (size >> 24) & 0xff)
    putc(outfile, (size >> 16) & 0xff)
    putc(outfile, (size >> 8) & 0xff)
    putc(outfile, size & 0xff)
    maxint = output_maxinteger(name1, outfile)
    infile = open(name1, "rb")
    if size > 0: encode(infile, outfile, maxint)
    infile.close()
    outfile.close()

# 復号
def decode_file(name1, name2):
    infile = open(name1, "rb")
    bytePerInt = getc(infile) #1byte目はbytePerInt
    outfile = open(name2, "wb")
    putc(outfile, bytePerInt)
    size = 0
    for _ in xrange(4):
        size = (size << 8) + getc(infile)
    if size > 0: decode(infile, outfile, size, bytePerInt)
    infile.close()
    outfile.close()

#
def main():
    eflag = False
    dflag = False
    opts, args = getopt.getopt(sys.argv[1:], 'ed')
    for x, y in opts:
        if x == '-e' or x == '-E':
            eflag = True
        elif x == '-d' or x == '-D':
            dflag = True
    if eflag and dflag:
        print 'option error'
    elif eflag:
        encode_file(args[0], args[1])
    elif dflag:
        decode_file(args[0], args[1])
    else:
        print 'option error'

#
s = time.clock()
main()
e = time.clock()
