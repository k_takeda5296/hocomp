#!/bin/bash
DIR=$(cd $(dirname $0); pwd)
python adapt_range_coder_ycomp.py -e $1 $1.yrc
python adapt_range_coder_ycomp.py -d $1.yrc $1.yrc.rev
diff $1 $1.yrc.rev
    
