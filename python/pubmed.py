import requests
from xml.etree.ElementTree import *
import os
import sys

#########################################################################
argvs = sys.argv 
argc = len(argvs)

print argvs
print argc

if argc!=3:
	print 'Usage0: # python %s [search term] [max # documents]' % argvs[0]
	quit()

try:
	dummy = int(argvs[2])
except:
	print 'Usage1: # python %s [search term] [max # documents]' % argvs[0]
	quit()

term = argvs[1]
retmax = argvs[2]

#
eutilurl = 'http://eutils.ncbi.nlm.nih.gov/entrez/eutils/'
esearchurl = eutilurl + 'esearch.fcgi?db=pubmed'
efetchurl = eutilurl + 'efetch.fcgi?db=pubmed'
#
def esearchUrl(term, retmax):
        return esearchurl + "&term=" + term + "&retmax=" + retmax

def efetchUrl(id):
        url = efetchurl + "&id=" + id + "&rettype=abstract"
        #print(url)
        return url


def id2abst(id):
        res = requests.get(efetchUrl(id))
        xml = res.content
        elem = fromstring(xml)
        return elem.findtext(".//AbstractText")

def isAsciiText(s):
	try:
		s.encode('ascii', 'strict')
	except:
		return False
	return True


##########################################################################################


#
outputDir = term + "/" + term + retmax + "/"
if not os.path.exists(outputDir):
        print('mkdir:'+outputDir)
        os.makedirs(outputDir)
#
res = requests.get(esearchUrl(term, retmax))
xml = res.content


#
elem = fromstring(xml)
num = 0
for e in elem.findall(".//Id"):
        print(num)
        abst = id2abst(e.text)
        if abst==None:
                continue
	if isAsciiText(abst):
		keta = len(str(retmax))
		fname = term + ("0"*keta + str(num))[-keta:] + ".txt"
		outpath = outputDir + fname
		f = open(outpath, 'w')
		f.write(abst.encode('utf-8'))
		num += 1
        
