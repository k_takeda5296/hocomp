let for_ho = ref true;;

type ty = O | Fun of ty * ty;;
type lterm = 
  | LVar of  int * ty
  | LAbst of lterm * ty
  | LApp of (lterm * lterm) * ty
;;

type tyterm = 
  | TyVar of  (int * tyterm list) * ty
  | TyAbst of tyterm * ty
	| TyLetC of (tyterm * tyterm) * ty
	| TyLetE of (tyterm * tyterm) * ty
type env = (ty * int) list;;

type tyt =
	| Var of int * ty * ty list
	| Abst of ty
	| LetC of ty * ty
	| LetE of ty * ty
	| Root
	| Dec(* declare *)
	
type tag =
	| KArg
	| KDec
	| KLetC
	| KLetE
	| KAbst
;;

let tag2string tag =
	match tag with
	| KArg -> "KArg"
	| KDec -> "KDec"
	| KLetC -> "KLetC"
	| KLetE -> "KLetE"
	| KAbst -> "KAbst"
;;

let print_tag tag = print_string @@ tag2string tag;;

let tagls = [KArg; KDec; KLetC; KLetE; KAbst];;

type nt = SS of string | TT of ty | EE of ty * env | KK of tag * ty;;
type alpha = T of string | NT of nt;;
type lhs = tyt * alpha list;;

type ptree = PT of lhs * (ptree list * ptree list);;(* (lhs, (ptree of present env, ptree of new env)) *)


type gram = (nt, (int * lhs) list) Hashtbl.t;;
