open Syntax
open TytermUtil
open OutputUtil
open GrammarHeadUtil
open GrammarHeadEnc
open GrammarHeadPtree

let gramtab0 = Hashtbl.create 1024;;
Hashtbl.add gramtab0 (SS "E") [(180, (Abst(O),[T("T")]))];;



let print_rules ls =
  print_string "[";
  List.iter (fun (r,lhs) -> 
    print_string @@ "("^string_of_int r^",";
    print_lhs lhs;
    print_string ");") ls;
  print_string "]";
;;


let print_grammar0 gtab =
  print_string "print_start:\n";
  Hashtbl.iter (fun nt l -> print_string (nt_to_string nt^" -> "); print_rules l; print_newline()) gtab;
  print_string "print_end:\n";
;;

let print_grammar() = print_grammar0 gramtab0;;




let update_env (env0,env1) lhs =
  match lhs with
	| (Abst(ty),_) -> assert (env0=env1);
		let argty = arg_type_of ty in
		let env0' = update_env env0 argty in ((env0',env0'),(env0',env0'))
  | (LetC(sigma,_),_) ->
		let env1' = TytermUtil.update_env env1 sigma in ((env0,env0),(env0,env1'))
	| (LetE(sigma,_),_) ->
		let env1' = TytermUtil.update_env env1 sigma in ((env0,env0),(env1',env1'))
	| _ -> assert (env0=env1); ((env0,env0),(env0,env0))
;;

let update_tytls tytls lhs pts =
	let (tyt,_) = lhs in
	match tyt with
	| LetC(sigma,ty) ->
		assert (tytls=[]);
		let pt::[] = pts in ptree2tytls pt []
	| LetE(sigma,ty) -> []
	| _ ->
		(match tytls with
		| [] -> []
		| t::ts -> if t=tyt then ts else (
(*			print_string @@ "update_tytls:\ttyt:";print_tyt tyt;print_string @@ "\tt:";print_tyt t;
			print_string @@ "\tcmp:"^string_of_int (tyt_cmp tyt t)^"\n";*)
			assert (tyt_cmp tyt t > 0);[]))
;;

(* leanear search! *)
let search_lhs lhs lhslist nt env =
	let rec search_lhs_aux lhs lhslist nt =
  	match lhslist with
   	| [] -> (print_lhs lhs; print_string @@ " was not found in:"^nt_to_string nt^"\n";
		 	print_string @@ "env:["; List.iter (fun (ty,occ) -> print_string @@ "("^type_to_string ty^","^string_of_int occ^");") env; print_string "]\n";
			print_grammar0 gramtab0; assert false)
  	| (low,_,r,lhs')::lhslist' ->
			if lhs=lhs' then
				(if r <= 0 then (print_string @@ "r:"^string_of_int r^"\n";assert false) else (low,r))
			else search_lhs_aux lhs lhslist' nt
	in search_lhs_aux lhs lhslist nt
;;

let count0 = ref 0;;
let count1 = ref 0;;
let valid_lhs lhs ty env tytls plhs =
	let var_check ty lhs env =
		match lhs with
		| (Abst(_),_) -> true
		| (LetC(_,_),_) -> true
		| (LetE(_,_),_) -> true
		| (Var(id,_,argtys),_) ->
  	  (try
				let var_ty = List.fold_right (fun t vt -> Fun(t,vt)) argtys ty in
  	    let (_,varnum) =  List.find (fun (t,_) -> t=var_ty) env in id < varnum
  	  with e -> false)(* unfavorible use of exception *)
		| _ -> true
	in
	let term_order_check ty lhs tytls =
		let (t,_) = lhs in
		match t with
		| LetC(_,_) -> true
		| LetE(_,_) -> true
		| _ -> 
			(match tytls with
			| [] -> true
			| tyt::xs -> tyt_cmp t tyt > -1)
	in
	let plhs_check lhs plhs for_ho =
		let (t,_) = lhs in
		let (p,_) = plhs in
		match p with
		| Var(_,_,_) -> if not for_ho then true else
			(match t with
			| LetC(_,_) -> false
			| LetE(_,_) -> false
			| _ -> true)
		| Dec -> if not for_ho then true else
			(match t with
			| Var(_,_,args) -> args<>[]
			| Abst(_) -> true
			| _ -> false)
		| LetC(_,_) ->
			(match t with
			| LetC(_,_) -> true
			| LetE(_,_) -> true
			| _ -> false)
		| LetE(_,_) ->
			(match t with
			| Var(_,_,args) -> args<>[]
			| _ -> true)
		| Abst(_) -> if not for_ho then true else
			(match t with
			| Var(_,_,args) -> args<>[]
			| _ -> true)
		| Root -> if not for_ho then true else
			(match t with
			| Abst(_) -> false
			| _ -> true)
	in
	let flag0 = term_order_check ty lhs tytls in
	let flag1 = var_check ty lhs env in
	let flag2 = plhs_check lhs plhs !Syntax.for_ho in
(*	if tytls<>[] then( 
	print_string @@ "valid_lhs:\tlhs:";print_lhs lhs;print_string @@ "\ttytls:[";
	List.iter (fun tyt -> print_tyt tyt;print_string ";") tytls;print_string "]";
	print_string @@ "\tflag0:"^string_of_bool flag0^"\tflag1:"^string_of_bool flag1^"\n";);*)
(*	if not flag0 || not flag1 then count0 := !count0+1;
	if flag0 && not flag1 then count1 := !count1+1;*)
	flag0 && flag1 && flag2
;;

let redist_lhsls occbase nt lhsls env tytls plhs = 
  let (KK(tag,ty)) = nt in
  let valid_lhsls = List.filter (fun (r,lhs) -> assert (r>0);valid_lhs lhs ty env tytls plhs) lhsls in
  let total = List.fold_left (fun sum (r,_) -> sum+r) 0 valid_lhsls in assert (occbase>0 && total>0);
  let fixed_lhsls = List.map (fun (r,lhs) -> (int_of_float ((float_of_int occbase)*.(float_of_int r)/.(float_of_int total)),lhs)) valid_lhsls in
  let (sum,dist_lhsls) = List.fold_left (fun (sum,ls) (r,lhs) -> (sum+r,(sum,sum+r,r,lhs)::ls)) (0,[]) fixed_lhsls (*reversed*)in
  let delta = occbase-sum in
  let _ = if delta < 0 then (print_string ("delta:"^string_of_int delta^"\n");assert false) in
  let (l,h,r,lhs)::xs = dist_lhsls in
  let rev_ans = (l,h+delta,r+delta,lhs)::xs in List.rev rev_ans
;;


let update_grammar lhs key =
	let (tyt,als) = lhs in
	let rules = Hashtbl.find gramtab0 key in
	let rules' = List.rev @@ List.fold_left (fun ls (r,lhs') -> if lhs=lhs' then (r+1,lhs')::ls else (r,lhs')::ls) [] rules in
	Hashtbl.replace gramtab0 key rules'
;;

let lookup_lhs occbase nt lhs env_pair tytls plhs =
	let env_pair' = update_env env_pair lhs in
	let (env,_) = env_pair in
  let lhslist = Hashtbl.find gramtab0 nt in
  let lhslist' = redist_lhsls occbase nt lhslist env tytls plhs in
  let (low,r) = search_lhs lhs lhslist' nt env in
	let _ = update_grammar lhs nt in
	(low,r,env_pair')
;;


(* linear search! inverse?*)
let rec search_val_aux v l =
  match l with
    [] -> assert false
  | (low,high,range,lhs)::l' ->
		if v< high then 
			let data = (low,high,range,lhs) in
			let num =(* ptree num for present env *)
				match lhs with
				| (LetC(_,_),_) -> 1
				| (LetE(_,_),_) -> 1
				| _ -> 0
			in
			(data,num)
    else search_val_aux v l'
;;

let search_val v nt env_pair occbase tytls plhs =
	let (env,_) = env_pair in
  let l = Hashtbl.find gramtab0 nt in
  let l' = redist_lhsls occbase nt l env tytls plhs in
  let ((low,high,range,lhs),num) = search_val_aux v l' in
	let env_pair' = update_env env_pair lhs in
	let _ = update_grammar lhs nt in
	((low,high,range,lhs),num,env_pair')
;;

(* get value(a hash) of key from hash of hash considering its absence *)
let get_hash_opt hash key =
  if Hashtbl.mem hash key then Hashtbl.find hash key
  else (let newhash = Hashtbl.create 1024 in Hashtbl.add hash key newhash; newhash)
;;

let count_hash_opt hash key =
  if Hashtbl.mem hash key then let c = Hashtbl.find hash key in Hashtbl.replace hash key (c+1)
  else Hashtbl.add hash key 1
;;

(* set of key as type nt *)
let add_key keyset key =
  let _ = Hashtbl.replace keyset key 0 in ()
;;


(* merge is delayed *)
let gather_rules tyterm =
  let rec gather_rules_aux tyterm hashes =
    let (abst_hash, letC_hash, letE_hash, var_hash) = hashes in
		let key = TT (type_of tyterm) in
		match tyterm with
    	| TyVar((name,args),typ) ->
        	let hash = get_hash_opt var_hash key in
					let argtys = List.map (fun arg -> type_of arg) args in
					let vty = List.fold_right (fun argty t -> Fun(argty,t)) argtys typ in
					let _ = count_hash_opt hash (name,vty,argtys) in
					let _ = List.iter (fun arg -> gather_rules_aux arg hashes) args in ()
    	| TyAbst(child,typ) ->
        	let _ = count_hash_opt abst_hash key in
        	let _ = gather_rules_aux child hashes in ()
			| TyLetC((arg,body),typ) ->
					let hash = get_hash_opt letC_hash key in
					let _ = count_hash_opt hash (type_of arg) in
					let _ = gather_rules_aux arg hashes in
					let _ = gather_rules_aux body hashes in ()
			| TyLetE((arg,body),typ) ->
					let hash = get_hash_opt letE_hash key in
					let _ = count_hash_opt hash (type_of arg) in
					let _ = gather_rules_aux arg hashes in
					let _ = gather_rules_aux body hashes in ()
  in
  let abshash = Hashtbl.create 1024 in(* ty -> occ *)
  let letChash = Hashtbl.create 1024 in(* ty -> sigma -> occ *)
	let letEhash = Hashtbl.create 1024 in(* ty -> sigma -> occ *)
  let varhash = Hashtbl.create 1024 in(* ty -> (id,varty,argtys) -> occ *)
  let _ = gather_rules_aux tyterm (abshash, letChash, letEhash, varhash) in
  (abshash, letChash, letEhash, varhash)
;;



(* add varlhs to inverse list of lhs *)
let add_var_rules g varhash =
	let var_cmp (i1,t1,_,_) (i2,t2,_,_) =
		let c = compare_ty t1 t2 in if c=0 then i1-i2 else c
	in
	Hashtbl.iter (fun key idhash ->
		let (TT ty) = key in
		let idls = Hashtbl.fold (fun (id,vty,argtys) occ ls -> (id,vty,argtys,occ)::ls) idhash [] in
		let sorted_idls = List.sort var_cmp idls (*inc order*)in
		List.iter (fun tag ->
			let key = KK(tag,ty) in
			let old_lhs_ls = if Hashtbl.mem g key then Hashtbl.find g key else [] in
			let lhs_ls = List.fold_left (fun ls (id,_,argtys,occ) ->
				let occ = 1 in(* init occ = 1 *)
				if occ > 0 then (occ,var_lhs_of id ty argtys)::ls else ls) old_lhs_ls sorted_idls in
			let _ = Hashtbl.replace g key lhs_ls in ()) Syntax.tagls
		) varhash
;;

(* add letlhs to inverse list of lhs *)
let add_let_rules g lethash let_lhs_of =
	let sigma_cmp (sigma1,occ1) (sigma2,occ2) =
		compare_ty sigma1 sigma2
	in
	Hashtbl.iter (fun key sigma_hash ->
		let (TT(ty)) = key in
		let sigma_ls = Hashtbl.fold (fun sigma occ ls -> (sigma,occ)::ls) sigma_hash [] in
		let sorted_sigma_ls = List.sort sigma_cmp sigma_ls (*inc order*)in
		List.iter (fun tag ->
			let key = KK(tag,ty) in
			let old_lhs_ls = if Hashtbl.mem g key then Hashtbl.find g key else [] in
			let lhs_ls = List.fold_left (fun ls (sigma,occ) ->
				let occ = 1 in(* init occ = 1 *)
				if occ > 0 then (occ,let_lhs_of sigma ty)::ls else ls) old_lhs_ls sorted_sigma_ls in
			let _ = Hashtbl.replace g key lhs_ls in ()) Syntax.tagls
		) lethash
;;

let add_abst_rules g absthash =
	Hashtbl.iter (fun key occ ->
		let occ = 1 in(* init occ = 1 *)
		let (TT(Fun(sigma,ty))) = key in
		List.iter (fun tag ->
			let key = KK(tag,Fun(sigma,ty)) in
			let old_lhs_ls = if Hashtbl.mem g key then Hashtbl.find g key else [] in
			let lhs_ls = (occ, lambda_lhs_of sigma ty)::old_lhs_ls in
			let _ = Hashtbl.replace g key lhs_ls in ()) Syntax.tagls
		) absthash
;;


let print_flag = ref false;;
let print_unranged_g g =
  if !print_flag then
    (print_string "print_unranged_g[start]:\n";
     Hashtbl.iter (fun (TT ty) occ_lhs_ls -> 
       print_string (key_to_nt_str ty^":\n");
       List.iter (fun (occ,lhs) -> print_string ("\t("^string_of_int occ^"):");print_lhs lhs;print_string "\n";) occ_lhs_ls) g;
     print_string "print_unranged_g[end]:\n";)
  else ()
;;



let rev_g_of rev_g =
	let g = Hashtbl.create 1024 in
	let _ = Hashtbl.iter (fun key lhs_ls -> Hashtbl.replace g key (List.rev lhs_ls)) rev_g in ()
;;


let rev_lhs_g_of rev_g =
	let g = Hashtbl.create 1024 in
	let _ = Hashtbl.iter (fun nt lhs -> Hashtbl.replace g nt (List.rev lhs)) rev_g in g
;;

let rev_ranged_lhs_g_of rev_g =
	let g = Hashtbl.create 1024 in
	let _ = Hashtbl.iter (fun nt lhs -> Hashtbl.replace g nt (List.rev lhs)) rev_g in g
;;

let convert_hashes_into_g hashes =
	let (abshash, letChash, letEhash, varhash) = hashes in
	let unranged_rev_g = Hashtbl.create 1024 in
	let _ = add_abst_rules unranged_rev_g abshash in
	let _ = add_let_rules unranged_rev_g letChash letC_lhs_of in
	let _ = add_let_rules unranged_rev_g letEhash letE_lhs_of in
	let _ = add_var_rules unranged_rev_g varhash in
	rev_lhs_g_of unranged_rev_g
;;


(* make grammar0 in which rules are allowed to be duplicate (as list) *)
(* make sure that g has only one value for one key *)
let make_grammar0 tyterm =
	let hashes = gather_rules tyterm in(* hash:ty->occ, hash:ty->hash:int->occ, hash:ty->hash:ty(sigma)->occ *)
	convert_hashes_into_g hashes
;;



let set_g_to_gramtab g =
  let _ = Hashtbl.clear gramtab0 in
  let _ = Hashtbl.iter (fun nt lhs_ls -> Hashtbl.add gramtab0 nt lhs_ls) g in ()
;;


let init_scfg tyterm = 
	let g = make_grammar0 tyterm in
	let _ = set_g_to_gramtab g in g
;;


let rec count_occ lhs t =
  let PT(lhs', (pts0,pts1)) = t in
  let n = if lhs=lhs' then 1 else 0 in
    List.fold_left (fun n t' -> n+ count_occ lhs t') n (pts0 @ pts1)
;;


let check_rule_num occbase = 
  Hashtbl.iter (fun _ l -> if not(List.length l < occbase+1) then (
    print_string @@ "check_rule_num\tlen:"^string_of_int (List.length l)^"\toccbase:"^string_of_int occbase^"\n";
    assert false)) gramtab0
;;




let type_to_nt typ = KK(KAbst,typ);;
let start_symbol() = type_to_nt O;;

(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
(************************************************************************************************)
let make_occs_str() =
	let s = Hashtbl.fold (fun (KK(tag,ty)) lhsls ss ->
		let init_s = type_to_string ty^"\t" in
		let sss = List.fold_left (fun ssss (r,_) -> ssss^"\t"^string_of_int r) init_s lhsls in
		ss^"\n"^sss) gramtab0 ""
	in s
;;
let ratio_of_ints i1 i2 =
	string_of_float (float_of_int i1 /. float_of_int i2)
;;
(*encode*)
let encode_gramtab bn t =
	let log = make_occs_str() in
	let file = "occs.log" in
	output_string_appending file log;
	let size0 = GrammarHeadEnc.encode_gramtab bn t gramtab0 in
	let _ = GrammarHeadEnc.check_decode gramtab0 in (size0, "")
;;

