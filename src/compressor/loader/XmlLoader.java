package compressor.loader;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import component.tree.SymbolTree;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.type.Fun;
import component.tyterm.type.O;
import component.tyterm.type.Type;
import compressor.HOCompressor;



public class XmlLoader implements Loader{

	private final String dir_;
	private final String file_;

	public XmlLoader(String dir, String file){
		dir_ = dir;
		file_ = file;
	}

	public TyRoot parsedTerm() {
		return parseAux(dir_+file_);
	}

	public String getDir(){
		return dir_;
	}

	public String getFile(){
		return file_;
	}

	private TyRoot parseAux(String fname) {
		SAXParserFactory spfactory = SAXParserFactory.newInstance();
		Xml2TyTermHandler handler = new Xml2TyTermHandler();

		try {
			SAXParser parser = spfactory.newSAXParser();
			parser.parse(new File(fname), handler);
		} catch (Exception e){//SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
			throw new IllegalArgumentException(e.toString());
		}
		SymbolTree tree = handler.getTree();
		if(HOCompressor.COMP_AS_BINARY_TREE) tree = BinaryTreeConverter.tree2BinaryTree(tree);
		TyRoot root = TyRoot.getRoot(null);
		List<List<String>> nameListList = makeNameListList(tree);
		root.addSymbolTypes(nameListList);
		TyTerm term = tree2Term(tree.getChildren().get(0), root);//ROOTを除く
		root.setChild(term); term.setParent(root);
		return root;
	}
	
	private static List<List<String>> makeNameListList(SymbolTree tree){
		Map<Integer, Set<String>> rankNameMap = new HashMap<>();
		obtainRankNameMap(tree, rankNameMap);
		List<List<String>> nameListList = new ArrayList<>();
		for(int rank : rankNameMap.keySet()){
			List<String> nameList = new ArrayList<>(rankNameMap.get(rank));
			Collections.sort(nameList);
			addToNameListList(nameListList, rank, nameList);
		}
		return nameListList;
	}
	private static void obtainRankNameMap(SymbolTree node, Map<Integer, Set<String>> rankNameMap){
		String name = node.getSymbolName();
		int rank = node.getChildren().size();
		if(!rankNameMap.containsKey(rank)){
			Set<String> nameSet = new HashSet<>();
			nameSet.add(name);
			rankNameMap.put(rank, nameSet);
		}else{
			Set<String> nameSet = rankNameMap.get(rank);
			if(!nameSet.contains(name)) nameSet.add(name);
		}
		
		node.getChildren().stream().forEach(child -> obtainRankNameMap(child, rankNameMap));
	}
	private static void addToNameListList(List<List<String>> nameListList, int rank, List<String> nameList){
		while(nameListList.size()-1 < rank-1) nameListList.add(new ArrayList<>());//addしたいindex-1まで埋める。nullを作りたくない
		if(nameListList.size()-1 > rank-1){
			nameListList.set(rank, nameList);//nameList.add(rank, nameList);と同値のはず
		}else if(nameListList.size()-1==rank-1){
			nameListList.add(nameList);
		}else{
			throw new IllegalArgumentException();
		}
		if(nameListList.get(rank)!=nameList) throw new IllegalArgumentException();
	}

	private TyTerm tree2Term(SymbolTree node, TyRoot root){
		Type type = O.O;
		for(@SuppressWarnings("unused") SymbolTree child : node.getChildren()){
			type = new Fun(O.O, type);
		}
		int name = root.getIntNameOf(node.getSymbolName(), type);
		TySymbol symbol = new TySymbol(name, type);

		TyTerm term = symbol;
		for(SymbolTree child : node.getChildren()){
			TyTerm arg = tree2Term(child, root);
			TyApp app = new TyApp(term, arg, ((Fun)term.getType()).getRight());
			term.setParent(app); arg.setParent(app);
			term = app;
		}
		return term;
	}

	private class Xml2TyTermHandler extends DefaultHandler {

		SymbolTree root_;//ルート
		SymbolTree consideringNode_;
		boolean isLastStart = false;//0なら直前はstartElement

		private Xml2TyTermHandler(){
			root_ = SymbolTree.getRoot();
			consideringNode_ = root_;
		}

		private SymbolTree getTree(){
			return root_;
		}

		/**
		 * ドキュメント開始
		 */
		@Override
		public void startDocument() {}

		/**
		 * ドキュメント終了
		 */
		@Override
		public void endDocument() {
			if(consideringNode_!=root_) throw new IllegalArgumentException("document unsuccessfully ended!");
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes){
			SymbolTree newNode = new SymbolTree(qName);
			consideringNode_.addChild(newNode);newNode.setParent(consideringNode_);
			consideringNode_ = newNode;
			isLastStart = true;
		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			if(!consideringNode_.getSymbolName().equals(qName)) throw new IllegalArgumentException("endElement does not match!");
			if(isLastStart && !HOCompressor.COMP_AS_BINARY_TREE){//直前がstartElementならばendノードを子供に追加する
				SymbolTree endNode = new SymbolTree(END_TAG);
				consideringNode_.addChild(endNode);
			}
			consideringNode_ = consideringNode_.getParent();
			isLastStart = false;
		}
	}

}