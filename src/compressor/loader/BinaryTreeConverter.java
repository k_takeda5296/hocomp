package compressor.loader;

import java.util.ArrayList;
import java.util.List;

import util.MyUtil;

import component.tree.SymbolTree;

public class BinaryTreeConverter {

	public static SymbolTree tree2BinaryTree(SymbolTree tree){
		List<SymbolTree> children = tree.getChildren();
		if(children.isEmpty()){
			return new SymbolTree("00"+tree.getSymbolName());
		}else{
			SymbolTree retTree = new SymbolTree("10"+tree.getSymbolName());
			SymbolTree child = childSiblingsOf(children);
			retTree.addChild(child); child.setParent(retTree);
			return retTree;
		}
	}

	private static SymbolTree childSiblingsOf(List<SymbolTree> children){
		SymbolTree tail = children.remove(children.size()-1);
		SymbolTree retTree = tree2BinaryTree(tail);
		List<SymbolTree> revChildren = MyUtil.reversedArrayList(children);
		for(SymbolTree child : revChildren){
			List<SymbolTree> childChildren = child.getChildren();
			if(childChildren.isEmpty()){
				SymbolTree tmp = retTree;
				retTree = new SymbolTree("01"+child.getSymbolName());
				retTree.addChild(tmp);
			}else{
				SymbolTree tmp = retTree;
				retTree = new SymbolTree("11"+child.getSymbolName());
				SymbolTree firstChild = childSiblingsOf(childChildren);
				retTree.addChild(firstChild); firstChild.setParent(retTree);
				retTree.addChild(tmp); tmp.setParent(retTree);
			}
		}
		return retTree;
	}


	public static SymbolTree binaryTree2Tree(SymbolTree btree){
		String symbol = btree.getSymbolName();
		String name = nameOf(symbol);
		String childrenNum = childNumOf(symbol);
		boolean hasFirst = childrenNum.charAt(0)=='1';
		//boolean hasSecond = childrenNum.charAt(1)=='1';//false
		if(!hasFirst){
			return new SymbolTree(name);
		}else{
			List<SymbolTree> children = childSiblingsOf(btree);
			SymbolTree retTree = new SymbolTree(name);
			children.stream().forEach(c -> retTree.addChild(c));
			return retTree;
		}
	}

	private static List<SymbolTree> childSiblingsOf(SymbolTree btree){
		List<SymbolTree> children = new ArrayList<>();
		List<SymbolTree> bChildren = getBChildrenOf(btree);
		SymbolTree btail = bChildren.remove(bChildren.size()-1);
		SymbolTree tail = binaryTree2Tree(btail);
		for(SymbolTree bChild : bChildren){
			String symbol = bChild.getSymbolName();
			String name = nameOf(symbol);
			String childrenNum = childNumOf(symbol);
			boolean hasFirst = childrenNum.charAt(0)=='1';
			//boolean hasSecond = childrenNum.charAt(1)=='1';//true
			if(!hasFirst){
				children.add(new SymbolTree(name));
			}else{
				SymbolTree child = new SymbolTree(name);
				List<SymbolTree> childChildren = childSiblingsOf(bChild);
				childChildren.forEach(cc -> child.addChild(cc));
				children.add(child);
			}
		}
		children.add(tail);
		return children;
	}

	private static List<SymbolTree> getBChildrenOf(SymbolTree btree){
		List<SymbolTree> bChildren = new ArrayList<>();
		SymbolTree bChild = btree.getChildren().get(0);//btreeは"10"
		bChildren.add(bChild);
		while(true){
			String symbol = bChild.getSymbolName();
			String childrenNum = childNumOf(symbol);
			boolean hasSecond = childrenNum.charAt(1) == '1';//右の子を持っているか
			if(!hasSecond) return bChildren;
			boolean hasFirst = childrenNum.charAt(0) == '1';
			bChild = hasFirst ? bChild.getChildren().get(1) : bChild.getChildren().get(0);//右の子
			bChildren.add(bChild);
		}
	}
	
	private static String nameOf(String symbolName){
		return symbolName.substring(2);
	}
	private static String childNumOf(String symbolName){
		return symbolName.substring(0,2);
	}
}
