package compressor.loader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;



public class XmlCleaner{

	public static void main (String[] args){
		new XmlCleaner("benchmark/xml/test/", "1998statistics.xml");
	}

	private final String dir_;
	private final String file_;

	public XmlCleaner(String dir, String file){
		dir_ = dir;
		file_ = file;

		String outPath = makeOutputPath(dir_, file_);
		outputTreeFile(dir_+file_, outPath);
	}
	
	
	private String makeOutputPath(String dir, String file){
		if(!file.substring(file.length()-4,file.length()).equals(".xml"))
			throw new IllegalArgumentException("Invalid file extension.");
		return dir_ + file_.substring(0, file_.length()-4) + ".tree.xml";
	}

	private void outputTreeFile(String input, String output) {
		SAXParserFactory spfactory = SAXParserFactory.newInstance();
		PrintHandler handler = new PrintHandler(output);
		try {
			SAXParser parser = spfactory.newSAXParser();
			parser.parse(new File(input), handler);
		} catch (Exception e){//SAXException | IOException | ParserConfigurationException e) {
			e.printStackTrace();
			throw new IllegalArgumentException(e.toString());
		}
		return;
	}

	private class PrintHandler extends DefaultHandler {
		private static final boolean PRINT_OUTPUT = true;

		PrintWriter pw_;
		String outPath_;
		private PrintHandler(String outPath){
			outPath_ = outPath;
		}

		int tabNum_ = 0;
		private void writeTab(){
			for(int i=0; i < tabNum_; i++){
				if(PRINT_OUTPUT) System.out.print(" ");
				pw_.write(" ");
			}
		}

		/**
		 * ドキュメント開始
		 */
		@Override
		public void startDocument() {
			try {
				pw_ = new PrintWriter(outPath_);
			} catch (FileNotFoundException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			} 
			if(PRINT_OUTPUT) System.out.println("startDocument!");
		}

		/**
		 * ドキュメント終了
		 */
		@Override
		public void endDocument() {
			if(PRINT_OUTPUT) System.out.println("endDocument!");
			pw_.close();
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes){
			writeTab();
			String sTag = "<"+qName+">";
			if(PRINT_OUTPUT) System.out.println(sTag);
			pw_.write(sTag+"\n");
			tabNum_++;
		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			tabNum_--;
			writeTab();
			String eTag = "</"+qName+">";
			System.out.println(eTag);
			pw_.write(eTag+"\n");
		}
	}

}