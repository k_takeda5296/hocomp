package compressor.loader;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import component.tree.SymbolTree;

import decompressor.saver.XmlSaver;


public class EnTextLoader{

	public static void main(String args[]){
		String input = "benchmark/txt/en/cancer/cancer500_50.txt";
		String output = confirmXml(input, true);
		System.out.println("EntextLoader#main	confirmed:	"+output);
	}

	public static String confirmXml(String path, boolean isSeq){
		File file = new File(path);
		if(!file.exists()) throw new IllegalArgumentException("path does not exists!");
		String xmlPath = path + (isSeq ? ".seq" : "") + ".xml";
		if((new File(xmlPath)).exists()) return xmlPath;
		makeXml(path, xmlPath, isSeq);
		return xmlPath;
	}
	
	/*
	 * ディレクトリ指定ver. ディレクトリ内のファイルを名前でソートした順にmaxNumだけ選び、XMLに変換
	 */
	public static String confirmXml(String dirPath, int maxNum, boolean isSeq){
		String textPath = EnTextsCombinator.combineEnTextsWithDir(dirPath, maxNum); 
		File file = new File(textPath);
		if(!file.exists()) throw new IllegalArgumentException("path does not exists!");
		String xmlPath = textPath + (isSeq ? ".seq" : "") + ".xml";
		//if((new File(xmlPath)).exists()) return xmlPath;
		makeXml(textPath, xmlPath, isSeq);
		return xmlPath;
	}

	private static void makeXml(String path, String xmlPath, boolean isSeq){
		File file = new File(path);
		List<List<String>> strListList = Tokenizer.file2SymbolListListForXml(file);
		if(isSeq){
			List<String> strList = new ArrayList<>();
			strListList.forEach(strls -> strls.forEach(str -> strList.add(str)));
			strListList = new ArrayList<>();
			strListList.add(strList);
		}
		SymbolTree tree = strListList2Tree(strListList);

		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(xmlPath)));
			out.write("<?xml version=\"1.0\"?>\n");
			XmlSaver.outputTreeAsXml(tree, out);
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}




	private static final String BR = "__BR__";
	private static final String BRE = "__BRE__";
	private static SymbolTree strListList2Tree(List<List<String>> strListList){
		if(strListList.size() < 1) throw new IllegalArgumentException("Empty file list!");
		SymbolTree br = new SymbolTree(BR);
		if(strListList.size() == 1){
			br.addChild(new SymbolTree(BRE));
			SymbolTree tree = strList2SingleBranch(strListList.get(0));
			br.addChild(tree);
			return br;
		}else{//strListList.size() > 1
			SymbolTree child = strListList2Tree(strListList.subList(1, strListList.size()));
			br.addChild(child);
			SymbolTree tree = strList2SingleBranch(strListList.get(0));
			br.addChild(tree);
			return br;
		}
	}

	private static SymbolTree strList2SingleBranch(List<String> strList){
		if(strList.size() < 1) throw new IllegalArgumentException("empty strList!");
		SymbolTree root = new SymbolTree(strList.get(0));
		if(strList.size()==1) return root;

		SymbolTree tree = root;
		for(int i=1; i < strList.size(); i++){
			SymbolTree newNode = new SymbolTree(strList.get(i));
			tree.addChild(newNode);
			tree = newNode;
		}
		return root;
	}

}