package compressor.loader;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import util.MyUtil;

import component.tyterm.term.TyRoot;

/*
 * xmlを生成してからそれをパース
 */
public class DnaTextLoader implements Loader{


	public static void main(String[] args){
		String DNA_DIR = "txt/dna/";
		String DNA_FILE = "TAIR10_1.ary";
		int DNA_NUM = 1000*1000;
		for(int i=0; i < 21; i++){
			Loader loader = new DnaTextLoader(DNA_DIR+DNA_FILE, i*DNA_NUM);
			loader.parsedTerm();
			System.out.println("DnaTextLoader#main	"+i);
			
		}
	}



	private final String dir_;
	private final String file_;

	public DnaTextLoader(String path, int maxNum){
		//if(maxNum % (1000*1000) != 0) throw new IllegalArgumentException("maxNum shoud be the times of 1M!!!");
		File file = new File(path);
		dir_ = file.getParent() + "/";
		file_ = file + "_" + maxNum + ".xml";
	}


	public TyRoot parsedTerm(){
		throw new IllegalArgumentException("not supported!");
	}
	
	public static String confirmXml(String path, int maxNum){
		String dir = (new File(path)).getParent() +"/";
		String file = (new File(path)).getName();
		MyUtil.makeDirIfNotExists(dir);
		String xmlPath = dir + file + "_" + maxNum + ".xml";
		if(new File(xmlPath).exists()) return xmlPath;
		return (new DnaTextLoader(dir+file, maxNum)).makeXml(xmlPath, dir+file, maxNum);
	}

	private String makeXml(String xmlPath, String dnaPath, int maxNum){
		FileOutputStream out;
		FileInputStream source;
		try{
			out = new FileOutputStream(xmlPath);
			source = new FileInputStream(dnaPath);
			outputTreeAsXml(maxNum, source, out);
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return xmlPath;
	}

	private void outputTreeAsXml(int remainNum, FileInputStream source, FileOutputStream out) throws IOException{
		if(remainNum < 1 || source.available() < 1) return;
		String symbol = elimNewLine(source);
		//System.out.println("symbol:"+symbol);
		String startElem = "<" + symbol + ">";
		out.write(startElem.getBytes());

		outputTreeAsXml(remainNum-1, source, out);

		String endElem = "</" + symbol + ">";
		out.write(endElem.getBytes());
	}

	private String elimNewLine(FileInputStream source) throws IOException{
		if(source.available() < 1) throw new IllegalArgumentException("no available sources");//elimNewLineのループ中にsourceが切れることはあまり無いと思うので適当に例外を投げている
		int ascii = source.read();
		//System.out.println("ascii"+ascii);
		if(ascii==10) return elimNewLine(source);
		String symbol = new String(new byte[] { (byte)ascii }, "US-ASCII");
		return symbol;
	}

	public String getDir(){
		return dir_;
	}

	public String getFile(){
		return file_;
	}



}