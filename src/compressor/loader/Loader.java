package compressor.loader;

import component.tyterm.term.TyRoot;

public interface Loader {
	public static final String END_TAG = "__END__";
	
	TyRoot parsedTerm();
	String getDir();
	String getFile();
}
