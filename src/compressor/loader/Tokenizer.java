package compressor.loader;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;



class Tokenizer {
	public final static char QUOTE = '\'';
	public final static char DOUBLE_QUOTE = '"';

	/*
	@SuppressWarnings("serial")
	private static final Set<Character> splitters_ = new LinkedHashSet<Character>(){{
		add('\t');
		add('\n');
		add(' ');
		add(' ');
	}};
	*/

	private static final String PREFIX = "__";
	
	@SuppressWarnings("serial")
	static final Set<String> terminators_ = new LinkedHashSet<String>(){{
		add(PREFIX+"PERIOD");
		add(PREFIX+"QUESTION");
		add(PREFIX+"EXCLAMATION");
	}};

	@SuppressWarnings("serial")
	private static final Map<Character, String> singletons_ = new HashMap<Character, String>(){{
		put('.', "PERIOD");
		put('?', "QUESTION");
		put('!', "EXCLAMATION");
		put(',', "CAMMA");
		put('\'', "SINGLEQ");
		put('"', "DOUBLEQ");
		put('(', "LPAREN");
		put(')', "RPAREN");
		put('[', "LBRACKETS");
		put(']', "RBRACKETS");
		put('{', "LBRACES");
		put('}', "RBRACES");
		put('-', "HYPHEN");
		put('+', "PLUS");
		put('±', "PLUSMINUS");
		put('*', "STAR");
		put('\\', "YEN");
		put('#', "SHARP");
		put('$', "DOLLER");
		put('&', "AND");
		put('|', "OR");
		put('<', "LT");
		put('>', "GT");
		put('≥', "GTE");
		put('≤', "LTE");
		put('=', "EQUAL");
		put('%', "PERCENT");
		put('~', "WAVE");
		put('/', "SLASH");
		put(':', "COLON");
		put(';', "SEMICOLON");
		put('×', "MULTIPLE");
		put('∞', "INFINITY");
		put('©', "C");
		put('', "Q");
		put('™', "TM");
		put('®', "R");
		put('³', "CUBE");
		put('@', "ATMARK");
		put('`', "BACKTIK");
		put('^', "HAT");
	}};
	


	/*
	 * 区切り文字を無視して単語、記号に分ける
	 */
	public static List<String> file2SymbolList(File file){
		List<String> strList = new ArrayList<>();
		try (Scanner scanner = new Scanner(file)) {
			while (scanner.hasNext()) strList.add(scanner.next());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		strList = splitWords(strList);

		return strList;
	}
	
	/*
	 * split by ch and add ch to PREFIX.
	 */
	private static List<String> splitWords(List<String> strList){
		List<String> retList = new ArrayList<>();
		strList.forEach(str -> {
			char[] ary = str.toCharArray();
			String s = new String("");
			for(char c : ary){
				if(singletons_.keySet().contains(c)){
					retList.add(s);
					retList.add(c+"");
					s = new String("");
				}else{
					s += c;
				}
			}
			retList.add(s);
		});
		return retList.stream().filter(str -> !str.equals("")).collect(Collectors.toList());
	}
	
	public static List<List<String>> file2SymbolListListForXml(File file){
		List<String> strList = file2SymbolList(file);//singletonsを考慮してstrlistに分割
		strList = processSymbolsforXml(strList);//XMLタグに使用不可能な文字列を変換
		
		//文の終端記号（ピリオドなど）でリストを分割
		List<List<String>> strListList = new ArrayList<>();
		int start = 0;
		for(int i=0; i < strList.size(); i++){
			if(terminators_.contains(strList.get(i))){
				strListList.add(strList.subList(start, i+1));
				start = i+1;
			}
		}
		if(start < strList.size()){
			strListList.add(strList.subList(start, strList.size()));
		}
		return strListList;
	}

	/*
	 * strListの要素はsingletonかsingletonを含まない文字列のどちらか
	 * strListの各要素について、要素がsingletonであるか数字で始まる文字列である場合、適切に置き換えを行う
	 */
	private static List<String> processSymbolsforXml(List<String> strList){
		List<String> retList = new ArrayList<>();
		strList.forEach(str -> {
			char[] ary = str.toCharArray();
			if(ary.length==1 && singletons_.keySet().contains(ary[0])){
				retList.add(PREFIX+singletons_.get(ary[0]));
			}else if(isNumber(ary[0])){
				retList.add(PREFIX+str);
			}else{
				retList.add(str);
			}
		});
		return retList.stream().filter(str -> !str.equals("")).collect(Collectors.toList());
	}
	
	/*
	 * 数字列とそう出ない列を分離
	 */
	/*
	private static List<String> processNumbers(char[] ary){
		List<String> retList = new ArrayList<>();
		for(int i=0; i < ary.length;){
			String s = new String("");
			char c = ary[i];
			while(i < ary.length && isNumber(c = ary[i])){
				s += c;
				i++;
			}
			if(!s.equals("")) retList.add(PREFIX+s);
			s = new String("");
			while(i < ary.length && !isNumber(c = ary[i])){
				s += c;
				i++;
			}
			retList.add(s);
		}
		return retList;
	}
	*/
	
	private static boolean isNumber(char c){
		return c >= '0' && c <='9';
	}

}
