package compressor.loader;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


public class EnTextsCombinator{

	public static void main(String args[]){
		String input = "benchmark/txt/en/cancer/cancer500/";
		String outFile = combineEnTextsWithDir(input, 100);
		System.out.println("Combinend: "+outFile);
	}

	public static String combineEnTextsWithDir(String dirPath, int maxNum){
		File dir = new File(dirPath);
		if(!dir.exists()) throw new IllegalArgumentException("path does not exists!	path:"+dir);
		if(!dir.isDirectory()) throw new IllegalArgumentException("path should be directory!");
		String outFile = dir.getParent() + "/" + dir.getName() + "_" + maxNum + ".txt";
		if((new File(outFile)).exists()) return outFile;
		combineDirIntoFile(dirPath, outFile, maxNum);
		return outFile;
	}

	private static void combineDirIntoFile(String dirPath, String outFile, int maxNum){
		File dir = new File(dirPath);
		File[] files = dir.listFiles(f -> f.isFile() && !f.isHidden());//対称のファイルはすべてasciiファイルと仮定
		List<File> fileList = new ArrayList<File>();
		Arrays.sort(files, (f1,f2) -> f1.getName().compareTo(f2.getName()));
		for(int i=0; i < files.length && i < maxNum; i++){
			System.out.println("file["+i+"]:"+files[i]);
			fileList.add(files[i]);
		}
		
		List<String> strList =
					fileList.stream()
					.flatMap(file -> Tokenizer.file2SymbolList(file).stream())
					.collect(Collectors.toList());

		try {
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outFile)));
			strList.stream().forEach(s -> out.write(s + " "));
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

}