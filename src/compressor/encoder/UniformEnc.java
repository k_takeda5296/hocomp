package compressor.encoder;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import util.MyUtil;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyId;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.type.Type;

class UniformEnc{

	static LinkedHashMap<Type, Integer> makeK(TyRoot term){
		LinkedHashMap<Type, Integer> K = new LinkedHashMap<Type, Integer>();
		List<TyAbst> absts = term.getAllAbstChildrenByTraverseAll();
		for(TyAbst abst : absts){
			if(!K.containsKey(abst.getType())) K.put(abst.getType(), K.size());
		}
		return K;
	}

	static List<Integer> term2DB(TyRoot root, LinkedHashMap<Type, Integer> K){
		LinkedHashMap<IdLike, Integer> G = makeG(root);
		UniformIndexManager uim = new UniformIndexManager(K, G);
		List<Integer> db = new ArrayList<Integer>();
		Enc(uim, root.getChild(), root.getChild().getType(), db);
		return db;
	}

	private static LinkedHashMap<IdLike, Integer> makeG(TyRoot term) {
		LinkedHashMap<IdLike, Integer> G = new LinkedHashMap<>();
		List<TySymbol> symbols = term.getSymbols();
		for(TySymbol s : symbols){
			G.put(new IdLike(s), G.size());
		}
		return G;
	}

	private static void Enc(UniformIndexManager uim, TyTerm term, Type type, List<Integer> encode){
		if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			TyTerm left = app.getLeftChild();
			TyTerm right = app.getRightChild();
			Enc(uim, left, type, encode);//leftはtypeを引き継ぐ
			Enc(uim, right, right.getType(), encode);//rightは引き継がない
		}else{
			encodeOne(uim, term, type, encode);
		}
	}

	private static void encodeOne(UniformIndexManager uim, TyTerm term, Type type, List<Integer> encode){
		if(term instanceof TyApp) throw new IllegalArgumentException("cannot encode TyApp!");
		if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			encode.add(uim.getIndex(abst, type));
			IdLike var = new IdLike(MyUtil.topOfSet(abst.getVars()));
			uim.addId(var);//varの型であることに注意
			TyTerm child = abst.getChild();
			Enc(uim, child, child.getType(), encode);
			uim.removeId(var);
		}else{//termToConsiderはTyVarかTySymbol
			TyId id = (TyId)term;
			int index = uim.getIndex(id, type);//possibilityIndex(G, id, type);//引数であるようなSymbolかVarのときは型で分類した中から選ぶ
			encode.add(index);//K.size()-1まではλ抽象の型番号として用いられている．
		}
	}

	static class IdLike{//TyIdのMapを使いたいがTyIdはequalsをオーバーライドできないため、エンコーディング用のTyId
		final int name_;
		final Type type_;
		IdLike(int name, Type type){
			name_ = name;
			type_ = type;
		}
		IdLike(TyId id){
			this(id.getName(), id.getType());
		}
		int getName() { return name_; }
		Type getType() { return type_ ; }
		@Override
		public boolean equals(Object obj){
			if(!(obj instanceof IdLike)) return false;
			IdLike i = (IdLike)obj;
			return this.name_==i.name_;
		}
		@Override
		public int hashCode(){
			return name_;
		}
		@Override
		public String toString(){
			return "{name:"+name_+", type:"+type_+"}";
		}
	}
}