package compressor.encoder;

import java.util.LinkedHashMap;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyId;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Type;
import compressor.encoder.UniformEnc.IdLike;

class UniformIndexManager {

	LinkedHashMap<Type, Integer> baseK_;
	LinkedHashMap<IdLike, Integer> baseG_;//LinkedHashSetでいい

	UniformIndexManager(LinkedHashMap<Type, Integer> baseK, LinkedHashMap<IdLike, Integer> baseG){
		baseK_ = baseK;
		baseG_ = baseG;
	}

	int getIndex(TyTerm term, Type type){
		try{
			if(term instanceof TyAbst){
				if(!baseK_.containsKey(term.getType())) throw new IllegalArgumentException("no match abst type!");
				return baseK_.get(term.getType());
			}else if(term instanceof TyId){
				TyId id = (TyId)term;
				IdLike idLike = new IdLike(id);
				if(!baseG_.containsKey(idLike)) throw new IllegalArgumentException("no match id");
				return baseG_.get(idLike) + baseK_.size();
			}
			throw new IllegalArgumentException("no match class of 'term'!");
		}catch(Exception e){
			System.err.println("term:"+term);
			System.err.println("type:"+type);
			if(term instanceof TyVar){
				TyVar var = (TyVar)term;
				System.err.println("var.type:"+var.getType());
				System.err.println("binder.type:"+var.getBinder().getType());
			}
			throw e;
		}
	}

	void addId(IdLike idLike){
		baseG_.put(idLike, baseG_.size());//ベースに追加
	}

	void removeId(IdLike idLike){
		baseG_.remove(idLike);//ベースから取り除く
	}

	void print(){
		System.out.println("TypeIndexManager["+hashCode()+"]");
		System.out.println("baseK:"+baseK_);
		System.out.println("baseG:"+baseG_);
	}
}
