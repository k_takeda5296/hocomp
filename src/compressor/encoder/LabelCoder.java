package compressor.encoder;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.List;

import main.Checker;
import util.MyUtil;
import coder.variablecoder.VariableByteCode;

import compressor.HOCompressor;


public final class LabelCoder {

	public static void compressLabelFile(String inputFile, String outputFile){
		String script = "python/adapt_range_coder/adapt_range_coder_ycomp.py";
		String cmd = "python"+ " "+script+ " -e " + inputFile + " " + outputFile;
		if(HOCompressor.PRINT_CMD) System.out.println("LabelCoder#compressLabelFile	cmd:"+cmd);
		Runtime r = Runtime.getRuntime();
		try {//pythonコマンドが通らないときはpyコマンドを試す
			Process p = r.exec(cmd);
			p.waitFor();
		} catch(IOException e) {
			cmd = "py"+ " "+script+ " -e " + inputFile + " " + outputFile;
			if(HOCompressor.PRINT_CMD) System.out.println("LabelCoder#compressLabelFile	cmd:"+cmd);
			r = Runtime.getRuntime();
			try {
				Process p = r.exec(cmd);
				p.waitFor();
			} catch (InterruptedException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO 自動生成された catch ブロック
				e1.printStackTrace();
			}
		} catch(Throwable e) {
			e.printStackTrace();
		}
	}

	public static void outputLabelFile(List<Integer> labelList, String labelFile, String yrcFile) {
		System.out.println("LabelCoder#outputLabelFile	labelList:"+labelList);
		byte[] bytes = HOCompressor.USE_VARIABLE_BYTE ? VariableByteCode.getBytes(labelList) : encodeRawLabelBytes(labelList);
		MyUtil.outputByteArray(bytes, labelFile);
		compressLabelFile(labelFile, yrcFile);
		if(HOCompressor.DECODE_FILE_CHECK) System.out.println("LabelCoder#outputLabelFile	check:"+Checker.intSeqCheck(labelList, yrcFile));
	}

	//int列をそのまま固定長のbyte列に置き換える（先頭はintあたりの必要ビット）
	private static byte[] encodeRawLabelBytes(List<Integer> labelList){
		int max = Collections.max(labelList);
		byte bytePerInt = (byte)(max < 256 ? 1 : max < 256*256 ? 2 : max < 256*256*256 ? 3 : 4);
		byte[] bytes = new byte[1 + bytePerInt * labelList.size()];
		bytes[0] = bytePerInt;
		for(int i=0; i < labelList.size(); i++){
			byte[] integer = int2Bytes(labelList.get(i), bytePerInt);
			System.arraycopy(integer, 0, bytes, 1+i*bytePerInt, bytePerInt);
		}
		return bytes;
	}

	private static byte[] int2Bytes(int i, int bytePerInt){
		byte[] bytes = ByteBuffer.allocate(4).putInt(i).array();
		byte[] integer = new byte[bytePerInt];
		System.arraycopy(bytes, 4-bytePerInt, integer, 0, bytePerInt);
		return integer;
	}
}
