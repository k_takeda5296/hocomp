package compressor.encoder;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyId;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Type;
import compressor.encoder.TypeClassifyingEnc.IdLike;

class TypeIndexManager {

	LinkedHashMap<Type, Integer> baseK_;
	LinkedHashMap<IdLike, Integer> baseG_;//LinkedHashSetでいい
	Map<Type, LinkedHashMap<Type, Integer>> Ks_ = new HashMap<>();
	Map<Type, LinkedHashMap<IdLike, Integer>> Gs_ = new HashMap<>();

	TypeIndexManager(LinkedHashMap<Type, Integer> baseK, LinkedHashMap<IdLike, Integer> baseG){
		baseK_ = baseK;
		baseG_ = baseG;
	}

	int getIndex(TyTerm term, Type type){
		try{
			if(!Ks_.containsKey(type)) updateKGs(type);
			if(term instanceof TyAbst){
				Map<Type, Integer> K = Ks_.get(type);
				if(!K.containsKey(term.getType())) throw new IllegalArgumentException("no match abst type!");
				return K.get(term.getType());
			}else if(term instanceof TyId){
				TyId id = (TyId)term;
				IdLike idLike = new IdLike(id);
				Map<Type, Integer> K = Ks_.get(type);//updateKGであらかじめ足しこんでおくほうが良い？
				Map<IdLike, Integer> G = Gs_.get(type);
				if(!G.containsKey(idLike)) throw new IllegalArgumentException("no match id");
				return G.get(idLike) + K.size();
			}
			throw new IllegalArgumentException("no match class of 'term'!");
		}catch(Exception e){
			System.err.println("term:"+term);
			System.err.println("type:"+type);
			if(term instanceof TyVar){
				TyVar var = (TyVar)term;
				System.err.println("var.type:"+var.getType());
				System.err.println("binder.type:"+var.getBinder().getType());
			}
			System.err.println("Ks:"+Ks_);
			System.err.println("Gs:"+Gs_);
			throw e;
		}
	}

	void addId(IdLike idLike){
		baseG_.put(idLike, baseG_.size());//ベースに追加
		//すでにGs_にidLikeが追加されるべきGが含まれていれば、そのGに追加
		for(Type t : Gs_.keySet()){
			if(idLike.getType().endWith(t)){
				Map<IdLike, Integer> G = Gs_.get(t);
				G.put(idLike, G.size());
			}
		}
	}

	void removeId(IdLike idLike){
		baseG_.remove(idLike);//ベースから取り除く
		//Gs_にあるGのうち、idLikeが（型から判断して）入っているGからidLikeを削除（末尾であるかもチェック）
		for(Type t : Gs_.keySet()){
			if(idLike.getType().endWith(t)){
				Map<IdLike, Integer> G = Gs_.get(t);
				if(!G.containsKey(idLike)) throw new IllegalArgumentException("idLike should be contained!");
				int index = G.remove(idLike);
				if(index != G.size()) throw new IllegalArgumentException("index should be the last!");
			}
		}
	}

	//updateでbaseK(,G)からforeachでnewK(,G)をつくるため、LinkedHashMapである必要がある
	private void updateKGs(Type type){
		LinkedHashMap<Type, Integer> newK = new LinkedHashMap<>();
		for(Type t : baseK_.keySet()){//baseKよりもtype.rightのKからとれればその方が速い(絞れている）
			if(t.endWith(type)) newK.put(t, newK.size());
		}
		Ks_.put(type, newK);

		LinkedHashMap<IdLike, Integer> newG = new LinkedHashMap<>();
		for(IdLike idLike : baseG_.keySet()){
			if(idLike.getType().endWith(type)) newG.put(idLike, newG.size());
		}
		Gs_.put(type, newG);
	}
	
	void print(){
		System.out.println("TypeIndexManager["+hashCode()+"]");
		System.out.println("baseK:"+baseK_);
		for(Type type : Ks_.keySet()){
			System.out.println("Ks:	type:"+type+"	K:"+Ks_.get(type));
		}
		System.out.println("baseG:"+baseG_);
		for(Type type : Gs_.keySet()){
			System.out.println("Gs:	type:"+type+"	G:"+Gs_.get(type));
		}
	}
}
