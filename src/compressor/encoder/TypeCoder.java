package compressor.encoder;


import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import main.Checker;
import util.MyUtil;

import component.tyterm.term.TyRoot;
import component.tyterm.type.Type;
import component.tyterm.type.TypeBitConverter;
import compressor.HOCompressor;


public final class TypeCoder {


	public static void outputConsTypeFile(TyRoot term, String fname){
		List<Type> typeList = new ArrayList<>();
		//シンボルの型をtermに保存された順にtypeListに追加
		if(!HOCompressor.COMP_AS_BINARY_TREE){
			Collection<Type> symbolTypes = term.getSymbolTypes();
			for(Type type : symbolTypes){
				typeList.add(type);
			}
		}

		outputTypeList(typeList, fname);
		System.out.println("TypeCoder#outputConsTypeFile	rawFile:"+(new File(fname)).length());
	}

	public static void outputTypeFile(TyRoot term, Map<Type, Integer> K, String fname) {
		List<Type> typeList = makeTypeList(K, term);
		outputTypeList(typeList, fname);
	}

	private static void outputTypeList(List<Type> typeList, String fname){
		byte[] bytes = TypeBitConverter.typeList2ByteList(typeList);

		String zipFile = fname+".zip";
		MyUtil.outputByteArray(bytes, fname);
		MyUtil.compressFileByZip(fname, zipFile);
		if(HOCompressor.DECODE_FILE_CHECK) System.out.println("TypeCoder#outputTypeFile	check:"+Checker.typeDecodeCheck(typeList, fname));
	}

	private static List<Type> makeTypeList(Map<Type, Integer> K, TyRoot term){
		List<Type> typeList = new ArrayList<Type>();
		typeList.add(term.getChild().getType());//λ式全体の型をリストの先頭に保存

		//Kの値の順に格納するため、一度配列に取り出してからtypeListに保存
		Type[] abstTypes = new Type[K.size()];
		for(Type type : K.keySet()) abstTypes[K.get(type)] = type;
		for(int i=0; i < abstTypes.length; i++){
			typeList.add(abstTypes[i]);
		}
		//シンボルの型をtermに保存された順にtypeListに追加
		if(!HOCompressor.COMP_AS_BINARY_TREE){
			Collection<Type> symbolTypes = term.getSymbolTypes();
			for(Type type : symbolTypes){
				typeList.add(type);
			}
		}

		return typeList;
	}
}
