package compressor.encoder;


import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import main.Checker;
import util.MyUtil;
import coder.trompcoder.TrompEncoder;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyVar;
import component.tyterm.type.Fun;
import component.tyterm.type.O;
import component.tyterm.type.Type;
import compressor.HOCompressor;


public final class EncoderImpl implements Encoder{
	
	public static void main(String[] args){
		TyRoot root = TyRoot.getRoot(null);
		Type o2o = new Fun(O.O, O.O);
		TySymbol c = new TySymbol(-1, O.O);
		TySymbol a = new TySymbol(-2, o2o);
		List<List<String>> nameListList = new ArrayList<>();
		List<String> nameListO = new ArrayList<>(); nameListO.add("c");
		List<String> nameListO2O = new ArrayList<>(); nameListO2O.add("a");
		nameListList.add(nameListO);
		nameListList.add(nameListO2O);
		root.addSymbolTypes(nameListList);

		TyAbst abstY = new TyAbst(false, 0, o2o);
		TyAbst abstX = new TyAbst(false, 2, new Fun(o2o, o2o));
		abstX.setChild(abstY);abstY.setParent(abstX);
		TyAbst abstF = new TyAbst(true, 3, new Fun(new Fun(o2o,o2o),O.O));
		
		TyVar x1 = abstX.makeVar();
		TyVar x2 = abstX.makeVar();
		TyVar y = abstY.makeVar();
		TyApp app1 = new TyApp(x1, new TyApp(x2, y));
		abstY.setChild(app1);app1.setParent(abstY);
		
		TyVar f = abstF.makeVar();
		TyApp app2 = new TyApp(new TyApp(f, a), c);
		abstF.setChild(app2);app2.setParent(abstF);
		
		TyApp app3 = new TyApp(abstF, abstX);
		root.setChild(app3);app3.setParent(root);
		
		System.out.println("EncoderImpl#main	root:"+root);
		System.out.println("EncoderImpl#main	validTermCheck:"+Checker.validTyTermCheck(root));
		
		EncoderImpl encoder = new EncoderImpl(root, "syuronnBench/test/", "test", true);
		encoder.encode();
	}

	private TyRoot root_;
	private String dir_;
	private String file_;
	private boolean typeClassifying_;
	
	public EncoderImpl(TyRoot root, String dir, String file, boolean typeClassifying){
		root_ = root;
		dir_ = dir;
		file_ = file;
		typeClassifying_ = typeClassifying;
	}

	@Override
	public void encode(){
		LinkedHashMap<Type, Integer> K = typeClassifying_ ? TypeClassifyingEnc.makeK(root_) : UniformEnc.makeK(root_);
		String outputDir = dir_+file_+"_output/";
		String outputRawDir = outputDir+"raw/";
		MyUtil.makeDirIfNotExists(outputDir);
		MyUtil.makeDirIfNotExists(outputRawDir);

		String labelFile = outputRawDir+file_+".label";
		String typeFile = outputRawDir+file_+".ty";
		String consFile = outputRawDir+file_+".cons";
		String yrcFile = outputDir+file_+".label.yrc";
		
		String constyFile = outputDir+file_+".consty";
		
		

		long consFileLen = ConsCoder.outputConsFile(root_, consFile);
		TypeCoder.outputTypeFile(root_, K, typeFile);
		TypeCoder.outputConsTypeFile(root_, constyFile);

		/*
		 * 圧縮とその時間計測
		 */
		long realStart0, realEnd0;
		long cpuStart0, cpuEnd0;
		long userStart0, userEnd0;
		ThreadMXBean bean = ManagementFactory.getThreadMXBean();

		long millis = 0 * 1000;
		MyUtil.sleepMillis(millis);

		if(HOCompressor.CHECK_MAX_MEMORY) Checker.updateMaxMemory();

		cpuStart0 = bean.getCurrentThreadCpuTime();
		userStart0 = bean.getCurrentThreadUserTime();
		realStart0 = System.currentTimeMillis();

		
		List<Integer> db = typeClassifying_ ? TypeClassifyingEnc.term2DB(root_, K) : UniformEnc.term2DB(root_, K);

		
		cpuEnd0 = bean.getCurrentThreadCpuTime();
		userEnd0 = bean.getCurrentThreadUserTime();
		realEnd0 = System.currentTimeMillis();
		HOCompressor.printLog("ENC_ELAPSED_TIME(REAL):	"+(realEnd0-realStart0)/(double)1000);
		HOCompressor.printLog("ENC_ELAPSED_TIME(CPU):	"+(cpuEnd0-cpuStart0)/((double)1000*1000*1000));
		HOCompressor.printLog("ENC_ELAPSED_TIME(USER):	"+(userEnd0-userStart0)/((double)1000*1000*1000));

		if(HOCompressor.CHECK_MAX_MEMORY) Checker.updateMaxMemory();

		long realStart1, realEnd1;
		long cpuStart1, cpuEnd1;
		long userStart1, userEnd1;
		cpuStart1 = bean.getCurrentThreadCpuTime();
		userStart1 = bean.getCurrentThreadUserTime();
		realStart1 = System.currentTimeMillis();

		LabelCoder.outputLabelFile(db, labelFile, yrcFile);


		cpuEnd1 = bean.getCurrentThreadCpuTime();
		userEnd1 = bean.getCurrentThreadUserTime();
		realEnd1 = System.currentTimeMillis();
		HOCompressor.printLog("OUTPUT_ELAPSED_TIME(REAL):	"+(realEnd1-realStart1)/(double)1000);
		HOCompressor.printLog("OUTPUT_ELAPSED_TIME(CPU):	"+(cpuEnd1-cpuStart1)/((double)1000*1000*1000));
		HOCompressor.printLog("OUTPUT_ELAPSED_TIME(USER):	"+(userEnd1-userStart1)/((double)1000*1000*1000));
		
		HOCompressor.printLog("TOTAL_ENCODE_TIME(REAL):	"+(realEnd0-realEnd0 + realEnd1-realStart1)/(double)1000);
		HOCompressor.printLog("TOTAL_ENCODE_TIME(CPU):	"+(cpuEnd0-cpuStart0 + cpuEnd1-cpuStart1)/((double)1000*1000*1000));
		HOCompressor.printLog("TOTAL_ENCODE_TIME(USER):	"+(userEnd0-userStart0 + userEnd1-userStart1)/((double)1000*1000*1000));

		if(HOCompressor.CHECK_MAX_MEMORY) Checker.updateMaxMemory();

		long labelFileLen = (new File(yrcFile)).length();
		long typeFileLen = Math.min((new File(typeFile)).length(), (new File(typeFile+".zip")).length());
		long constyFileLen = Math.min((new File(constyFile)).length(), (new File(constyFile+".zip")).length());
		HOCompressor.printLog("LABEL_FILE_SIZE:	"+labelFileLen);
		HOCompressor.printLog("CONS_FILE_SIZE:	"+consFileLen);
		HOCompressor.printLog("TYPE_FILE_SIZE:	"+typeFileLen);
		HOCompressor.printLog("TOTAL_FILE_SIZE:	"+(labelFileLen+consFileLen+typeFileLen));
		HOCompressor.printLog("CONSTY_FILE_SIZE:	"+constyFileLen);
		
		long yaguchiBits = (labelFileLen + typeFileLen)*8;
		double yaguchiRatio = yaguchiBits / (double)root_.getChild().size();
		HOCompressor.printLog("YAGUCHI_SIZE(bits):	"+constyFileLen);
		HOCompressor.printLog("YAGUCHI_RATIO(bits/size):	"+yaguchiRatio);
		
		long realStartT, realEndT;
		long cpuStartT, cpuEndT;
		long userStartT, userEndT;
		cpuStartT = bean.getCurrentThreadCpuTime();
		userStartT = bean.getCurrentThreadUserTime();
		realStartT = System.currentTimeMillis();

		long trompBytes = TrompEncoder.trompCompSizeOf(root_, outputDir+file_);

		cpuEndT = bean.getCurrentThreadCpuTime();
		userEndT = bean.getCurrentThreadUserTime();
		realEndT = System.currentTimeMillis();
		HOCompressor.printLog("TROMP_TIME(REAL):	"+(realEndT-realStartT)/(double)1000);
		HOCompressor.printLog("TROMP_TIME(CPU):	"+(cpuEndT-cpuStartT)/((double)1000*1000*1000));
		HOCompressor.printLog("TROMP_TIME(USER):	"+(userEndT-userStartT)/((double)1000*1000*1000));
		
		double trompRatio = trompBytes * 8 / (double)root_.getChild().size();
		
		HOCompressor.printLog("TROMP_SIZE(bits):	"+trompBytes*8);
		HOCompressor.printLog("TROMP_SIZE(byts):	"+trompBytes);
		HOCompressor.printLog("TROMP_RATIO(bits/size):	"+trompRatio);
		

		if(HOCompressor.CHECK_MAX_MEMORY) Checker.updateMaxMemory();
	}

}
