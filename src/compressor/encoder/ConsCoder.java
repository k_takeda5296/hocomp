package compressor.encoder;


import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import main.Checker;
import util.MyUtil;
import component.tyterm.term.TyRoot;
import compressor.HOCompressor;


public final class ConsCoder {


	public static long outputConsFile(TyRoot term, String fname) {
		List<String> symbolNames = term.getSymbolNames();
		return HOCompressor.COMP_AS_BINARY_TREE ? outputConsFileBin(symbolNames, fname) : outputConsFile(symbolNames, fname);
	}
	
	public static long outputConsFile(Collection<String> symbolNames, String fname) {
		List<Byte> byteList = symbolNames2ByteList(symbolNames);
		byte[] bytes = new byte[byteList.size()];
		for(int i=0; i < byteList.size(); i++){
			bytes[i] = byteList.get(i);
		}

		String zipFile = fname+".zip";
		MyUtil.outputByteArray(bytes, fname);
		MyUtil.compressFileByZip(fname, zipFile);

		if(HOCompressor.DECODE_FILE_CHECK){
			List<String> consList = new ArrayList<String>(symbolNames);
			System.out.println("ConsCoder#outputConsFile	check:"+Checker.consDecodeCheck(consList, fname));
		}
		
		return Math.min((new File(fname).length()), (new File(zipFile)).length());
	}
	
	private static List<Byte> symbolNames2ByteList(Collection<String> symbolNames){
		List<Byte> byteList = new ArrayList<Byte>();
		for(String name : symbolNames){
			byte[] b = name.getBytes();
			for(int j=0; j < b.length; j++){
				byteList.add(b[j]);
			}
			byteList.add((byte)0);
		}
		return byteList;
	}
	
	private static long outputConsFileBin(Collection<String> symbolNames, String fname){
		List<String> symbolNames00 = new ArrayList<>();
		List<String> symbolNames01 = new ArrayList<>();
		List<String> symbolNames10 = new ArrayList<>();
		List<String> symbolNames11 = new ArrayList<>();
		for(String symbolName : symbolNames){
			String name = symbolName.substring(2);
			String childNum = symbolName.substring(0,2);
			if(childNum.equals("00")){
				symbolNames00.add(name);
			}else if(childNum.equals("01")){
				symbolNames01.add(name);
			}else if(childNum.equals("10")){
				symbolNames10.add(name);
			}else if(childNum.equals("11")){
				symbolNames11.add(name);
			}else{
				throw new IllegalArgumentException();
			}
		}
		long size00 = outputConsFile(symbolNames00, fname+"00");
		long size01 = outputConsFile(symbolNames01, fname+"01");
		long size10 = outputConsFile(symbolNames10, fname+"10");
		long size11 = outputConsFile(symbolNames11, fname+"11");
		return size00 + size01 + size10 + size11;
	}
}
