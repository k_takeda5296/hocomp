package compressor;


import java.io.File;
import java.io.FileNotFoundException;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import main.Checker;
import util.CommandLineParser;
import util.MyLogger;
import coder.tytermcoder.TyTermCoder;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyRoot;
import component.tyterm.type.subdivider.OldTypeSubdivider;
import component.tyterm.type.subdivider.TypeSubdivider;
import compressor.converter.BetaExpansion;
import compressor.converter.Compressor;
import compressor.converter.RecursiveExpander;
import compressor.converter.RecursiveExpanderIncl;
import compressor.converter.SimplifyIncl;
import compressor.encoder.Encoder;
import compressor.encoder.EncoderImpl;
import compressor.loader.DnaTextLoader;
import compressor.loader.EnTextLoader;
import compressor.loader.Loader;
import compressor.loader.XmlLoader;


public final class HOCompressor {

	//parserの設定
	public static enum MODE { XML, EN_TEXT, DNA_TEXT, DNA_JOIN, PROG_TEXT };


	//プロファイリングしたいときに待つ秒数（5秒くらいあると良い）
	public static final int WAIT_MILLIS = 6 * 1000;

	//ファイルにログを出力するか
	public static final boolean LOGGING_INTO_FILE = true;
	public static MyLogger LOGGER;

	//圧縮前のλ式を出力するかどうか
	public static boolean PRINT_ORIGINAL_TERM = false;

	//圧縮後のλ式を出力するかどうか
	public static boolean PRINT_COMPRESSED_TERM = false;
	//上記の出力時に高階なパターンのみを表示
	public static final boolean PRINT_ONLY_HO_PATTERN = false;
	//上記の出力時に同値穴を持つパターンのみを表示
	public static final boolean PRINT_ONLY_EH_PATTERN = false;

	public static final boolean PRINT_ABST_TYPES = false;


	public static final boolean SHOW_CHOOSED_CONTEXTS = false;


	//解凍結果をファイルに書き出すか
	public static final boolean OUTPUT_DECOMP = false;

	//圧縮ファイルから読み出した値が書き出した値と一致するかチェック
	public static final boolean DECODE_FILE_CHECK = false;

	//圧縮後のTyTermと、それをエンコードしたファイルから読み出してデコードしたTyTermが等しいかチェック
	public static final boolean DECODE_CHECK = true;

	//デコードしたtermを簡約したものが元ファイルのtermと等しいかチェック
	public static final boolean REDUCE_CHECK = false;

	//ポインタが双方向になっているかをチェック
	public static final boolean BIDIRECTIONAL_CHECK = false;

	//ポインタが双方向になっているか、束縛が外れていないか、正しく型付けされているかチェック
	public static final boolean TERM_VARIDITY_CHECK = true;

	//メモリ使用量をチェック
	public static final boolean CHECK_MAX_MEMORY = false;
	public static final int CHECKING_TERM = 4000;

	public static final boolean CHECK_RANGE_CODER = true;

	//部分木をすべてシンボルに置き換えるか
	public static final boolean REPLACE_ALL_SUBTREE = false;

	//同値穴を持つ文脈を、ラベルサイズが減少しなくても抜き出すか
	public static final boolean PARTIALITY_TO_EQUIVALENT_HOLE = false;

	public static final boolean CONTEXT_HASH_CONFLICTS_CHECK = false;

	public static final boolean CHOOSE_0_PRIORITY_CONTEXT = false;
	public static final int PRIORITY_BORDER = -5;

	public static final boolean USE_VARIABLE_BYTE = false;

	public static final boolean PRINT_CMD = false;

	public static final boolean DISTINGUISH_CONTEXT_BY_HOLE = false;

	public static final boolean TYPE_CLASSIFYING_ENCODE = false;//Remark 3.2

	private static final boolean SUBDIVIDE_TYPES = false;
	private static final boolean USE_OLD_SUBDIVIDER = true;

	public static final boolean SPLIT_FILE_BY_PERIOD = true;

	private static final boolean INCL_CONTEXT_SIZE = true;

	public static final boolean CONSIDER_ONLY_BODY = true;//Pruneするか
	public static final int PRUNE_BORDER = 0;

	private static final boolean CHECK_HAS_EXPANSION_IN_PATTERNS = true;
	
	public static boolean COMP_AS_BINARY_TREE = true;
	
	public static boolean SEQUENTIAL_XML_FOR_TEXTS = true;

	public static final String OPT_FILE = "-f";
	public static final String OPT_DNA_TEXT = "-dna_text";
	public static final String OPT_EN_TEXT = "-en_text";
	public static final String OPT_PROG_TEXT = "-prog_text";
	public static final String OPT_CSIZE = "-c";
	public static final String OPT_PBORDER = "-pborder";
	public static final String OPT_LOG = "-log";

	private static final String USAGE = "\n" +
			OPT_FILE+"	圧縮するファイル（英文テキストの時はディレクトリ）のパス\n" +
			OPT_DNA_TEXT+"	対象がDNAテキストの時に長さを指定（値は長さ）\n" + 
			OPT_EN_TEXT+"	対象が英文テキストの時に指定（値は最大ファイル数）\n" + 
			OPT_PROG_TEXT+"	対象がプログラムのソースコード（Python除く）の時に指定（値なし）\n" + 
			OPT_CSIZE+"	文脈の最大ノード数\n" +
			OPT_LOG+"	ログファイル名を指定する\n";





	/*
	 * メイン関数
	 */
	public static void main(String[] args){

		CommandLineParser clp = new CommandLineParser()
		.addOption(OPT_FILE, true)
		.addOption(OPT_DNA_TEXT)
		.addOption(OPT_EN_TEXT)
		.addOption(OPT_PROG_TEXT)
		.addOption(OPT_CSIZE, true)
		.addOption(OPT_PBORDER, true)
		.addOption(OPT_LOG);
		if(!clp.parse(args)) throw new IllegalArgumentException("invalid options!" + System.getProperty("line.seperator") + USAGE);
		Loader loader = makeLoader(clp);
		//System.out.println("HOCompressor#main	args:"+Arrays.toString(args));

		init();
		String logFileName = clp.hasOptionValue(OPT_LOG) 
				? clp.getOptionValue(OPT_LOG) 
						: (new SimpleDateFormat("yyyyMMdd-HHmmss")).format(Calendar.getInstance().getTime())+"_"+loader.getFile()+"_"+clp.getOptionValue(OPT_CSIZE)+".comp.log";
				if(LOGGING_INTO_FILE) LOGGER = new MyLogger(logFileName);
				HOCompressor main = new HOCompressor();
				try {
					int cSize = Integer.parseInt(clp.getOptionValue(OPT_CSIZE));
					if(cSize < 1) throw new NumberFormatException();
					int pBorder = Integer.parseInt(clp.getOptionValue(OPT_PBORDER));
					main.execute(loader, cSize, pBorder);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch(NumberFormatException e){
					System.err.println(OPT_CSIZE+" should take positive integer!");
					e.printStackTrace();
				}finally{
					if(LOGGING_INTO_FILE) LOGGER.close();
				}
	}

	public static void init(){
		BetaExpansion.init();
		SimplifyIncl.init();
		TyAbst.init();
	}

	/*
	 * ローダーを作る
	 */
	private static Loader makeLoader(CommandLineParser clp){
		String xmlPath = checkOption(clp);
		String dir = (new File(xmlPath)).getParent() + "/";
		String file = (new File(xmlPath)).getName();
		return new XmlLoader(dir, file);
	}

	public static String checkOption(CommandLineParser clp){
		String oriPath = clp.getOptionValue(OPT_FILE);
		if(clp.hasOption(OPT_DNA_TEXT)){
			if(clp.hasOption(OPT_EN_TEXT)) throw new IllegalArgumentException();
			if(clp.hasOption(OPT_PROG_TEXT)) throw new IllegalArgumentException();
			if(clp.getOptionValue(OPT_DNA_TEXT)==null) throw new IllegalArgumentException();
			int maxNum = Integer.parseInt(clp.getOptionValue(OPT_DNA_TEXT));
			if(maxNum < 1) throw new IllegalArgumentException();
			return DnaTextLoader.confirmXml(oriPath, maxNum);
		}else if(clp.hasOption(OPT_EN_TEXT)){
			if(clp.hasOption(OPT_DNA_TEXT)) throw new IllegalArgumentException();
			if(clp.hasOption(OPT_PROG_TEXT)) throw new IllegalArgumentException();
			if(clp.getOptionValue(OPT_EN_TEXT)==null) throw new IllegalArgumentException();
			int maxNum = Integer.parseInt(clp.getOptionValue(OPT_EN_TEXT));
			if(maxNum < 1) throw new IllegalArgumentException();
			return EnTextLoader.confirmXml(oriPath, maxNum, SEQUENTIAL_XML_FOR_TEXTS);
		}else if(clp.hasOption(OPT_PROG_TEXT)){
			if(clp.hasOption(OPT_DNA_TEXT)) throw new IllegalArgumentException();
			if(clp.hasOption(OPT_EN_TEXT)) throw new IllegalArgumentException();
			return EnTextLoader.confirmXml(oriPath, SEQUENTIAL_XML_FOR_TEXTS);
		}
		return oriPath;
	}


	/*
	 * ロガー関数
	 */
	public static void printLog(String str){
		if(LOGGER==null){
			System.out.println("LOGGER is null!");
			return;
		}
		if(LOGGING_INTO_FILE) LOGGER.println(str);
		System.out.println(str);
	}
	public static void printLog(Object obj){
		if(LOGGER==null){
			System.out.println("LOGGER is null!");
			return;
		}
		if(LOGGING_INTO_FILE) LOGGER.println(obj);
		System.out.println(obj);
	}


	/*
	 * メインの実行関数。時間の計測や実験値の書き出しをしている
	 */
	public void execute(Loader loader, int cSize, int pBorder) throws FileNotFoundException, InterruptedException{
		String dir = loader.getDir();
		String file = loader.getFile();
		/********************************************************************************
		 * xmlファイルを読み込んでλ式を得る
		 ********************************************************************************/
		TyRoot term = loader.parsedTerm();
		//term.printTermInfo();

		//圧縮前のλ式を出力
		if(PRINT_ORIGINAL_TERM){
			printLog("ORIGINAL_TERM:");
			printLog(term.getChild());
			printLog(term.getAllVarTypesAsString());
		}
		int oriLabelSize = term.labelSize();

		//ファイル名とラベルサイズを出力
		System.out.println("FILE_NAME:	"+dir+file+"	"+oriLabelSize);

		if(TERM_VARIDITY_CHECK){
			System.out.println("Main#execute	validTermCheck:"+Checker.validTyTermCheck(term));
		}

		/********************************************************************************
		 * 圧縮
		 ********************************************************************************/
		Compressor compressor = INCL_CONTEXT_SIZE
				? new RecursiveExpanderIncl(term, 5, cSize, pBorder)
		: new RecursiveExpander(term, cSize);

				/*
				 * 圧縮とその時間計測
				 */
				long realStart, end;
				long cpuStart, cpuEnd;
				long userStart, userEnd;
				ThreadMXBean bean = ManagementFactory.getThreadMXBean();
				cpuStart = bean.getCurrentThreadCpuTime();
				userStart = bean.getCurrentThreadUserTime();
				realStart = System.currentTimeMillis();

				//圧縮メイン
				TyRoot compressedTerm = compressor.getTerm();

				cpuEnd = bean.getCurrentThreadCpuTime();
				userEnd = bean.getCurrentThreadUserTime();
				end = System.currentTimeMillis();
				/*
				 * 時間計測終わり
				 */

				/*
				 * パターン内にβ展開の形が残っているか
				 */
				if(CHECK_HAS_EXPANSION_IN_PATTERNS){
					try{
						Checker.checkHasExpansionInPatterns(compressedTerm, false, pBorder);
					}catch(Exception e){
						printLog("COMPRESSED_TERM:");
						printLog(compressedTerm.getChild().toStringAsFunctionalProgram(compressedTerm));
						printLog(compressedTerm.getAllVarTypesAsString());
						throw e;
					}
				}

				/*
				 * β簡約の代替
				 */
				if(TERM_VARIDITY_CHECK){
					System.out.println("Main#execute	validTermCheck:"+Checker.validTyTermCheck(compressedTerm));
				}



				/********************************************************************************
				 * 型を細分化
				 ********************************************************************************/
				if(SUBDIVIDE_TYPES){
					if(USE_OLD_SUBDIVIDER) OldTypeSubdivider.subdivideTypes(compressedTerm);
					else TypeSubdivider.subdivideTypes(compressedTerm);

				}


				//printPatterns(compressedTerm);


				long realTime = end - realStart;
				long cpuTime = cpuEnd - cpuStart;
				long userTime = userEnd - userStart;
				/*
				 * ログ出力
				 */
				outputTermInfo(compressedTerm, dir, file, oriLabelSize, realTime, cpuTime, userTime, file+".log");

				/********************************************************************************
				 * エンコードと書き出し
				 ********************************************************************************/
				//エンコードと書き出し
				Encoder encoder = new EncoderImpl(compressedTerm, dir, file, TYPE_CLASSIFYING_ENCODE);
				encoder.encode();

				Encoder termCoder = new TyTermCoder(dir, file, compressedTerm);
				termCoder.encode();

				/*
		Encoder ltermCoder = new TyTermCoder(dir, file+".let", TyTermUtil.treatLetOrder(compressedTerm));
		ltermCoder.encode();
				 */

				if(CHECK_MAX_MEMORY) System.out.println("MAX_MEMORY:	"+Checker.MAX_MEMORY/(1000*1000)+"	intList:"+Checker.intList);//+" MB");

	}

	public static void outputTermInfo(TyRoot root, String dir, String file, int oriSize, long realTime, long cpuTime, long userTime, String logFileName){
		if(LOGGER==null) LOGGER = new MyLogger(logFileName);

		//圧縮後のλ式を出力
		if(PRINT_COMPRESSED_TERM){
			printLog("COMPRESSED_TERM:");
			printLog(root.getChild().toStringAsFunctionalProgram(root));
			printLog(root.getAllVarTypesAsString());
		}

		printLog("FILE_NAME:	"+dir+file+"	"+oriSize);
		//実験値の出力
		printLog("ORIGINAL_LABEL_SIZE:	"+oriSize);
		printLog("COMPRESSED_NODE_SIZE:	"+root.getChild().size());
		printLog("COMPRESSED_LABEL_SIZE:	"+root.labelSize());
		printLog("ELAPSED_TIME(REAL):	"+realTime/(double)1000);
		printLog("ELAPSED_TIME(CPU):	"+cpuTime/((double)1000*1000*1000));
		printLog("ELAPSED_TIME(USER):	"+userTime/((double)1000*1000*1000));
		printLog("BETA_EX_TIMES:	"+BetaExpansion.BETA_EX_TIMES);

		//simplifyの実験値
		printLog("SIMPLIFY_CHECKS:	"+SimplifyIncl.CHECKTIMES());
		printLog("SIMPLIFY_TIMES:	"+SimplifyIncl.SIMPLIFYTIMES());
		printLog("SIMPLIFY_TIMES_BETA_ONCE:	"+SimplifyIncl.BETAONCETIMES());
		printLog("SIMPLIFY_TIMES_BETA_ID:	"+SimplifyIncl.BETAIDTIMES());
		printLog("SIMPLIFY_TIMES_ETA:	"+SimplifyIncl.ETATIMES());
	}
}