package compressor.converter;


import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyRoot;
import compressor.HOCompressor;
import compressor.converter.tables.Tables;


/*
 * β簡約の条件を「ラベルサイズが減る」ことにするべき
 */
public final class SimplifyIncl{

	private int checkTimes_ = 0;
	private int simplifyTimes_ = 0;
	private int simplifyTimesBetaOnce_ = 0;
	private int simplifyTimesBetaId_ = 0;
	private int simplifyTimesEta_ = 0;
	
	public SimplifyIncl(){}
	
	private static SimplifyIncl INSTANCE = new SimplifyIncl();
	
	public static void init() {
		INSTANCE = new SimplifyIncl();
	}
	
	public static void simplifyAll(TyRoot root, Collection<TyAbst> newAbsts, int contextSize){
		System.out.println("SimplifyIncl#simplifyAll	newAbsts.size:"+newAbsts.size());
		simplify(root, null, newAbsts, contextSize);
	}
	
	public static TyRoot simplify(TyRoot term, Tables tables, Collection<TyAbst> simplifyQueue, int contextSize) {
		return INSTANCE.simplifyy(term, tables, simplifyQueue, contextSize);
	}

	private static boolean FLAG = false;
	public TyRoot simplifyy(TyRoot term, Tables tables, Collection<TyAbst> simplifyQueue, int contextSize) {
		//System.out.println("Simplify#simplify	simplifyQueue.size:"+simplifyQueue.size());
		Set<TyAbst> additionalAbstsToSimplify = new LinkedHashSet<TyAbst>();
		for(TyAbst abst : simplifyQueue){
			checkTimes_++;
			if(BetaReduction.isBetaReducibleWithOnce(abst)){
				if(FLAG) HOCompressor.printLog("Simplify#betaReductionWithOnce	abst:"+abst.getName());
				//Main.printLog(term.toStringAsFunctionalProgram());
				BetaReduction.betaReductionWithOnce(abst, tables, additionalAbstsToSimplify, contextSize);
				simplifyTimes_++;
				simplifyTimesBetaOnce_++;
			}else if(BetaReduction.isBetaReducibleWithId(abst)){
				if(FLAG) HOCompressor.printLog("Simplify#betaReductionWithId	abst:"+abst.getName());
				//Main.printLog(term.toStringAsFunctionalProgram());
				BetaReduction.betaReductionWithId(abst, tables, additionalAbstsToSimplify, contextSize);
				simplifyTimes_++;
				simplifyTimesBetaId_++;
			}else if(EtaReduction.isEtaReducible(abst)){
				if(FLAG) HOCompressor.printLog("Simplify#etaReuction:"+abst.getName());
				//Main.printLog(term.toStringAsFunctionalProgram());
				EtaReduction.etaReduction(abst, tables, additionalAbstsToSimplify, contextSize);
				simplifyTimes_++;
				simplifyTimesEta_++;
			}else{
			}
		}
		return additionalAbstsToSimplify.size() > 0 ? simplify(term, tables, additionalAbstsToSimplify, contextSize) : term;
	}
	
	public static int CHECKTIMES(){
		return INSTANCE.getCheckTimes();
	}
	public int getCheckTimes() {
		return checkTimes_;
	}
	
	public static int SIMPLIFYTIMES(){
		return INSTANCE.getSimplifyTimes();
	}
	public int getSimplifyTimes() {
		return simplifyTimes_;
	}
	
	public static int BETAONCETIMES(){
		return INSTANCE.getSimplifyTimesBetaOnce();
	}
	public int getSimplifyTimesBetaOnce() {
		return simplifyTimesBetaOnce_;
	}
	
	public static int BETAIDTIMES(){
		return INSTANCE.getSimplifyTimesBetaId();
	}
	public int getSimplifyTimesBetaId() {
		return simplifyTimesBetaId_;
	}
	
	public static int ETATIMES(){
		return INSTANCE.getSimplifyTimesEta();
	}
	public int getSimplifyTimesEta() {
		return simplifyTimesEta_;
	}

}
