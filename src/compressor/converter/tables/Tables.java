package compressor.converter.tables;


import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;

import main.Main;
import component.context.Context;
import component.context.ContextFactory;
import component.occurrenceset.OccurrenceSet;
import component.tyterm.TyTermIterFunc;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TyTerm;
import compressor.HOCompressor;



/*
 * TyTermを破壊しないぞ！
 * １、文脈＝＞文脈オブジェクトへのハッシュ
 * ２、文脈オブジェクトの入った優先度付きキュー
 * ３、文脈オブジェクト＝＞ノード集合のハッシュ（occurence listsにあたる）
 */

/*
 * TODO: 写像H（contextHash_）をクラスとして実装して分離する
 */

public final class Tables {

	private static final boolean USE_QUEUE_NON_HOLE = HOCompressor.DISTINGUISH_CONTEXT_BY_HOLE;

	private PriorityUpdatableQueue<Context> queueWithHole_;
	private PriorityUpdatableQueue<Context> queueNonHole_;
	private OccurrenceMap occMap_;
	private int contextSize_;


	public Tables(TyRoot root, int contextSize){
		queueWithHole_ = new AmatizedPriorityQueue<Context>((int)Math.sqrt(root.getChild().labelSize()));
		if(USE_QUEUE_NON_HOLE) queueNonHole_ = new AmatizedPriorityQueue<Context>((int)Math.sqrt(root.getChild().labelSize()));
		occMap_ = new OccurrenceMap();
		contextSize_ = contextSize;

		initTables(root);
	}

	private void initTables(TyRoot root){
		LinkedHashSet<TyApp> newNodes = new LinkedHashSet<>();
		root.getChild().iterPostOreder(new TyTermIterFunc(){
			@Override
			public void iterFunc(TyTerm term) {
				if(!(term instanceof TyApp)) return;
				TyApp app = (TyApp)term;

				app.countContext(contextSize_);
				newNodes.add(app);
			}
		});
		update(new LinkedHashSet<TyApp>(), new LinkedHashSet<TyApp>(), newNodes);
		//System.out.println("initTables:	symbols:"+root.getAllSymbolTypesAsString());
	}


	static final boolean FLAG0 = false;
	static final boolean FLAG1 = false;
	public void update(LinkedHashSet<TyApp> nodesToRemove, LinkedHashSet<TyApp> nodesToUpdate, LinkedHashSet<TyApp> newNodes) {
		if(FLAG0){
			System.out.println("Tables#update	nR:"+nodesToRemove);
			System.out.println("Tables#update	nU:"+nodesToUpdate);
			System.out.println("Tables#update	nN:"+newNodes);
		}
		//削除ノードは再計算の必要なし
		nodesToUpdate.removeAll(nodesToRemove);
		//また、newNodesは文脈を持っておらずremoveできない
		nodesToUpdate.removeAll(newNodes);
		if(FLAG0){
			System.out.println("Tables#update	nR:"+nodesToRemove);
			System.out.println("Tables#update	nU:"+nodesToUpdate);
			System.out.println("Tables#update	nN:"+newNodes);
		}
		

		//occMap_.removeNodesを一回で行わないと文脈の2回削除が起こるため、まとめる（本当？文脈の削除が起こるのはoccSetが空になったときなので、起こらない気がする）。また、OccExSetでは現在（11/14）の実装では抜き出せるノードしか保持していないが、弾かれたノードがまだ存在するのにisEmpty()がtrueになるのは直したほうがいいと思う。と、ここまで書いてそれこそが原因だと分かった。
		/*
		 * OccExSetの実装の問題の対処法としては、
		 * １、OccExSetで弾かれたノードも別の集合に保存しておき、両方の集合が空になったときに初めてisEmpty()をtrueとする
		 * ２、OccExSetで弾かれたノードの文脈集合からその文脈を削除する（TreeRePairと同等）
		 * 
		 * １番だと、抜き出せるようになっているノードをいつまでも保持し続けている無駄がある（もしかしたら定期的に別集合をチェックして大丈夫なものを抜き出せる集合に移動させる高速な（償却線形とかの）アルゴリズムがあるかもしれないかもしれないので、それを思いついたらこっちがいいと思われる）
		 * ２番だと、TreeRePairと同様の処理であり、無難といえば無難
		 */
		Collection<Context> changedContexts = new HashSet<Context>();
		//削除・更新ノードの削除
		final int id = Main.ID;
		if(FLAG1) System.out.println("Tables#update	nR.contians?:"+nodesToRemove.parallelStream().anyMatch(t -> t.getId()==id));
		changedContexts.addAll(occMap_.removeNodes(nodesToRemove));
		if(FLAG1) System.out.println("Tables#update	nU.contians?:"+nodesToUpdate.parallelStream().anyMatch(t -> t.getId()==id));
		changedContexts.addAll(occMap_.removeNodes(nodesToUpdate));
		
		if(FLAG1) System.out.println("Tables#update	nN.contians?:"+newNodes.parallelStream().anyMatch(t -> t.getId()==id));

		//生成・更新ノードの追加
		newNodes.forEach(app -> app.countContext(contextSize_));
		nodesToUpdate.forEach(app -> app.countContext(contextSize_));
		changedContexts.addAll(occMap_.addNodes(newNodes));
		changedContexts.addAll(occMap_.addNodes(nodesToUpdate));

		//キューの更新
		updateQueue(queueWithHole_, changedContexts);
		if(USE_QUEUE_NON_HOLE) updateQueue(queueNonHole_, changedContexts);
	}

	private void updateQueue(PriorityUpdatableQueue<Context> queue, Collection<Context> changedContexts){
		changedContexts.forEach(c ->{ 
			if(occMap_.containsKey(c)){
				queue.enqueue(c, occMap_.calcPriority(c));
			}else{
				queue.removeElement(c);
				ContextFactory.removeContext(c);
			}
		});
	}


	public boolean hasContextWithHoleToReplace(){
		return queueWithHole_.hasNext();
	}

	public boolean hasContextNonHoleToReplace(){
		return USE_QUEUE_NON_HOLE && queueNonHole_.hasNext();
	}

	public OccurrenceSet getContextWithHoleToReplace(){
		/*
		 * 謎のエラーの正体は、segmentからノードをremoveするときにsegMapがそのノードを端にもつsegumentを含まないことによるエラー。
		 * これは削除されるノードがpost-orderになっていないことにより起こる。
		 * しかしβ展開時の削除ノードの集め方から、削除ノードを単純にpost-orderにすることは難しいため、（要出典）
		 * ここでcontextHashからremoveしてしまうことで抜き出した文脈についてはすべて削除することにしている。
		 * 代わりに、removeNodeFromTablesではnullチェックを強いられているんだ！
		 */
		OccurrenceSet occSet = getOccSetFromQueue(queueWithHole_);
		return occSet;
	}

	public OccurrenceSet getContextNonHoleToReplace(){
		if(!USE_QUEUE_NON_HOLE) throw new IllegalArgumentException("No queues for no-holes contexts!");
		return getOccSetFromQueue(queueNonHole_);
	}

	private OccurrenceSet getOccSetFromQueue(PriorityUpdatableQueue<Context> queue){
		return occMap_.get(queue.peek());
	}

}
