package compressor.converter.tables;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import component.context.Context;
import component.occurrenceset.OccurrenceSet;
import component.occurrenceset.OccurrenceSetFactory;
import component.tyterm.term.TyApp;
import compressor.HOCompressor;

/*
 * 写像Hの役割を持つ
 * OccurrenceSetは隠す方針
 */
public class OccurrenceMap {

	Map<Context, OccurrenceSet> map_ = new HashMap<>();

	/**
	 * 削除・更新ノードを受け取って、それらをOccurrenceSetから削除し、優先度の変更があった文脈を返す。
	 * @param removeNodes	削除・更新ノードの集合
	 * @return 優先度の変更があった文脈の集合
	 */
	Set<Context> removeNodes(Collection<TyApp> removeNodes){
		Set<Context> changedContexts = new HashSet<>();
		Map<Context, Set<TyApp>> rNodeMap = makeRNodeMap(removeNodes);
		for(Context context : rNodeMap.keySet()){
			if(!map_.containsKey(context)){
				System.err.println("OccMap#removeNodes	nodes:"+rNodeMap.get(context));
				throw new IllegalArgumentException("No occSet are mapped for the context:"+context);
			}
			OccurrenceSet occSet = map_.get(context);
			occSet.removeNodes(rNodeMap.get(context));
			if(occSet.isEmpty()){
				map_.remove(context);
				//System.out.println("OccMap#removeNodes	context:"+context);
			}
			changedContexts.add(context);
		}
		return changedContexts;
	}

	private Map<Context, Set<TyApp>> makeRNodeMap(Collection<TyApp> rNodes){
		Map<Context, Set<TyApp>> rNodeMap = new LinkedHashMap<>();
		for(TyApp node : rNodes){
			for(Context c : node.getContexts()){
				if(!rNodeMap.containsKey(c)) rNodeMap.put(c, new HashSet<>());
				rNodeMap.get(c).add(node);
			}
		}
		return rNodeMap;
	}

	/**
	 * 生成・更新ノード（持っている文脈は計算済み）を受け取って、それらをOccurrenceSetから削除し、変更があった文脈を返す。
	 * @param addNodes	生成・更新ノードの集合
	 * @return 変更があった文脈の集合
	 */
	Collection<Context> addNodes(Collection<TyApp> addNodes){
		
		Collection<Context> changedContexts = new HashSet<>();
		for(TyApp node : addNodes){
			List<Context> removeContext = new ArrayList<>();
			for(Context context : node.getContexts()){
				if(!map_.containsKey(context)){
					map_.put(context, makeOccurrenceSet(context));
					//System.out.println("OccMap#addNodes	context:"+context);
				}
				boolean isAdded = map_.get(context).addNode(node);
				if(!isAdded) removeContext.add(context);
				changedContexts.add(context);
			}
			removeContext.forEach(c -> node.removeContext(c));
		}
		return changedContexts;
	}

	private OccurrenceSet makeOccurrenceSet(Context context){
		OccurrenceSet occSet = OccurrenceSetFactory.makeOccSet(context);
		return occSet;
	}

	/**
	 * 文脈を受け取ってその優先度を返す
	 * @param c
	 * @return
	 */
	int calcPriority(Context c){
		if(!map_.containsKey(c)){
			print();
			throw new IllegalArgumentException("no occSet for the context:"+c);
		}
		OccurrenceSet occSet = map_.get(c);
		return HOCompressor.CONSIDER_ONLY_BODY ?  occSet.reductionLabelSizeOfBody() : occSet.reductionLabelSize();
	}

	OccurrenceSet get(Context c){
		return map_.get(c);
	}

	boolean containsKey(Context c){
		return map_.containsKey(c);
	}

	public void print() {
		map_.keySet().forEach(c -> System.out.println("context:"+c+"	"+map_.get(c)));
	}
}
