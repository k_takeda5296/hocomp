package compressor.converter.tables;


import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import component.context.Context;
import compressor.HOCompressor;



/*
 * 優先度を変更できる優先度つきキュー
 */
public final class AmatizedPriorityQueue<E extends Context> implements PriorityUpdatableQueue<E> {

	private Set<E>[] setList_;//Set<E>を要素に持つ
	private Map<E, Integer> priorityMap_ = new HashMap<>();
	private int maxIndex_;

	private static final int BORDER = HOCompressor.PRIORITY_BORDER;

	@SuppressWarnings("unchecked")
	public AmatizedPriorityQueue(int bound){
		setList_ = HOCompressor.CHOOSE_0_PRIORITY_CONTEXT ? new Set[bound+1-BORDER] : new Set[bound];//0も抜き出す場合は0用のエントリも用意
		for(int i=0; i < setList_.length; i++){
			setList_[i] = new LinkedHashSet<E>();//ここはArrayListで良い。そうすると非決定性も無くなる。なんてことは無かった。removeが定数時間であるためにはOccurrenceSet自体にリンクが張られている必要がある。しかし、ここで全ての配列要素をLinkedHashSetで初期化しているのは少しメモリと時間の無駄である。が、入力ラベルサイズに対して高々O(√n)なので、あまり効果はない(???)。
		}
	}

	private int priority2Index(int priority){
		return Math.max(0, Math.min(priority, setList_.length-1));
	}

	/**
	 * 優先度を指定してeを追加する。すでに追加されているときは優先度を更新する。
	 * @param e
	 * @param priority
	 */
	@Override
	public void enqueue(E e, int priority){
		if(priorityMap_.containsKey(e)) removeElement(e);
		priorityMap_.put(e, priority);
		int index = priority2Index(priority);
		Set<E> newSet = setList_[index];
		if(newSet.contains(e)) throw new IllegalArgumentException("c should not be contained!");
		newSet.add(e);
		maxIndex_ = Math.max(index, maxIndex_);
	}

	@Override
	public void removeElement(E e){
		int index = priority2Index(priorityMap_.remove(e));
		Set<E> set = (Set<E>)setList_[index];
		if(!set.contains(e)) throw new IllegalArgumentException("no element to remove!");
		set.remove(e);
		lowerMaxIndex();
	}
	
	private void lowerMaxIndex(){
		Set<E> set = (Set<E>)setList_[maxIndex_];
		while(set.size() < 1 && maxIndex_ > 0){
			set = (Set<E>)setList_[--maxIndex_];
		}
	}

	@Override
	public boolean hasNext(){
		return maxIndex_ > 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public E peek(){
		Set<E> set = (Set<E>)setList_[maxIndex_];
		E e;
		e = maxIndex_ < setList_.length-1 ? 
				(E)set.toArray()[0] : //ここで非決定性が生まれている（うそ。LinkedHashSetにしたことで決定的になった）
					Collections.max(set, new Comparator<E>(){
						@Override
						public int compare(E c0, E c1) {
							int value0 = priorityMap_.get(c0);
							int value1 = priorityMap_.get(c1);
							return value0 - value1;
						}
					});
				return e;
	}

	@Override
	public String toString(){
		String str = "PriorityQueue:"+System.getProperty("line.separator");
		for(int i=0; i < setList_.length; i++){
			Set<E> set = (Set<E>)setList_[i];
			str += "	["+i+"]:"+set.toString()+System.getProperty("line.separator");
			str += "	maxindex:"+maxIndex_;
		}
		return str;
	}
}
