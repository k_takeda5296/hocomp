package compressor.converter.tables;


public interface PriorityUpdatableQueue<E> {

	void enqueue(E e, int priority);
	boolean hasNext();
	E peek();
	void removeElement(E c);
}
