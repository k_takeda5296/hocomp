package compressor.converter;



import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import main.Checker;

import component.occurrenceset.OccurrenceSet;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyRoot;
import compressor.HOCompressor;
import compressor.converter.tables.Tables;




public final class RecursiveExpanderIncl implements Compressor{

	private Tables tables_;
	private TyRoot currentTerm_;
	private int contextSize_;
	private final int maxSize_;
	private final int pBorder_;


	public RecursiveExpanderIncl(TyRoot term, int startSize, int maxSize, int pBorder){
		currentTerm_ = term;
		tables_ = new Tables(currentTerm_, startSize);
		contextSize_ = startSize;
		maxSize_ = maxSize;
		pBorder_ = pBorder;
	}


	@Override
	public TyRoot getTerm(){
		preprocess();
		compress();
		return currentTerm_;
	}

	private void preprocess(){
		subTreeExpansion();
	}

	private void compress(){
		while(true){
			//部分木のβ-expansion
			if(tables_.hasContextWithHoleToReplace()){
				OccurrenceSet occListWithHole = tables_.getContextWithHoleToReplace();

				//simplify用のFIFOキュー

				//β-expansion
				Collection<TyAbst> simplifyQueue = BetaExpansion.betaExpansion(currentTerm_, occListWithHole.getContext(), occListWithHole.getC2Nodes(), tables_, contextSize_);
				if(HOCompressor.BIDIRECTIONAL_CHECK) Checker.checkB(currentTerm_, "B-EX1", "", "");

				//TIMES++;
				//if(TIMES > MAX_TIMES) break;
				//simplify
				SimplifyIncl.simplify(currentTerm_, tables_, simplifyQueue, contextSize_);
				if(HOCompressor.BIDIRECTIONAL_CHECK) Checker.checkB(currentTerm_, "SIMPLIFY", "", "");


				//部分木のβ-expansion
				subTreeExpansion();

			}else{//maxSize以下である間サイズを+2して再実行
				contextSize_ += 2;
				if(contextSize_ > maxSize_){
					break;
				}else{
					tables_ = new Tables(currentTerm_, contextSize_);
				}
			}
		}
		//最後にsimplify．pruning phaseのように、ラベルサイズが減るような全てのβ簡約を行うようにする
		if(HOCompressor.CONSIDER_ONLY_BODY){
			System.out.println("RecursiveExpantionIncl	valid0:"+Checker.validTyTermCheck(currentTerm_));
			System.out.println("RecursiveExpantionIncl	existsReducibleLet?:"+Pruner.existReducibleLet(currentTerm_.getChild(), new HashSet<>(), new ArrayList<>()));
			List<TyAbst> newAbsts = Pruner.prune(currentTerm_, pBorder_);
			System.out.println("RecursiveExpantionIncl	valid1:"+Checker.validTyTermCheck(currentTerm_));
			SimplifyIncl.simplifyAll(currentTerm_, newAbsts, contextSize_);
			Set<TyAbst> abstSet = new HashSet<>(newAbsts);
			newAbsts.clear();
			boolean flag = Pruner.existReducibleLet(currentTerm_.getChild(), abstSet, newAbsts);
			System.out.println("RecursiveExpantionIncl	newAbsts.size:"+newAbsts.size()+"	existsReducibleLet?:"+flag);
			while(newAbsts.size() > 0){
				SimplifyIncl.simplifyAll(currentTerm_, newAbsts, contextSize_);
				System.out.println("RecursiveExpantionIncl	valid2:"+Checker.validTyTermCheck(currentTerm_));
				newAbsts.clear();
				flag = Pruner.existReducibleLet(currentTerm_.getChild(), abstSet, newAbsts);
				System.out.println("RecursiveExpantionIncl	newAbsts.size:"+newAbsts.size()+"	existsReducibleLet?:"+flag);
				assert(newAbsts.size()>0 == flag);
			}
		}
	}

	//穴なし文脈のβ展開
	private void subTreeExpansion() {
		while(true){
			//System.out.println("RecursiveExpansion#subTreeExpansion"+tables_.hasContextNonHoleToReplace());
			if(tables_.hasContextNonHoleToReplace()){
				OccurrenceSet occListNonHole = tables_.getContextNonHoleToReplace();
				Collection<TyAbst> simplifyQueue = BetaExpansion.betaExpansion(currentTerm_, occListNonHole.getContext(), occListNonHole.getC2Nodes(), tables_, contextSize_);
				if(HOCompressor.BIDIRECTIONAL_CHECK) Checker.checkB(currentTerm_, "B-EX2", "", "");
				SimplifyIncl.simplify(currentTerm_, tables_, simplifyQueue, contextSize_);
				if(HOCompressor.BIDIRECTIONAL_CHECK) Checker.checkB(currentTerm_, "SIMPLIFY2", "", "");
			}else{
				break;
			}
		}
	}

}
