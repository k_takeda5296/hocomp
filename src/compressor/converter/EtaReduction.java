package compressor.converter;


import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyTerm;
import compressor.converter.tables.Tables;


/*
 * β簡約の条件を「ラベルサイズが減る」ことにするべき
 */
public final class EtaReduction {

	static int size = 5;

	public static void etaReduction(TyAbst abst, Tables tables, Set<TyAbst> additionalAbsts, int contextSize) {
		//System.out.println("EtaReduction#etaReduction");
		//TyTermの付け替え
		TyTerm c0Foot = abst.getParent();
		TyApp app = (TyApp)abst.getChild();
		TyTerm lTerm = app.getLeftChild();
		c0Foot.replaceChild(abst, lTerm);lTerm.setParent(c0Foot);


		//simplifyの候補
		if(lTerm.size()==1){
			if(c0Foot instanceof TyApp){
				TyApp c0App = (TyApp)c0Foot;
				if(c0App.getLeftChild() instanceof TyAbst){
					TyAbst candAbst = (TyAbst)c0App.getLeftChild();
					additionalAbsts.add(candAbst);
				}
			}
		}
		if(lTerm instanceof TyAbst){
			additionalAbsts.add((TyAbst)lTerm);
		}
		if(c0Foot instanceof TyAbst){
			additionalAbsts.add((TyAbst)c0Foot);
		}

		//additionalAbstsから今回削除されるabstを除外
		additionalAbsts.remove(abst);

		//tablesの更新
		LinkedHashSet<TyApp> nodesToRemove = new LinkedHashSet<TyApp>();
		LinkedHashSet<TyApp> nodesToUpdate = new LinkedHashSet<TyApp>();
		LinkedHashSet<TyApp> newNodes = new LinkedHashSet<TyApp>();
		//nodesToRemove.add(abst);
		nodesToRemove.add(app);
		if(app.getRightChild() instanceof TyApp) nodesToRemove.add((TyApp)app.getRightChild());
		collectUpdateNodes(c0Foot, nodesToUpdate, contextSize);

		if(tables!=null){
			tables.update(nodesToRemove, nodesToUpdate, newNodes);//すべての文脈を返さなければならない？

			//abstTreeの更新
			if(abst.getName()==540) System.out.println("EtaRed#	f540!");
			try{
				TyAbst parentAbst = abst.getAbstParent();
				parentAbst.removeAbstChild(abst);
				List<TyAbst> children = abst.getAbstChildren();
				parentAbst.addAbstChildren(children);
				for(TyAbst child : children){
					child.setAbstParent(parentAbst);
				}
			}catch(Exception e){
				System.err.println("abst:"+abst.getName());
				System.err.println("abst.parent:"+abst.getParent());
				System.err.println("abst.Abstparent:"+abst.getAbstParent());
				throw e;
			}

			//Checker.checkInRootPosterity(term, nodesToRemove);
		}
	}

	private static void collectUpdateNodes(TyTerm c1Foot, LinkedHashSet<TyApp> updateNodes, int contextSize){
		int parentNum = contextSize/2;
		while(parentNum > 0){
			if(c1Foot instanceof TyApp){
				updateNodes.add((TyApp)c1Foot);
			}
			if(c1Foot.getParent()==null) break;
			c1Foot = c1Foot.getParent();
			parentNum--;
		}
	}

	public static boolean isEtaReducible(TyAbst abst) {
		if(abst.getVars().size() > 1) return false;
		TyTerm child = abst.getChild();
		if(!(child instanceof TyApp)) return false;
		TyApp app = (TyApp)child;
		return abst.getVars().contains(app.getRightChild());
	}
}
