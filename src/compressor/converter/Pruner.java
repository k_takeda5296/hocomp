package compressor.converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;

public class Pruner {

	public static List<TyAbst> prune(TyRoot root, int pBorder){
		List<TyAbst> newAbsts = new ArrayList<>();
		if(!hasNextAbst(root)) return newAbsts;
		TyAbst abstF = nextAbstOf(root);
		pruneLoop(abstF, newAbsts, pBorder);
		return newAbsts;
	}
	
	private static void pruneLoop(TyAbst abstF, List<TyAbst> newAbsts, int pBorder){
		while(true){
			pruneDecTerm(abstF, newAbsts, pBorder);
			if(isToBePruned(abstF, pBorder)){
				pruneAbst(abstF, newAbsts);
			}
			if(!hasNextAbst(abstF)) return;
			abstF = nextAbstOf(abstF);
		}
	}
	
	private static void pruneDecTerm(TyAbst abstF, List<TyAbst> newAbsts, int pBorder){
		if(!(abstF.getParent() instanceof TyApp)) throw new IllegalArgumentException();
		TyApp app = (TyApp)abstF.getParent();
		TyTerm decTerm = app.getRightChild();
		TyTerm decBody = decTerm;
		while(decBody instanceof TyAbst) decBody = ((TyAbst)decBody).getChild();
		if(!(decBody instanceof TyApp)) return;
		TyApp app2 = (TyApp)decBody;
		if(app2.getLeftChild() instanceof TyAbst) pruneLoop((TyAbst)app2.getLeftChild(), newAbsts, pBorder);
	}

	private static boolean hasNextAbst(TyAbst abstF){
		if(!(abstF.getChild() instanceof TyApp)) return false;
		TyApp app = (TyApp)abstF.getChild();
		if(!(app.getLeftChild() instanceof TyAbst)) return false;
		return true;
	}

	private static TyAbst nextAbstOf(TyAbst abstF){
		assert(abstF.getChild() instanceof TyApp);
		TyApp app = (TyApp)abstF.getChild();
		assert(app.getLeftChild() instanceof TyAbst);
		return (TyAbst)app.getLeftChild();
	}


	private static int kOf(TyTerm term, int acc){
		if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			return kOf(abst.getChild(), acc+1);
		}else{
			return acc;
		}
	}

	private static int hOf(TyTerm term, int acc){
		if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			return kOf(abst.getChild(), acc+abst.getVars().size());
		}else{
			return acc;
		}
	}

	static boolean FLAG = false;
	public static boolean isToBePruned(TyAbst abstF, int pBorder){
		assert(abstF.getParent() instanceof TyApp);
		TyApp app = (TyApp)abstF.getParent();
		TyTerm decTerm = app.getRightChild();


		int k = kOf(decTerm, 0);
		int h = hOf(decTerm, 0);
		int labelSize = decTerm.labelSize() - h;

		int incSizePerOcc = labelSize + h - k - 1;
		int decSize = labelSize + k + h + 1;
		int incSize = incSizePerOcc * abstF.getVars().size() - decSize;
		return incSize <= pBorder;
	}

	private static boolean containsAbst(int name, TyTerm term){
		if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			if(abst.getName()==name) return true;
			return containsAbst(name, abst.getChild());
		}else if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			return containsAbst(name, app.getLeftChild()) || containsAbst(name, app.getRightChild());
		}else{
			return false;
		}
	}

	private static void pruneAbst(TyAbst abstF, List<TyAbst> newAbsts){
		assert(abstF.getParent() instanceof TyApp);
		TyApp app = (TyApp)abstF.getParent();
		TyTerm decTerm = app.getRightChild();

		int k = kOf(decTerm, 0);
		for(TyVar varF : abstF.getVars()){
			pruneOne(abstF, varF, decTerm, k, newAbsts);
		}

		TyTerm c0Foot = app.getParent();
		TyTerm c1Root = abstF.getChild();
		/*
		System.out.println("Pruner#pruneAbst0	c0Foot:"+c0Foot);
		System.out.println("Pruner#pruneAbst0	c1Root:"+c1Root);
		 */
		c0Foot.replaceChild(app, c1Root);
		c1Root.setParent(c0Foot);
		/*
		System.out.println("Pruner#pruneAbst1	c0Foot:"+c0Foot);
		System.out.println("Pruner#pruneAbst1	c1Root:"+c1Root);
		 */
		FLAG = false;
	}

	private static void pruneOne(TyAbst abstF, TyVar varF, TyTerm decTerm, int k, List<TyAbst> newAbsts){
		List<TyTerm> argsAndC1FootAndVarHead = parseArgsAndC1FootAndVarHead(varF, k, decTerm);
		TyTerm varHead = argsAndC1FootAndVarHead.remove(argsAndC1FootAndVarHead.size()-1);
		TyTerm c1Foot = argsAndC1FootAndVarHead.remove(argsAndC1FootAndVarHead.size()-1);
		assert(varHead.getParent()==c1Foot);
		List<TyTerm> args = argsAndC1FootAndVarHead;
		boolean flag = args.stream().anyMatch(arg -> arg.size() < 10 && containsAbst(4958, arg));
		if(flag){
			args.forEach(arg -> System.out.println("Pruner#pruneOne	args.contains(4958):"+(arg.size() > 20 ? arg.size() : arg)));
			System.out.println("Pruner#pruneOne	k:"+k+"	args.size:"+args.size()+"	decTerm:"+decTerm);
			System.out.println("Pruner#pruneOne	varF["+varF.getType()+"]:"+varF);
			System.out.println("Pruner#pruneOne	varF.parent["+varF.getParent().getType()+"]:"+varF.getParent());
			System.out.println("Pruner#pruneOne	varF.parent.parent["+varF.getParent().getParent().getType()+"]:"+varF.getParent().getParent());
		}

		List<TyTerm> abstsAndDecBody = parseAbstsAndDecBody(decTerm, args.size());
		TyTerm decBody = abstsAndDecBody.remove(abstsAndDecBody.size()-1);
		assert(args.size()==abstsAndDecBody.size());

		Map<TyAbst, TyTerm> varMap = new HashMap<>();
		for(int i=0; i < args.size(); i++)
			varMap.put((TyAbst)abstsAndDecBody.get(i), args.get(i));
		TyTerm repTerm = makeTermToReplace(decBody, varMap, new HashMap<>(), newAbsts);

		c1Foot.replaceChild(varHead, repTerm);
		repTerm.setParent(c1Foot);
	}

	private static List<TyTerm> parseArgsAndC1FootAndVarHead(TyVar varF, int k, TyTerm decTerm){
		boolean flag = varF.getName()==8319 && false;
		List<TyTerm> argsAndC1FootAndVarHead = new ArrayList<>();
		TyTerm p = varF;
		TyTerm app = p.getParent();
		if(flag){
			System.out.println("Pruner#parseArgsAndC1FootAndVarHead0	k:"+k);
			System.out.println("Pruner#parseArgsAndC1FootAndVarHead0	p:"+p);
			System.out.println("Pruner#parseArgsAndC1FootAndVarHead0	app:"+app);
		}
		while(true){
			if(k <= 0) break;
			if(!(app instanceof TyApp)) break;
			if(((TyApp)app).getLeftChild()!=p) break;
			
			argsAndC1FootAndVarHead.add(((TyApp)app).getRightChild());
			p = app;
			app = app.getParent();
			k--;
			if(flag){
				System.out.println("Pruner#parseArgsAndC1FootAndVarHead1	k:"+k);
				System.out.println("Pruner#parseArgsAndC1FootAndVarHead1	p:"+p);
				System.out.println("Pruner#parseArgsAndC1FootAndVarHead1	app:"+app);
			}
		}
		argsAndC1FootAndVarHead.add(p.getParent());//C1Foot
		argsAndC1FootAndVarHead.add(p);//varHead
		if(flag) System.out.println("Pruner#parseArgsAndC1FootAndVarHead2	k:"+k+"	argsAnd.size:"+argsAndC1FootAndVarHead.size());
		return argsAndC1FootAndVarHead;
	}

	private static List<TyTerm> parseAbstsAndDecBody(TyTerm decTerm, int argNum){
		List<TyTerm> abstsAndDecBody = new ArrayList<>();
		TyTerm p = decTerm;
		while(p instanceof TyAbst && argNum > 0){
			abstsAndDecBody.add(p);
			p = ((TyAbst)p).getChild();
			argNum--;
		}
		abstsAndDecBody.add(p);
		return abstsAndDecBody;
	}

	private static TyTerm makeTermToReplace(TyTerm decBody, Map<TyAbst, TyTerm> varMap, Map<TyAbst, TyAbst> abstMap, List<TyAbst> newAbsts){
		if(decBody instanceof TySymbol){
			return decBody.copy();
		}else if(decBody instanceof TyVar){
			TyVar var = (TyVar)decBody;
			if(varMap.containsKey(var.getBinder())){//束縛子がdecTerm内で、対応する引数がある場合
				TyTerm term = varMap.get(var.getBinder());
				assert(term!=null);
				assert term.getType().equals(var.getType()) : "var:"+var+"	term:"+term;
				return var.getBinder().getVars().size() > 1 ? term.copy() : term;//2回以上出現する時はすべてコピーを用いる
			}else if(abstMap.containsKey(var.getBinder())){//束縛子がdecTerm内で、対応する引数がない場合
				TyAbst binder = abstMap.get(var.getBinder());
				return binder.makeVar();
			}else{//束縛子がdecTerm外の場合
				return var.getBinder().makeVar();
			}
		}else if(decBody instanceof TyApp){
			TyApp app = (TyApp)decBody;
			TyTerm left = makeTermToReplace(app.getLeftChild(), varMap, abstMap, newAbsts);
			TyTerm right = makeTermToReplace(app.getRightChild(), varMap, abstMap, newAbsts);
			TyTerm retTerm = new TyApp(left, right);
			left.setParent(retTerm);
			right.setParent(retTerm);
			/*
			TyTerm maybeAbst = left;
			while(maybeAbst instanceof TyAbst){
				newAbsts.add((TyAbst)maybeAbst);
				maybeAbst = ((TyAbst)maybeAbst).getChild();
			}
			maybeAbst = right;
			while(maybeAbst instanceof TyAbst){
				newAbsts.add((TyAbst)maybeAbst);
				maybeAbst = ((TyAbst)maybeAbst).getChild();
			}
			*/
			return retTerm;
		}else if(decBody instanceof TyAbst){
			TyAbst abst = (TyAbst)decBody;
			TyAbst retTerm = new TyAbst(false, abst.getType());
			newAbsts.add(retTerm);
			abstMap.put(abst, retTerm);
			TyTerm child = makeTermToReplace(abst.getChild(), varMap, abstMap, newAbsts);
			retTerm.setChild(child);
			child.setParent(retTerm);
			return retTerm;
		}
		throw new IllegalArgumentException("something wrong!");
	}

	public static boolean existReducibleLet(TyTerm term, Set<TyAbst> abstSet, List<TyAbst> newAbsts){
		if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			return existReducibleLet(abst.getChild(), abstSet, newAbsts);
		}else if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			boolean left = existReducibleLet(app.getLeftChild(), abstSet, newAbsts);
			boolean right = existReducibleLet(app.getRightChild(), abstSet, newAbsts);
			if(app.getRightChild() instanceof TyApp){
				TyApp rApp = (TyApp)app.getRightChild();
				if(rApp.getLeftChild() instanceof TyAbst){
					newAbsts.add((TyAbst)rApp.getLeftChild());
					if(!abstSet.contains(rApp.getLeftChild())){
						System.out.println("Pruner#existReducibleLet");
						/*
					System.out.println("Pruner#existReducibleLet	rApp:"+rApp);
					System.out.println("Pruner#existReducibleLet	rApp.left:"+rApp.getLeftChild());
						 */
						System.out.println("Pruner#existReducibleLet	rApp.left.name:"+((TyAbst)rApp.getLeftChild()).idStr());
						System.out.println("Pruner#existReducibleLet	rApp.left.contained:"+abstSet.contains(rApp.getLeftChild()));
					}
					return true;
				}
			}
			return left || right;
		}
		return false;
	}

}
