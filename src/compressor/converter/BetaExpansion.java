package compressor.converter;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import main.Checker;
import util.MyUtil;

import component.context.Context;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Fun;
import component.tyterm.type.Type;
import compressor.HOCompressor;
import compressor.converter.tables.Tables;

public final class BetaExpansion {

	public static int BETA_EX_TIMES = 0;

	/*
	 * replaceした後にextractionを行う
	 */
	public static Set<TyAbst> betaExpansion(TyRoot term, Context context, Set<TyApp> occurrences, Tables tables, int contextSize){
		if(HOCompressor.CHECK_MAX_MEMORY) if(BETA_EX_TIMES % HOCompressor.CHECKING_TERM == 0) Checker.updateMaxMemory();
		BETA_EX_TIMES++;
		//System.out.println("BetaExpansion#betaExpansion["+BETA_EX_TIMES+"]	context["+occurrences.size()+"]:"+context);
		//System.out.println("BetaExpansion#betaExpansion0	context["+occurrences.size()+"]:"+context+"	occurrences:"+occurrences);
		//System.out.println("BetaExpansion#betaExpansion0	context["+occurrences.size()+"]:"+context+"	term:"+term.getChild());
		//System.out.print("BetaExpansion#betaExpansion["+BETA_EX_TIMES+"]	context["+occurrences.size()+"]:"+context+"	occurrences");
		//occurrences.stream().forEach(t -> System.out.print("["+t.getId()+"],"));System.out.println();
		//System.out.print("BetaExpansion#betaExpansion["+BETA_EX_TIMES+"]	context["+occurrences.size()+"]:"+context+"	occurrence.id");
		//occurrences.stream().filter(t -> !term.getPosterity().contains(t)).forEach(t -> System.out.print("["+t.getId()+"]["+t.getParent()+"],"));System.out.println();
		//int id = Main.ID;
		//System.out.println("BetaExpansion#betaExpansion["+BETA_EX_TIMES+"]	context["+occurrences.size()+"]:"+context+"	root.contain?"+id+":"+term.getPosterity().parallelStream().anyMatch(t -> t instanceof TyApp && ((TyApp)t).getId()==id));
		LinkedHashSet<TyAbst> simplifySet = new LinkedHashSet<TyAbst>();
		LinkedHashSet<TyApp> nodesToRemove = new LinkedHashSet<TyApp>();
		LinkedHashSet<TyApp> nodesToUpdate = new LinkedHashSet<TyApp>();
		LinkedHashSet<TyApp> newNodes = new LinkedHashSet<TyApp>();


		TyAbst abstF = makeAbstF(context, term);

		//replaceフェイズ
		Set<TyVar> removedVars = new LinkedHashSet<TyVar>();
		TyApp firstOccurrence = MyUtil.topOfSet(occurrences);//obtainFirstOccurrence(occurrences, context, term);//再利用せずにcontext.correspondingTermで＠も作ってしまうバージョンも書いてみたい
		replace(occurrences, abstF, context, nodesToRemove, nodesToUpdate, newNodes, simplifySet, removedVars, firstOccurrence, term, contextSize);


		//extractionフェイズ
		extract(term, context, abstF, firstOccurrence, nodesToUpdate, newNodes, simplifySet);

		//update
		tables.update(nodesToRemove, nodesToUpdate, newNodes);
		//削除されるvarsをabstから削除
		for(TyVar var : removedVars){
			var.getBinder().getVars().remove(var);
		}
		return simplifySet;
	}

	/*
	 * abstFの子は未設定
	 * rootを渡していて汚い
	 */
	private static TyAbst makeAbstF(Context context, TyRoot root){
		TyAbst c0Foot = obtainC0Foot(context, root);
		//assert(c0Foot instanceof TyRoot || c0Foot.isReducible());//起こりうる
		TyTerm c1Root = c0Foot.getChild();
		Type type = context.getCorrespondingTermType();
		TyAbst abstF = new TyAbst(true, new Fun(type, c1Root.getType()));
		return abstF;
	}

	private static void extract(TyRoot term, Context context, TyAbst abstF, TyApp occurrence,
			LinkedHashSet<TyApp> nodesToUpdate, LinkedHashSet<TyApp> newNodes, Set<TyAbst> simplifySet){
		TyTerm contextTerm = context.makeCorrespondingTerm(occurrence, simplifySet);
		TyAbst c0Foot = obtainC0Foot(context, term);
		TyTerm c1Root = c0Foot.getChild();
		TyApp app = new TyApp(abstF, contextTerm, c1Root.getType());

		//λ抽象木の更新
		updateAbstTree(c0Foot, abstF, contextTerm);

		//更新用ノード集合のマネジメント
		newNodes.add(app);
		if(c0Foot!=term && c0Foot.getParent() instanceof TyApp){
			nodesToUpdate.add((TyApp)c0Foot.getParent());//文脈の最大サイズが5なので親の親まで見る。が、結局あいだにabstを挟むので意味なし
		}

		//ノードの付け替え
		c0Foot.setChild(app);app.setParent(c0Foot);
		abstF.setParent(app);
		contextTerm.setParent(app);
		abstF.setChild(c1Root);c1Root.setParent(abstF);
	}


	private static TyAbst obtainC0Foot(Context context, TyRoot root){
		Optional<TyAbst> c0Foot = context.getC0Foot();
		return c0Foot.isPresent() ? c0Foot.get() : root;
	}

	private static void replace(Set<TyApp> occurrences, TyAbst abstF, Context context, LinkedHashSet<TyApp> nodesToRemove, LinkedHashSet<TyApp> nodesToUpdate,
			LinkedHashSet<TyApp> newNodes, Set<TyAbst> simplifySet, Set<TyVar> removedVars, TyApp firstOccurrence, TyRoot root, int contextSize){
		for(TyApp occurrence : occurrences){
			replaceOne(occurrence, abstF, context, nodesToRemove, nodesToUpdate, newNodes, simplifySet, removedVars, firstOccurrence==occurrence, root, contextSize);
		}
	}

	private static void replaceOne(TyApp occurrence, TyAbst abstF, Context context, LinkedHashSet<TyApp> nodesToRemove, LinkedHashSet<TyApp> nodesToUpdate,
			LinkedHashSet<TyApp> newNodes, Set<TyAbst> simplifySet, Set<TyVar> removedVars, boolean isFirstOcc, TyRoot root, int contextSize){
		try{
			//η簡約候補
			if(occurrence.getParent() instanceof TyAbst){
				simplifySet.add((TyAbst)occurrence.getParent());
			}
			TyTerm[] holeTerms = context.getHoleTermsAndCollectRemovedHoleVars(occurrence, removedVars);
			if(!isFirstOcc) context.collectRemovedVars(occurrence, removedVars);
			
			//System.out.println("BetaExpansion#replaceOne	holeTerms["+holeTerms.size()+"]:"+holeTerms);

			TyVar varF = abstF.makeVar();
			TyTerm replaceTerm = appliedVarF(varF, holeTerms, newNodes);
			TyTerm c1Foot = occurrence.getParent();

			//更新用ノード集合の更新
			collectUpdateNodes(occurrence, isFirstOcc, context, c1Foot, nodesToUpdate, contextSize);
			//削除用ノード集合の更新
			collectRemoveNodes(occurrence, isFirstOcc, context, nodesToRemove);

			//ノードの付け替え
			c1Foot.replaceChild(occurrence, replaceTerm);
			replaceTerm.setParent(c1Foot);

			//Checker.checkB(c1Foot.getParent(), "REPLACE_IN", "", "");

			//同値穴によるβ_once簡約候補
			if(holeTerms.length > 0){
				TyTerm arg = holeTerms[0];
				if(arg instanceof TyVar) simplifySet.add(((TyVar) arg).getBinder());
			}
			//contextが穴なし文脈のとき、fの親が@でその左の子がabstならβ_id簡約
			if(context.getK()==0){
				TyTerm parent = varF.getParent();
				if(parent instanceof TyApp){
					TyTerm left = ((TyApp)parent).getLeftChild();
					if(left instanceof TyAbst) simplifySet.add((TyAbst)left);
				}
			}
		}catch(Exception e){
			System.err.println("BetaEx#replaceOne	context:"+context+"	occurrence:"+occurrence);
			throw e;
		}
	}
	
	/**
	 * 必要な数の祖先までを更新ノードの集合に追加する
	 * @param c1Foot
	 * @param updateNodes
	 * @param contextSize
	 */
	private static void collectUpdateNodes(TyApp occurrence, boolean isFirstOcc, Context context, TyTerm c1Foot, LinkedHashSet<TyApp> updateNodes, int contextSize){
		if(isFirstOcc) context.collectCorrespondingApps(occurrence, updateNodes);
		int parentNum = contextSize/2;
		while(parentNum > 0){
			if(c1Foot instanceof TyApp){
				updateNodes.add((TyApp)c1Foot);
			}
			if(c1Foot.getParent()==null) break;
			c1Foot = c1Foot.getParent();
			parentNum--;
		}
	}
	
	private static void collectRemoveNodes(TyApp occurrence, boolean isFirstOcc, Context context, LinkedHashSet<TyApp> removeNodes){
		if(!isFirstOcc){
			removeNodes.add(occurrence);
			context.collectCorrespondingApps(occurrence, removeNodes);
		}
	}

	/**
	 * 出現を置き換えるλ項を返す
	 * @param varF
	 * @param holeTerms
	 * @param newNodes
	 * @return
	 */
	private static TyTerm appliedVarF(TyVar varF, TyTerm[] holeTerms, Set<TyApp> newNodes){
		TyTerm term = varF;
		for(TyTerm arg : holeTerms){
			Type type = ((Fun)term.getType()).getRight();
			TyApp app = new TyApp(term, arg, type);
			newNodes.add(app);
			term.setParent(app);
			arg.setParent(app);
			term = app;
		}
		return term;
	}

	private static void updateAbstTree(TyAbst c0Foot, TyAbst abstF, TyTerm contextTerm) {
		List<TyAbst> c0Children = c0Foot.getAbstChildren();
		abstF.addAbstChildren(c0Children);
		for(TyAbst abst : c0Children){
			abst.setAbstParent(abstF);
		}
		c0Foot.removeAbstChildren(c0Children);
		c0Foot.addAbstChild(abstF);
		abstF.setAbstParent(c0Foot);
		if(contextTerm instanceof TyAbst){
			TyAbst abstX = (TyAbst)contextTerm;
			c0Foot.addAbstChild(abstX);
			abstX.setAbstParent(c0Foot);
		}
	}

	public static void init() {
		BETA_EX_TIMES = 0;
	}

}
