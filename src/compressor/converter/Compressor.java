package compressor.converter;

import component.tyterm.term.TyRoot;

public interface Compressor {

	TyRoot getTerm();
}
