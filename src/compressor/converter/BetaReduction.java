package compressor.converter;


import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import util.MyUtil;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import compressor.converter.tables.Tables;

/*
 * β簡約の条件を「ラベルサイズが減る」ことにするべき
 */
public final class BetaReduction {

	static int size = 5;

	//λ抽象の束縛する変数が一つしかないときのβ-reduction（ラムダ式のコピーを行わない）
	public static void betaReductionWithOnce(TyAbst abst, Tables tables, Set<TyAbst> additionalAbsts, int contextSize) {
		//Main.printLog("betaReductinWithOnce	");
		//System.out.println("BetaReduction#betaReductionWithOnce");

		//TyTermの付け替え
		TyApp app = (TyApp)abst.getParent();
		TyTerm c0Foot = app.getParent();
		TyVar varF = MyUtil.topOfSet(abst.getVars());
		TyTerm c1Foot = varF.getParent();
		TyTerm c1Root = abst.getChild();
		TyTerm rTerm = app.getRightChild();
		c0Foot.replaceChild(app, c1Root);c1Root.setParent(c0Foot);
		c1Foot.replaceChild(varF, rTerm);rTerm.setParent(c1Foot);


		//Main.printLog("BetaReduction#betaReductionWithOnce	abst:	"+abst);
		//Main.printLog("BetaReduction#betaReductionWithOnce	app:	"+app);
		//Main.printLog("BetaReduction#betaReductionWithOnce	var:	"+varF);

		//simplifyする候補
		if(c1Foot instanceof TyApp){
			TyApp c1App = (TyApp)c1Foot;
			if(c1App.getLeftChild() instanceof TyAbst){
				TyAbst candAbst = (TyAbst)c1App.getLeftChild();
				additionalAbsts.add(candAbst);
			}
		}
		if(c1Root instanceof TyAbst){
			additionalAbsts.add((TyAbst)c1Root);
		}

		//additionalAbstsから今回削除されるabstを除外
		additionalAbsts.remove(abst);


		//tablesの更新
		LinkedHashSet<TyApp> nodesToRemove = new LinkedHashSet<TyApp>();
		LinkedHashSet<TyApp> nodesToUpdate = new LinkedHashSet<TyApp>();
		LinkedHashSet<TyApp> newNodes = new LinkedHashSet<TyApp>();
		//nodesToRemove.add(abst);
		nodesToRemove.add(app);
		//nodesToRemove.add(varF);
		collectUpdateNodes(c0Foot, nodesToUpdate, contextSize);
		collectUpdateNodes(c1Foot, nodesToUpdate, contextSize);

		if(tables!=null){
			tables.update(nodesToRemove, nodesToUpdate, newNodes);

			//abstTreeの更新
			TyAbst parentAbst = abst.getAbstParent();
			parentAbst.removeAbstChild(abst);
			List<TyAbst> children = abst.getAbstChildren();
			parentAbst.addAbstChildren(children);
			for(TyAbst child : children){
				child.setAbstParent(parentAbst);
			}

			/*
		abst.clear();
		app.clear();
		varF.clear();
			 */
			//Checker.checkInRootPosterity(term, nodesToRemove);
		}
	}

	//λ抽象への代入項が変数のときのβ-reduction
	public static void betaReductionWithId(TyAbst abst, Tables tables, Set<TyAbst> additionalAbsts, int contextSize) {
		//System.out.println("BetaReduction#betaReductionWithOnce");
		//TyTermの付け替え
		TyApp app = (TyApp)abst.getParent();
		TyTerm c0Foot = app.getParent();
		Set<TyVar> vars = abst.getVars();
		TyTerm c1Root = abst.getChild();
		try{
			c0Foot.replaceChild(app, c1Root);c1Root.setParent(c0Foot);
		}catch(Exception e){
			System.err.println("BetaId#	abst.name:"+abst.getName());
			throw new IllegalArgumentException("BetaId");
		}

		//simplifyする候補
		if(c1Root instanceof TyAbst){
			additionalAbsts.add((TyAbst)c1Root);
		}

		//additionalAbstsから今回削除されるabstを除外
		additionalAbsts.remove(abst);


		//tablesの更新
		LinkedHashSet<TyApp> nodesToRemove = new LinkedHashSet<TyApp>();
		LinkedHashSet<TyApp> nodesToUpdate = new LinkedHashSet<TyApp>();
		LinkedHashSet<TyApp> newNodes = new LinkedHashSet<TyApp>();
		//nodesToRemove.add(abst);
		nodesToRemove.add(app);
		//nodesToRemove.addAll(vars);

		/*
		//tablesの更新/*
		Set<TyTerm> nodesToRemove = new LinkedHashSet<TyTerm>();
		Set<TyTerm> nodesToUpdate = new LinkedHashSet<TyTerm>();
		Set<TyTerm> newNodes = new LinkedHashSet<TyTerm>();
		nodesToRemove.add(abst);nodesToRemove.add(app);
		nodesToRemove.addAll(vars);
		 */

		TyTerm rTermId = app.getRightChild();
		for(TyVar varF : vars){
			TyTerm c1Foot = varF.getParent();
			collectUpdateNodes(c1Foot, nodesToUpdate, contextSize);
			TyTerm newId = rTermId.copy();
			//newNodes.add(newId);
			c1Foot.replaceChild(varF, newId);newId.setParent(c1Foot);

			//simplifyする候補
			if(c1Foot instanceof TyApp){
				TyApp c1App = (TyApp)c1Foot;
				if(c1App.getLeftChild() instanceof TyAbst){
					TyAbst candAbst = (TyAbst)c1App.getLeftChild();
					additionalAbsts.add(candAbst);
				}
			}
		}

		//代入するIdが変数の場合、その束縛子からrTermIdを削除
		if(rTermId instanceof TyVar){
			TyVar rVar = (TyVar)rTermId;
			rVar.getBinder().getVars().remove(rVar);
		}
		//nodesToRemove.add(rTermId);

		if(tables!=null){
			tables.update(nodesToRemove, nodesToUpdate, newNodes);

			//abstTreeの更新
			TyAbst parentAbst = abst.getAbstParent();
			parentAbst.removeAbstChild(abst);
			List<TyAbst> children = abst.getAbstChildren();
			parentAbst.addAbstChildren(children);
			for(TyAbst child : children){
				child.setAbstParent(parentAbst);
			}

			//代入したidの上が＠でその左の子がAbstのとき

			//Checker.checkInRootPosterity(term, nodesToRemove);
		}
	}

	private static void collectUpdateNodes(TyTerm c1Foot, LinkedHashSet<TyApp> updateNodes, int contextSize){
		int parentNum = contextSize/2;
		while(parentNum > 0){
			if(c1Foot instanceof TyApp){
				updateNodes.add((TyApp)c1Foot);
			}
			if(c1Foot.getParent()==null) break;
			c1Foot = c1Foot.getParent();
			parentNum--;
		}
	}

	//λ抽象の束縛する変数が一つしかないときのβ-reductionのチェック
	public static boolean isBetaReducibleWithOnce(TyAbst abst) {
		if(abst.getVars().size() > 1) return false;
		TyTerm parent = abst.getParent();
		if(!(parent instanceof TyApp)) return false;
		TyApp app = (TyApp)parent;
		return app.getLeftChild() == abst;
	}

	//λ抽象への代入項が変数のときのβ-reductionのチェック
	public static boolean isBetaReducibleWithId(TyAbst abst) {
		TyTerm parent = abst.getParent();
		if(!(parent instanceof TyApp)) return false;
		TyApp app = (TyApp)parent;
		TyTerm right = app.getRightChild();
		return app.getLeftChild() == abst && (right instanceof TySymbol || right instanceof TyVar);
	}

}
