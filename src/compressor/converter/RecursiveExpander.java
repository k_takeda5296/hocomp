package compressor.converter;



import java.util.Collection;

import main.Checker;

import component.occurrenceset.OccurrenceSet;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyRoot;
import compressor.HOCompressor;
import compressor.converter.tables.Tables;




public final class RecursiveExpander implements Compressor{

	private final Tables tables_;
	private TyRoot currentTerm_;
	private int contextSize_;


	public RecursiveExpander(TyRoot term, int contextSize){
		currentTerm_ = term;
		tables_ = new Tables(currentTerm_, contextSize);
		contextSize_ = contextSize;
	}


	@Override
	public TyRoot getTerm(){
		preprocess();
		compress();
		return currentTerm_;
	}

	private void preprocess(){
		subTreeExpansion();
	}

	private void compress(){
		while(true){
			//部分木のβ-expansion
			if(tables_.hasContextWithHoleToReplace()){
				OccurrenceSet occListWithHole = tables_.getContextWithHoleToReplace();

				//simplify用のFIFOキュー

				//β-expansion
				Collection<TyAbst> simplifyQueue = BetaExpansion.betaExpansion(currentTerm_, occListWithHole.getContext(), occListWithHole.getC2Nodes(), tables_, contextSize_);
				if(HOCompressor.BIDIRECTIONAL_CHECK) Checker.checkB(currentTerm_, "B-EX1", "", "");

				//TIMES++;
				//if(TIMES > MAX_TIMES) break;
				//simplify
				SimplifyIncl.simplify(currentTerm_, tables_, simplifyQueue, contextSize_);
				if(HOCompressor.BIDIRECTIONAL_CHECK) Checker.checkB(currentTerm_, "SIMPLIFY", "", "");


				//部分木のβ-expansion
				subTreeExpansion();

			}else{//有効な（ラベルサイズが減るような）コンテキストが無ければsimplify
				break;
			}
		}
		//最後にsimplify．pruning phaseのように、ラベルサイズが減るような全てのβ簡約を行うようにする？
	}

	//穴なし文脈のβ展開
	private void subTreeExpansion() {
		while(true){
			//System.out.println("RecursiveExpansion#subTreeExpansion"+tables_.hasContextNonHoleToReplace());
			if(tables_.hasContextNonHoleToReplace()){
				OccurrenceSet occListNonHole = tables_.getContextNonHoleToReplace();
				Collection<TyAbst> simplifyQueue = BetaExpansion.betaExpansion(currentTerm_, occListNonHole.getContext(), occListNonHole.getC2Nodes(), tables_, contextSize_);
				if(HOCompressor.BIDIRECTIONAL_CHECK) Checker.checkB(currentTerm_, "B-EX2", "", "");
				SimplifyIncl.simplify(currentTerm_, tables_, simplifyQueue, contextSize_);
				if(HOCompressor.BIDIRECTIONAL_CHECK) Checker.checkB(currentTerm_, "SIMPLIFY2", "", "");
			}else{
				break;
			}
		}
	}

}
