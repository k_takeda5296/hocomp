package util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public final class MyUtil {
	
	public static String getDate(){
		return (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime());
	}
	
	public static void printInputStream(InputStream is) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		try {
			for (;;) {
				String line = br.readLine();
				if (line == null) break;
				System.out.println(line);
			}
		} finally {
			br.close();
		}
	}

	public static <T> T[] concat(T[] first, T[] second) {
		T[] result = Arrays.copyOf(first, first.length + second.length);
		System.arraycopy(second, 0, result, first.length, second.length);
		return result;
	}

	public static void sleepMillis(long millis){
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
	}

	public static List<Boolean> byteList2BooleanList(List<Byte> byteList){
		List<Boolean> boolList = new ArrayList<Boolean>();
		for(int i=0; i < byteList.size(); i++){
			List<Boolean> bits = byte2BooleanList(byteList.get(i));
			boolList.addAll(bits);
		}
		return boolList;
	}

	public static List<Boolean> byte2BooleanList(byte b){
		List<Boolean> boolList = new ArrayList<Boolean>();
		for(int j=0; j < 8; j++){
			byte mask = (byte)Math.pow(2, 7-j);//右からj番目のみ1
			boolList.add((b & mask) != 0 ? true : false);
		}
		return boolList;
	}

	public static <T> T topOfSet(Set<T> set){
		T e = null;
		for(T t : set){
			e = t;
			break;
		}
		return e;
	}

	public static int makeHashCode(Object[] objects){
		int result = 17;
		for(Object obj : objects)
			result += 31 * obj.hashCode();
		return result;
	}

	public static void outputByteArray(byte[] bytes, String fname) {
		OutputStream output;
		try {
			output = new BufferedOutputStream(new FileOutputStream(fname));
			output.write(bytes);
			output.flush();
			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void outputByteList(List<Byte> byteList, String fname){
		byte[] bytes = new byte[byteList.size()];
		for(int i=0; i < byteList.size(); i++){
			bytes[i] = byteList.get(i);
		}
		outputByteArray(bytes, fname);
	}

	public static List<Byte> inputByteList(String fname) {
		FileInputStream input;
		byte[] bytes = new byte[256];
		List<Byte> byteList = new ArrayList<Byte>();
		try {
			input = new FileInputStream(fname);
			int readNum = 0;
			while((readNum = input.read(bytes)) > 0){
				for(int i=0; i < readNum; i++){
					byteList.add(bytes[i]);
				}
			}
			input.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return byteList;
	}

	public static void makeDirIfNotExists(String dir){
		if(Files.notExists(Paths.get(dir))) System.out.println("makeDirIfNotExists:"+new File(dir).mkdirs());
	}
	
	public static <T> ArrayList<T> reversedArrayList(List<T> list){
		ArrayList<T> rList = new ArrayList<T>();
		for(int i=list.size()-1; i > -1; i--) rList.add(list.get(i));
		return rList;
	}
	
	
	public static void repeat(int n, Runnable runnable){
		IntStream.rangeClosed(1, n).forEach(i -> runnable.run());
	}
	
	public static byte[] boolList2ByteArray(List<Boolean> boolList){
		if(boolList.size() < 1) throw new IllegalArgumentException("size should be positive!");
		byte[] bytes = new byte[(boolList.size()-1)/8 + 1];
		int index = 0;
		for(int i=0; i < bytes.length; i++){
			byte b = 0;
			for(int j=0; j < 8; j++){
				if(index < boolList.size()) b = (byte) (boolList.get(index++) ? (b << 1) + 1 : (b << 1));
				else b = (byte)(b << 1);
			};
			bytes[i] = b;
		}
		return bytes;
	}
	
	public static void main(String[] arsgs){
		System.out.println("MyUtil##main	-1/8::"+((-1)/8));
		boolean[] bools = new boolean[]{ true, false, true, false, true, false, true, false, true };
		List<Boolean> boolList = new ArrayList<>();
		for(boolean bool : bools) boolList.add(bool);
		System.out.println("MyUtil##main	boolList:"+boolList);
		byte[] bytes = boolList2ByteArray(boolList);
		System.out.println("MyUtil##main	bytes:");
		for(byte b : bytes) System.out.println(String.format("0x%x", b));
		addInt2ByteList(65535, null);
	}

	public static void addInt2ByteList(int n, List<Byte> byteList) {
		byte[] bytes = ByteBuffer.allocate(4).putInt(n).array();
		for(byte b : bytes) byteList.add(b);
	}
	
	public static byte[] int2ByteArray(int n){
		return ByteBuffer.allocate(4).putInt(n).array();
	}

	public static void compressFileByZip(String inFile, String outFile){
		File[] files = { new File(inFile) };
		ZipOutputStream zos = null;
		try {
			zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(new File(outFile))));
			createZip(zos, files);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				zos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static void createZip(ZipOutputStream zos, File[] files) throws IOException {
		byte[] buf = new byte[1024];
		InputStream is = null;
		try {
			for (File file : files) {
				ZipEntry entry = new ZipEntry(file.getName());
				zos.putNextEntry(entry);
				is = new BufferedInputStream(new FileInputStream(file));
				int len = 0;
				while ((len = is.read(buf)) != -1) {
					zos.write(buf, 0, len);
				}
			}
		} finally {
			is.close();
		}
	}


}
