package util;
import java.lang.reflect.Field;

import component.tyterm.type.Fun;

/*
 * http://nnabeyang.hatenablog.com/entry/2013/02/08/142948
 */
public class MemoryCheck {

	public static void main(String[] args) throws Exception {
		int usage = calcUsage(Fun.class);
		System.out.println("usage = " + usage + " bytes");
	}
	private static int OVERHEAD = 16;
	public static int calcUsage(Class testClass) throws Exception {
		int usage = OVERHEAD;
		Object obj = testClass.newInstance();
		for (Field field : testClass.getDeclaredFields()) {
			String fieldName = field.getName();
			String fieldType = field.getType().getSimpleName();
			if (fieldType.equals("boolean"))
				usage += 1;
			else if  (fieldType.equals("char"))
				usage += 2;
			else if  (fieldType.equals("int"))
				usage += 4;
			else if  (fieldType.equals("long"))
				usage += 8;
			else if  (fieldType.equals("double")) 
				usage += 8;
			else if  (fieldType.equals("boolean[]")) {
				usage += 8; // ref
				int n = ((boolean[]) field.get(obj)).length;
				usage += 1 * n + 24;
			}
			else if  (fieldType.equals("char[]")) {
				usage += 8; // ref
				int n = ((char[]) field.get(obj)).length;
				usage += 2 * n + 24;
			}
			else if  (fieldType.equals("int[]")) {
				usage += 8; // ref
				int n = ((int[]) field.get(obj)).length;
				usage += 4 * n + 24;
			}
			else if  (fieldType.equals("long[]")) {
				usage += 8; // ref
				int n = ((long[]) field.get(obj)).length;
				usage += 8 * n + 24;
			}
			else if  (fieldType.equals("double[]")) {
				usage += 8; // ref
				int n = ((double[]) field.get(obj)).length;
				usage += 8 * n + 24;
			}
			else {
				throw new Exception(fieldType);
			}
		}
		while (usage % 8 != 0) usage++; // padding
		return usage;
	}

}