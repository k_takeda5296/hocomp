package util;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;

public class TimeMeasurer {

	//状態によって計測開始時刻か計測結果のどちらかを表す
	private long real_;
	private long cpu_;
	private long user_;
	
	private ThreadMXBean bean_ = ManagementFactory.getThreadMXBean();
	
	private boolean measuring_ = false;
	
	private boolean printable_ = false;

	public TimeMeasurer(){}
	
	/**
	 * 計測を開始する。すでに計測中の場合は例外IllegalArgumentExceptionを投げる
	 */
	public void start(){
		if(measuring_) throw new IllegalArgumentException("Already during measurement!");
		measuring_ = true;
		printable_ = false;
		real_ = System.currentTimeMillis();
		cpu_ = bean_.getCurrentThreadCpuTime();
		user_ = bean_.getCurrentThreadUserTime();
	}
	
	
	/**
	 * 計測を終了する。計測中でない場合は例外IllegalArgumentExceptionを投げる
	 */
	public void end(){
		long realEnd = System.currentTimeMillis();
		long cpuEnd = bean_.getCurrentThreadCpuTime();
		long userEnd = bean_.getCurrentThreadUserTime();
		real_ = realEnd - real_;
		cpu_ = cpuEnd - cpu_;
		user_ = userEnd - user_;
		if(!measuring_) throw new IllegalArgumentException("Measurement have NOT started!");
		measuring_ = false;
		printable_ = true;
	}
	
	/**
	 * 計測結果を表示する。未計測または計測中の場合は例外IllegalArgumentExceptionを投げる
	 * @param situation
	 */
	public void print(String situation){
		if(measuring_ ) throw new IllegalArgumentException("Still being measured!");
		if(!printable_) throw new IllegalArgumentException("No results!");
		System.out.println("["+situation+"]_ELAPSED_TIME(REAL):	"+real_/(double)1000);
		System.out.println("["+situation+"]_ELAPSED_TIME(CPU):	"+cpu_/((double)1000*1000*1000));
		System.out.println("["+situation+"]_ELAPSED_TIME(USER):	"+user_/((double)1000*1000*1000));
	}
}
