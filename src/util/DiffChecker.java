package util;

import java.io.File;

import component.tyterm.term.TyRoot;
import compressor.HOCompressor;
import compressor.loader.DnaTextLoader;
import compressor.loader.EnTextsCombinator;
import compressor.loader.XmlLoader;

public class DiffChecker {
	
	public static final String OPT_FILE = "-f";
	public static final String OPT_DNA_TEXT = "-dna_text";
	public static final String OPT_EN_TEXT = "-en_text";
	public static final String OPT_PROG_TEXT = "-prog_text";
	public static final String OPT_CSIZE = "-c";
	public static final String OPT_LOG = "-log";

	private static final String USAGE = "\n" +
			OPT_FILE+"	圧縮するファイル（英文テキストの時はディレクトリ）のパス\n" +
			OPT_DNA_TEXT+"	対象がDNAテキストの時に長さを指定（値は長さ）\n" + 
			OPT_EN_TEXT+"	対象が英文テキストの時に指定（値は最大ファイル数）\n" + 
			OPT_PROG_TEXT+"	対象がプログラムのソースコード（Python除く）の時に指定（値なし）\n" + 
			OPT_CSIZE+"	文脈の最大ノード数\n" +
			OPT_LOG+"	ログファイル名を指定する\n";
	
	public static void main(String[] args){
		CommandLineParser clp = new CommandLineParser()
		.addOption(OPT_FILE, true)
		.addOption(OPT_DNA_TEXT)
		.addOption(OPT_EN_TEXT)
		.addOption(OPT_PROG_TEXT)
		.addOption(OPT_CSIZE, true)
		.addOption(OPT_LOG);
		if(!clp.parse(args)) throw new IllegalArgumentException("invalid options!" + System.getProperty("line.seperator") + USAGE);
		String xmlPath = HOCompressor.checkOption(clp);
		String srcDir = (new File(xmlPath)).getParent() + "/";
		String srcFile = (new File(xmlPath)).getName();
		
		if(!DiffChecker.xmlDiff(srcDir, srcFile, srcDir, srcFile+".dec")) throw new IllegalArgumentException("different!");
	}

	//それぞれファイルを読み込んだλ式を比較している（スペース、タブ、改行などは無視している。そのうち実装）
	public static boolean xmlDiff(String srcDir, String srcXml, String decompDir, String decompXml){
		XmlLoader srcLoader = new XmlLoader(srcDir, srcXml);
		XmlLoader decompLoader = new XmlLoader(decompDir, decompXml);
		TyRoot src = srcLoader.parsedTerm();
		TyRoot decomp = decompLoader.parsedTerm();
		return src.getChild().isSame(decomp.getChild());
	}
}
