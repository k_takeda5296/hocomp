package util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;


/**
 * ディレクトリlog/yyyyMMdd/以下に、受け取ったファイル名のログファイルを出力する。すでにあるファイル名を指定した場合は追記する。
 * @author k_takeda
 *
 */
public class MyLogger {

	private String outPath_;
	private String outFile_;
	private BufferedWriter writer_ = null;

	public MyLogger(String fileName){
		init(fileName);
	}
	
	public static String getLogDir(){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-HHmmss");
		sdf = new SimpleDateFormat("yyyyMMdd");
		String strDate = sdf.format(cal.getTime());
		return "log/" + strDate + "/";
	}
	
	private void init(String fileName){
		outPath_ = getLogDir();
		outFile_ = fileName;
		try {
			if(!(new File(outPath_).exists())) (new File(outPath_)).mkdir();
			writer_ = new BufferedWriter(new FileWriter(new File(outPath_+outFile_), true));
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	public void println(String str){
		try {
			writer_.write(str);
			writer_.newLine();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}

	public void println(Object obj){
		println(obj.toString());
	}

	
	public void close(){
		try {
			writer_.flush();
			writer_.close();
		} catch (IOException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}
}
