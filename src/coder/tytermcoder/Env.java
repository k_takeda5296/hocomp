package coder.tytermcoder;

import java.util.HashSet;
import java.util.Set;

public class Env {

	private Set<Integer> set_ = new HashSet<>();
	
	Env(){}
	
	void addVar(int var){
		if(set_.contains(var)) throw new IllegalArgumentException("This var is already contained!");
		set_.add(var);
	}
	
	void removeVar(int var){
		if(!set_.contains(var)) throw new IllegalArgumentException("This var is not contained!");
		set_.remove(var);
	}
	
	boolean contains(int var){
		return set_.contains(var);
	}
}
