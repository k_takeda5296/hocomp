package coder.tytermcoder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalInt;

import util.MyUtil;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Type;
import compressor.HOCompressor;
import compressor.encoder.Encoder;

public class TyTermCoder implements Encoder{

	String dir_;
	String file_;
	TyRoot root_;
	
	public TyTermCoder(String dir, String file, TyRoot root){
		dir_ = "ocaml/tyterm/"+dir;
		MyUtil.makeDirIfNotExists(dir_);
		file_ = file+".tyterm";
		root_ = root;
	}
	
	/**
	 * （単純）型付きλ項を最左導出でエンコードする。
	 * ・最初の4バイトはλ項のシンボル名・変数名の最大バイト数 N（文字列のまま書き出す）
	 * ・次の4バイトは型のバイト数 M
	 * ・残りは木構造の最左導出データ
	 * ・各ノードは1+N+Mバイトであり、1バイト目がシンボル(0)・変数(1)・λ抽象(2)・関数適用(3)のいずれか、次のNバイトがシンボル名・変数名（関数適用の時は読み捨てられる）、残りのMバイトが型）
	 * ・型は単純型をビット表現したもの
	 * 
	 * 12/12追記：
	 * （単純）型付きλ項のド・ブラン表現（の逆）を最左導出でエンコードする。
	 * 最初の4バイトはλ項のシンボル名の最大バイト数N（文字列のまま書き出す）
	 * 次の4バイトはシンボルの数M
	 * 次はNMバイトはシンボルの列
	 * 次の4バイトは最大の逆ドブラン表現のインデックスL
	 * 残りは木構造の最左導出データ（逆ドブラン表現）
	 * 
	 * 2/6追記
	 * 初期の環境（TySymbolの環境）を先頭にエンコードする
	 */
	@Override
	public void encode() {
		List<Byte> byteList = new ArrayList<>();
		encodeEnv(byteList);
		encodeTerm(byteList);
		MyUtil.outputByteList(byteList, dir_+file_);
	}
	private void encodeEnv(List<Byte> byteList){
		final Map<Type, Integer> env = new HashMap<>();
		for(Type ty : root_.getSymbolTypes()){
			if(!env.containsKey(ty)) env.put(ty, 0);
			env.put(ty, env.get(ty)+1);
		}
		MyUtil.addInt2ByteList(env.size(), byteList);
		
		Optional<Integer> maxTypeLen = env.keySet().stream().map(ty -> env.get(ty)).max((x,y) -> Integer.compare(x, y));
		if(!maxTypeLen.isPresent()) assert(false);
		MyUtil.addInt2ByteList(maxTypeLen.get(), byteList);//型の最大バイト数
		System.out.println("TyTermCoder#encodeEnv:	maxtypeLen:"+maxTypeLen.get());
		env.forEach((ty,occ) ->{
			byte[] tBytes = bytesOfType(ty,maxTypeLen.get());
			for(byte b : tBytes) byteList.add(b);
			MyUtil.addInt2ByteList(occ, byteList);
		});
	}
	
	private byte[] bytesOfType(Type type, int n){
		byte[] tBytes = MyUtil.boolList2ByteArray(type.getBits());
		if(tBytes.length > n) throw new IllegalArgumentException("type-bits is longer than the expected!	type:"+type+"	n:"+n);
		byte[] bytes = new byte[n];
		for(int i=0; i < tBytes.length; i++) bytes[i] = tBytes[i];
		return bytes;
	}
	
	private void encodeTerm(List<Byte> byteList){
		int N = maxNameLength();
		int M = root_.getSymbolNames().size();
		MyUtil.addInt2ByteList(N, byteList);
		MyUtil.addInt2ByteList(M, byteList);
		symbols2ByteList(root_.getSymbolNames(), N, M, byteList);
		int l = deBurijnMaxIndex();
		int L = 3;//l < 0 ? -1 : l < 256 ? 1 : l < Math.pow(256, 2) ? 2 : l < Math.pow(256, 3) ? 3 : -1;//一時しのぎ
		if(L < 0) throw new IllegalArgumentException("too huge deBurijn Index!");
		MyUtil.addInt2ByteList(L, byteList);
		int K = (maxTypeBitsLength(root_.getChild(), -1) - 1)/8 + 1;
		if(K < 0) throw new IllegalArgumentException("negative max length!");
		MyUtil.addInt2ByteList(K, byteList);
		System.out.println("TyTermCoder#encodeTerm	N:"+N+"\tM:"+M+"\tL:"+L+"\tK:"+K);
		Map<Type, Map<Integer, Integer>> indexMap = initIndexMap();
		term2ByteList(root_.getChild(), K, L, indexMap, byteList);
	}
	
	private Map<Type, Map<Integer, Integer>> initIndexMap(){
		Map<Type, Map<Integer, Integer>> indexMap = new HashMap<>();
		root_.getSymbols().stream().forEach(s -> {
			putVar(indexMap, s.getType(), s.getName());
		});
		return indexMap;
	}
	
	private int deBurijnMaxIndex(){
		OptionalInt maxAbstDepth = root_.getAllAbstChildren().stream().mapToInt(a -> a.depth()).max();
		assert(root_.getSymbolNames().size() > 0);
		return root_.getSymbolNames().size()-1 + (maxAbstDepth.isPresent() ? maxAbstDepth.getAsInt() : 0);//abstが無い場合は深さ0
	}
	
	private void symbols2ByteList(List<String> symbols, int N, int M, List<Byte> byteList){
		if(symbols.size()!=M) throw new IllegalArgumentException("invalid size or M!");
		for(String name : symbols){
			addSymbol2ByteList(name, N, byteList);
		}
	}
	
	private void addSymbol2ByteList(String name, int N, List<Byte> byteList){
		for(int i=0; i < N; i++){
			if(i < name.getBytes().length) byteList.add(name.getBytes()[i]);
			else byteList.add((byte)0);
		}
	}
	
	/**
	 * termを逆ドブラン表現での最左導出でbyteListに書き出す
	 * @param term
	 * @param L
	 * @param indexMap
	 * @param byteList
	 */
	private static final int APP_INDEX = 0;
	private static final int ABST_INDEX = 1;
	private static final int INDEX_OVERHEAD = 2;
	private void term2ByteList(TyTerm term, int K, int L, Map<Type, Map<Integer, Integer>> indexMap, List<Byte> byteList){
		if(term instanceof TySymbol){
			TySymbol symbol = (TySymbol)term;
			if(!indexMap.containsKey(symbol.getType())) throw new IllegalArgumentException("no match type!");
			addNodeBytes(symbol.getType(), K, INDEX_OVERHEAD+indexMap.get(symbol.getType()).get(symbol.getName()), L, byteList);
			return;
		}else if(term instanceof TyVar){
			TyVar var = (TyVar)term;
			if(!indexMap.containsKey(var.getType())) throw new IllegalArgumentException("no match type!");
			addNodeBytes(var.getType(), K, INDEX_OVERHEAD+indexMap.get(var.getType()).get(var.getName()), L, byteList);
			return;
		}else if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			addNodeBytes(abst.getType(), K, ABST_INDEX, L, byteList);
			putVar(indexMap, abst.getArgType(), abst.getName());
			term2ByteList(abst.getChild(), K, L, indexMap, byteList);
			removeVar(indexMap, abst.getArgType(), abst.getName());
			return;
		}else if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			addNodeBytes(app.getType(), K, APP_INDEX, L, byteList);
			term2ByteList(app.getLeftChild(), K, L, indexMap, byteList);
			term2ByteList(app.getRightChild(), K, L, indexMap, byteList);
			return;
		}
		throw new IllegalArgumentException("no match class!");
	}
	
	private void putVar(Map<Type, Map<Integer, Integer>> indexMap, Type type, Integer name){
		if(!indexMap.containsKey(type)) indexMap.put(type, new HashMap<>());
		Map<Integer, Integer> map = indexMap.get(type);
		if(map.containsKey(name)) throw new IllegalArgumentException("dublicate abst names!	name:"+name);
		map.put(name, map.size());
	}
	
	private void removeVar(Map<Type, Map<Integer, Integer>> indexMap, Type type, Integer name){
		if(!indexMap.containsKey(type)) throw new IllegalArgumentException("no map for type!	name:"+name);
		Map<Integer, Integer> map = indexMap.get(type);
		if(!map.containsKey(name)) throw new IllegalArgumentException("unexpectedly removed abst!	name:"+name);
		map.remove(name);
	}
	
	private void addNodeBytes(Type type, int K, int index, int L, List<Byte> byteList){
		byte[] bytes = node2Bytes(type, K, index, L);
		for(byte b : bytes) byteList.add(b);
	}
	
	private byte[] node2Bytes(Type type, int K, int index, int L){
		byte[] bytes = new byte[K+L];
		byte[] indexBytes = MyUtil.int2ByteArray(index);
		for(int i=0; i < L; i++){
			if(i < indexBytes.length) bytes[i] = indexBytes[i + (4-L)];
			else bytes[i] = 0;
		}
		byte[] tBytes = MyUtil.boolList2ByteArray(type.getBits());
		for(int i=L; i < K+L; i++){
			if(i-L < tBytes.length) bytes[i] = tBytes[i-L];
			else bytes[i] = 0;
		}
		return bytes;
	}
	
	/**
	 * λ項に現れるシンボル・変数の名前（文字列）の最大の長さ
	 * @return
	 */
	private int maxNameLength(){
		OptionalInt on1 = root_.getSymbolNames().stream().mapToInt(s -> s.length()).max();
		int n1 = on1.isPresent() ? on1.getAsInt() : -1;
		OptionalInt on2 = root_.getAllAbstChildren().stream().mapToInt(a -> a.idStr().length()).max();
		int n2 = on2.isPresent() ? on1.getAsInt() : -1;
		int max = Math.max(n1, n2);
		if(max < 0) throw new IllegalArgumentException("negative max length!");
		return max;
	}
	
	private int maxTypeBitsLength(TyTerm term, int n){
		int max = Math.max(n, term.getType().getBits().size());
		if(term instanceof TyAbst){
			return maxTypeBitsLength(((TyAbst)term).getChild(), max);
		}else if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			return maxTypeBitsLength(app.getLeftChild(), maxTypeBitsLength(app.getRightChild(), max));
		}
		return max;
	}

}
