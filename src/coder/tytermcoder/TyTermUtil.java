package coder.tytermcoder;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;

public class TyTermUtil {

	public static TyVar DELIMITER = new TyVar(null);
	/*
	 * 連続するlet文をブロックに分けてソートした順序にし、ブロックの区切りには特別なlet分を挟む
	 */
	public static TyRoot treatLetOrder(TyRoot term){
		return term;
	}

	public static void treatLetOrderAux(TyTerm term, Env env){
		if(term instanceof TySymbol){

		}else if(term instanceof TyVar){

		}else if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			env.addVar(abst.getName());
			treatLetOrderAux(abst.getChild(), env);
			env.removeVar(abst.getName());
		}else if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			if(app.getLeftChild() instanceof TyAbst){
				orderLetSeq(app, env);
			}else{
				treatLetOrderAux(app.getLeftChild(), env);
				treatLetOrderAux(app.getRightChild(), env);
			}
		}else{
			throw new IllegalArgumentException("no such class!");
		}
	}

	/*
	 * 引数のLet文から始まる連続するLet文をブロックに分けてソートし、宣言した項とボディの項を再帰的にtreatLetOrderAuxにかける
	 */
	public static void orderLetSeq(TyApp app, Env env){
		
	}
}
