package coder.variablecoder;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * Variable Byte Codeによる整数値圧縮を実装する。
 * 
 */
public class VariableByteCode {
	
	
	public static byte[] getBytes(List<Integer> iList){
		byte[] bytes = preprocess(iList);
		compress(iList, bytes);
		return bytes;
	}
	
	/**
	 * 
	 * 整数値の可変バイトコードでのサイズを取得するメソッドである。
	 * 
	 * @param pNumber 整数値
	 * @return 可変バイトコードでのサイズ
	 */
	private static int estimateByteNum( final int pNumber ) {
		
		int num = 0;
		
		if ( (1 << 7) > pNumber ) {
			num = 1;
		} else if ( (1 << 14) > pNumber ) {
			num = 2;
		} else if ( (1 << 21) > pNumber ) {
			num = 3;
		} else if ( (1 << 28) > pNumber ) {
			num = 4;
		} else {
			num = 5;
		}
		
		return num;
	}

	/**
	 * 
	 * VBCodeに変換するメソッドである。
	 * 
	 * @param pNumber 整数値
	 */
	static int mOffset = 0;
	private static void encodeVBCodeNumber( final int pNumber, byte[] byteArray ) {
		

		int num = estimateByteNum( pNumber );
		int lastOffset = 0;
		
		switch ( num ) {
		case 1:
			break;
		case 2:
			byteArray[mOffset] = (byte) ((pNumber >> 7) & 0x7F);
			lastOffset = 1;
			break;
		case 3:
			byteArray[mOffset] = (byte) ((pNumber >> 14) & 0x7F);
			byteArray[mOffset + 1] = (byte) ((pNumber >> 7) & 0x7F);
			lastOffset = 2;
			break;
		case 4:
			byteArray[mOffset] = (byte) ((pNumber >> 21) & 0x7F);
			byteArray[mOffset + 1] = (byte) ((pNumber >> 14) & 0x7F);
			byteArray[mOffset + 2] = (byte) ((pNumber >> 7) & 0x7F);
			lastOffset = 3;
			break;
		case 5:
			byteArray[mOffset] = (byte) ((pNumber >> 28) & 0x7F);
			byteArray[mOffset + 1] = (byte) ((pNumber >> 21) & 0x7F);
			byteArray[mOffset + 2] = (byte) ((pNumber >> 14) & 0x7F);
			byteArray[mOffset + 3] = (byte) ((pNumber >> 7) & 0x7F);
			lastOffset = 4;
			break;
		}
		
		byteArray[mOffset + lastOffset] = (byte) ((pNumber & 0x7F) | 0x80);
		
		mOffset += num;
		
		return;
	}
	
	/**
	 * 
	 * 前処理を行うメソッドである。
	 * 
	 */
	public static byte[] preprocess(List<Integer> iList) {
		
		mOffset = 0;
		int byteNum = 0;
		int arraySize = 0;
		
		for ( int i = 0; i < iList.size(); i++ ) {
			byteNum = estimateByteNum(iList.get(i));
			arraySize += byteNum;
		}
		
		return new byte[arraySize];
	}
	
	/**
	 * 
	 * 可変バイトに圧縮するメソッドである。
	 * 
	 */
	public static byte[] compress(List<Integer> iList, byte[] bytes) {
		
		for ( int i = 0; i < iList.size(); i++ ) {
			encodeVBCodeNumber( iList.get(i), bytes );
		}
		
		assert( mOffset == iList.size() );
		
		return bytes;
		
	}
	
	/**
	 * 
	 * 可変バイトをデコードするメソッドである。
	 * 
	 */
	public static List<Integer> decode(byte[] bytes) {
		List<Integer> iList = new ArrayList<Integer>();
		int value = 0;
		int byteValue = 0;
		
		for ( int i = 0; i < bytes.length; i++ ) {
			byteValue = bytes[i];
			value <<= 7;
			value |= (byteValue & 0x7F);
			if ( 0 != (byteValue & 0x80) ) {
				iList.add(value);
				value = 0;
			}
		}
		
		return iList;
		
	}

}