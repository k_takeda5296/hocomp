package coder.scfg;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import util.MyUtil;

/**
 * http://www.geocities.jp/m_hiroi/light/pyalgo36.html
 * @author koutaro_t
 *
 */
public class RangeDecoder{

	private static final long MAX_RANGE = RangeCoder.MAX_RANGE;
	private static final long MIN_RANGE = RangeCoder.MIN_RANGE;//3バイトの符号無し最大値（符号無し0xffffff）
	
	
	private long code_;
	private long range_;
	
	
	private int size_;
	private List<Byte> byteList_;
	private List<Integer> intList_;
	
	public RangeDecoder(List<Byte> byteList, int size){
		size_ = size;
		intList_ = new ArrayList<>();
		byteList_ = MyUtil.reversedArrayList(byteList);
		System.out.println("RangeDecoder##	byteList:"+byteList_);
		popCode();
		popCode();
		popCode();
		popCode();
		range_ = MAX_RANGE;
		System.out.println("RangeDecoder##	byteList:"+byteList_);
		System.out.println("RangeDecoder##	byteList:"+byteList_+"	range:"+range_+"	code:"+code_);
		System.out.println("RangeDecoder##	range:"+String.format("0x%x", range_));
		System.out.println("RangeDecoder##	code:"+String.format("0x%x", code_));
	}
	
	
	/**
	 * byteListから1つ要素をポップしてcodeを更新する
	 */
	private void popCode(){
		byte b;
		if(byteList_.size() < 1) b = 0;
		else b = byteList_.remove(byteList_.size()-1);
		code_ *= 256;
		code_ += byte2uInt(b);
	}
	
	private int byte2uInt(byte b){
		return  b & 0xff;
	}
	
	/**
	 * 256^4以下のlongを受け取って256進数表記に文字列を返す
	 * @param l
	 * @return
	 */
	private static String long2Str(long l){
		String str = "";
		int max = 4;
		for(int i=max; i > -1; i--) str += longPer256(l, i)+" + ";
		return str.substring(0, str.length()-3);
	}
	private static String longPer256(long l, int n){
		return "256^"+n+"*"+l%(long)Math.pow(256, n+1)/(long)Math.pow(256, n);
	}
	
	/**
	 * 頻度表freqのi番目の区間を入力としてエンコードを進める
	 * @param freq
	 * @param i
    def decode(self, rc):
        # 記号の探索
        def search_code(value):
            i = 0
            j = self.size - 1
            while i < j:
                k = (i + j) / 2
                if self.count_sum[k + 1] <= value:
                    i = k + 1
                else:
                    j = k
            return i
        #
        temp = rc.range / self.count_sum[self.size]
        c = search_code(rc.low / temp)
        rc.low -= temp * self.count_sum[c]
        rc.range = temp * self.count[c]
        rc.decode_normalize()
        return c
	 */
	public void decode(Freq freq){
		long tmp = range_ / freq.getSum();
		System.out.println("RangeDecoder#decode	tmp:"+tmp);
		System.out.println("RangeDecoder#decode	code:"+code_);
		int i = freq.getIndexOf(code_/tmp);
		intList_.add(i);
		System.out.println("RangeDecoder#decode	code:"+code_+"	range:"+range_+"	i:"+i);
		System.out.println("RangeDecoder#decode	code:"+long2Str(code_));
		System.out.println("RangeDecoder#decode	range:"+long2Str(range_));
		
		range_ = freq.rangeOf(i) * tmp;
		long gapLow = freq.lowOf(i) * tmp;
		System.out.println("RangeDecoder#decode	code:"+code_+"	range:"+range_+"	i:"+i+"	gapLow:"+long2Str((long)gapLow));
		System.out.println("RangeDecoder#decode	code:"+code_+"	range:"+range_+"	i:"+i+"	gapLow:"+long2Str((long)gapLow)+"	tmp:"+long2Str((long)tmp));
		System.out.println("RangeDecoder#decode	code:"+code_+"	range:"+range_+"	i:"+i+"	gapLow:"+long2Str((long)gapLow)+"	tmp:"+long2Str((long)tmp)+"	lowOf:"+freq.lowOf(i));
		code_ -= gapLow;
		decodeNormalize();
	}
	
	/**
	 * lowとrangeを正規化する
	 * 
    def decode_normalize(self):
        while self.range < MIN_RANGE:
            self.range <<= 8
            self.low = ((self.low << 8) + getc(self.file)) & MASK
	 */
	private void decodeNormalize(){
		while(range_ < MIN_RANGE){//buffに出力して全体を256倍する
			popCode();
			range_ *= 256;
		}
		System.out.println("RangeCoder#decodeNormalize1	range:		"+range_+"	low:"+code_);
	}
	
	/**
	 * 与えられたsizeだけのシンボルがdecode済みでなければtrue、そうでなければfalseを返す
	 * @return
	 */
	public boolean ongoing(){
		return intList_.size() < size_;
	}

	public List<Integer> getIntList(){
		return intList_;
	}

}
