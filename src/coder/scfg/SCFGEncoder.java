package coder.scfg;

import component.tyterm.term.TyRoot;
import compressor.encoder.Encoder;

public class SCFGEncoder implements Encoder{

	private TyRoot root_;
	private String dir_;
	private String file_;
	
	private SCFGRangeCoder rc_;
	private SCFG scfg_;
	
	public SCFGEncoder(TyRoot root, String dir, String file){
		root_ = root;
		dir_ = dir;
		file_ = file;
		scfg_ = term2SCFG(root);
		rc_ = new SCFGRangeCoder(scfg_);
	}
	
	private SCFG term2SCFG(TyRoot root){
		return null;
	}
	
	@Override
	public void encode(){
		
	}
}
