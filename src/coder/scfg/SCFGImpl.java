package coder.scfg;




public class SCFGImpl implements SCFG{
	
	private CFG cfg_;
	
	@Override
	public CFG.Rule[] getRules(){
		return cfg_.getRules();
	}

	@Override
	public Symbol getStartSymbol() {
		return cfg_.getStartSymbol();
	}
}
