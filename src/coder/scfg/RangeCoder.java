package coder.scfg;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import util.MyUtil;

/**
 * http://www.geocities.jp/m_hiroi/light/pyalgo36.html
 * @author koutaro_t
 *
 */
public class RangeCoder{

	static final long MAX_RANGE = (long)Integer.MAX_VALUE*2 + 2;//4バイトの符号無し最大値（0xffffffff）
	static final long MIN_RANGE = MAX_RANGE/256 - 1;//3バイトの符号無し最大値（符号無し0xffffff）
	static final long MASK = 0xffffffff;
	static final long SHIFT = 256*256*256;
	
	
	private long low_;
	private long range_;
	
	private boolean buffInit_;
	private byte buff_;
	private int carry_;
	
	private List<Byte> byteList_;
	
	public RangeCoder(){
		low_ = 0;
		range_ = MAX_RANGE;
		byteList_ = new ArrayList<>();
		buffInit_ = false;
	}
	
	
	public static void main(String[] args){
		String str = "ddcbbaabaaa";
		Freq freq = Freq.str2Freq(str);
		System.out.println("RangeCoder#main	freq:"+freq);
		System.out.println("RangeCoder#main	freq:"+freq+"	MAX_RANGE:"+MAX_RANGE);
		System.out.println("RangeCoder#main	freq:"+freq+"	MAX_RANGE:"+MAX_RANGE+"	MIN_RANGE:"+MIN_RANGE);
		RangeCoder rc = new RangeCoder();
		Map<Integer, Integer> charIndexMap = new LinkedHashMap<>();
		str.chars().forEach(c -> {
			if(!charIndexMap.containsKey(c)) charIndexMap.put(c, charIndexMap.size());
		});
		//エンコード
		str.chars().forEach(c -> rc.encode(freq, freq.size()-1-charIndexMap.get(c)));
		rc.flush();
		List<Byte> byteList = rc.getByteList();
		System.out.println("RangeCoder#main	str:"+str);
		System.out.println("RangeCoder#main	byteList:"+byteList);
		for(byte b : byteList) System.out.printf("0x%x,", b);
		System.out.println();
		
		//decode
		RangeDecoder rd = new RangeDecoder(byteList, str.length());
		while(rd.ongoing()) rd.decode(freq);
		System.out.println("RangeCoder#main	intList:"+rd.getIntList());
	}
	
	
	/**
	 * 256^4以下のlongを受け取って256進数表記に文字列を返す
	 * @param l
	 * @return
	 */
	private static String long2Str(long l){
		String str = "";
		int max = 4;
		for(int i=max; i > -1; i--) str += longPer256(l, i)+" + ";
		return str.substring(0, str.length()-3);
	}
	private static String longPer256(long l, int n){
		return "256^"+n+"*"+l%(long)Math.pow(256, n+1)/(long)Math.pow(256, n);
	}
	
	/**
	 * 頻度表freqのi番目の区間を入力としてエンコードを進める
	 * @param freq
	 * @param i
	 * def encode(self, rc, c):
        temp = rc.range / self.count_sum[self.size]
        rc.low += self.count_sum[c] * temp
        rc.range = self.count[c] * temp
        rc.encode_normalize()
	 */
	public void encode(Freq freq, int i){
		System.out.println("RangeCoder#encode0	i:"+i+"	freq:"+freq+"	range:"+range_+"	low:"+low_);
		System.out.println("RangeCoder#encode0	i:"+i+"	freq:"+freq+"	range:	"+long2Str(range_));
		System.out.println("RangeCoder#encode0	i:"+i+"	freq:"+freq+"	low:	"+long2Str(low_));
		long tmp = range_ / freq.getSum();
		range_ = (long) (freq.rangeOf(i) * tmp);
		long gapLow = freq.lowOf(i) * tmp;
		low_ += gapLow;
		System.out.println("RangeCoder#encode1	i:"+i+"	freq:"+freq+"	range:"+range_+"	low:"+low_+"	gapLow:"+(long)gapLow);
		System.out.println("RangeCoder#encode1	i:"+i+"	freq:"+freq+"	range:	"+long2Str(range_));
		System.out.println("RangeCoder#encode1	i:"+i+"	freq:"+freq+"	low:	"+long2Str(low_));
		encodeNormalize();
	}
	
	/**
	 * lowとrangeを正規化する
	 * 
	 * # 定数
ENCODE = "encode"
DECODE = "decode"
MAX_RANGE = 0x100000000
MIN_RANGE = 0x1000000
MASK      = 0xffffffff
SHIFT     = 24
	 * # 符号化の正規化
    def encode_normalize(self):
        if self.low >= MAX_RANGE:
            # 桁上がり
            self.buff += 1
            self.low &= MASK
            if self.cnt > 0:
                putc(self.file, self.buff)
                for _ in xrange(self.cnt - 1): putc(self.file, 0)
                self.buff = 0
                self.cnt = 0
        while self.range < MIN_RANGE:
            if self.low < (0xff << SHIFT):
                putc(self.file, self.buff)
                for _ in xrange(self.cnt): putc(self.file, 0xff)
                self.buff = (self.low >> SHIFT) & 0xff
                self.cnt = 0
            else:
                self.cnt += 1
            self.low = (self.low << 8) & MASK
            self.range <<= 8
	 */
	private void encodeNormalize(){
		System.out.println("RangeCoder#encodeNormalize0	range:		"+range_+"	low:"+low_);
		System.out.println("RangeCoder#encodeNormalize0	range:		"+range_+"	low:"+low_+"	range:"+String.format("0x%x", range_)+"	low:"+String.format("0x%x", low_));
		System.out.println("RangeCoder#encodeNormalize0	MIN_RANGE:	"+MIN_RANGE);
		if(low_ > MAX_RANGE){//桁上がり
			System.out.println("RangeCoder#encodeNormalize4	range:		"+range_+"	low:"+low_+"	range:"+String.format("0x%x", range_)+"	low:"+String.format("0x%x", low_));
			buff_ += 1;
			low_ %= MAX_RANGE;
			if(carry_ > 0){
				byteList_.add(buff_);
				buffInit_ = true;
				MyUtil.repeat(carry_-1, () -> byteList_.add((byte) 0));
				buff_ = 0;
				carry_ = 0;
			}
		}
		while(range_ < MIN_RANGE){//buffに出力して全体を256倍する
			System.out.println("RangeCoder#encodeNormalize3	low&MASK:"+(low_&MASK)+"	0xff<<SHIFT:"+((long)0xff*SHIFT));
			if((low_ & MASK) < ((long)0xff *SHIFT)){
				if(buffInit_) byteList_.add(buff_);
				buffInit_ = true;
				System.out.println("RangeCoder#encodeNormalize2	buff:"+String.format("0x%x", buff_));
				MyUtil.repeat(carry_, () -> byteList_.add((byte) 0xff));
				buff_ = (byte)((low_ / SHIFT) & 0xff);
				carry_ = 0;
			}else{
				carry_++;
			}
			try{
			low_ = (low_ * 256) % (SHIFT*256);
			}catch(Exception e){
				System.err.println(" low:"+low_+"	SHIFT:"+SHIFT*256);
				throw e;
			}
			range_ *= 256;
		}
		System.out.println("RangeCoder#encodeNormalize1	range:		"+range_+"	low:"+low_);
		System.out.println("RangeCoder#encodeNormalize1	range:		"+range_+"	low:"+low_+"	buff:"+String.format("0x%x", buff_));
		System.out.println("RangeCoder#encodeNormalize1	range:		"+long2Str(range_));
		System.out.println("RangeCoder#encodeNormalize1	low:		"+long2Str(low_));
	}
	
	/**
	 * 最後に呼び出す
	 *     # 終了
    def finish(self):
        c = 0xff
        if self.low >= MAX_RANGE:
            # 桁上がり
            self.buff += 1
            c = 0
        putc(self.file, self.buff)
        for _ in xrange(self.cnt): putc(self.file, c)
        #
        putc(self.file, (self.low >> 24) & 0xff)
        putc(self.file, (self.low >> 16) & 0xff)
        putc(self.file, (self.low >> 8) & 0xff)
        putc(self.file, self.low & 0xff)
	 */
	public void flush(){
		byte c = (byte)0xff;
		if(low_ > MAX_RANGE){//桁上がり
			buff_++;
			c = 0;
		}
		if(buffInit_) byteList_.add(buff_);
		//MyUtil.repeat(carry_-1, i -> byteList_.add(c));
		byteList_.add((byte) (low_ >>> 24 & 0xff));
		byteList_.add((byte) (low_ >>> 16 & 0xff));
		byteList_.add((byte) (low_ >>> 8 & 0xff));
		byteList_.add((byte) (low_ >>> 0 & 0xff));
	}
	

	public List<Byte> getByteList(){
		return byteList_;
	}

}
