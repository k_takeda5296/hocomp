package coder.scfg;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * 頻度表を表すクラス
 * @author koutaro_t
 *
 */
class Freq{
	int[] sumCounts_;//sumCounts_[sumCounts_.length-1]が出現の合計を表す
	Freq(int[] counts){
		System.out.println("Freq##	counts:"+Arrays.toString(counts));
		sumCounts_ = new int[counts.length];
		int sum = 0;
		for(int i=0; i < counts.length; i++){
			sum += counts[i];
			sumCounts_[i] = sum;
		}
	}
	
	@Override
	public String toString(){
		return "Freq:{ "+Arrays.toString(sumCounts_)+" }";
	}
	
	/**
	 * この頻度表の総頻度数を返す
	 * @return
	 */
	int getSum(){
		return sumCounts_[sumCounts_.length-1];
	}
	
	/**
	 * i番目の区間の下限を返す
	 * @param i
	 * @return
	 */
	int lowOf(int i){
		if(i < 0 || i > sumCounts_.length-1) throw new IllegalArgumentException("Invalid index:"+i+" for array.length:"+sumCounts_.length);
		int low = i==0 ? 0 : sumCounts_[i-1];
		System.out.println("Freq#lowOf		i:"+i+"	sumCounts:"+Arrays.toString(sumCounts_)+"	low:"+low);
		return low;
	}
	
	/**
	 * i番目の区間の幅（すなわち、出現数）を返す
	 * @param i
	 * @return
	 */
	int rangeOf(int i){
		if(i < 0 || i > sumCounts_.length-1) throw new IllegalArgumentException("Invalid index:"+i+" for array.length:"+sumCounts_.length);
		return i==0 ? sumCounts_[0] : sumCounts_[i] - sumCounts_[i-1];
	}
	
	
	/**
	 * TODO:二分探索にする
	 * @param low
	 * @return
	 */
	int getIndexOf(long low){
		System.out.println("Freq#getIndexOf	low:"+low);
		for(int i=0; i < sumCounts_.length; i++){
			long boundary = lowOf(i);
			System.out.println("Freq#getIndexOf	low:"+low+"	boundary:"+boundary);
			if(low < boundary) return i-1;
		}
		return sumCounts_.length-1;
	}
	
	/**
	 * テーブルのサイズ（区間数）を返す
	 * @return
	 */
	int size(){
		return sumCounts_.length;
	}
	
	static Freq str2Freq(String str){
		Map<Integer, Integer> charIndexMap = new TreeMap<>();
		str.chars().forEach(c -> {
			if(!charIndexMap.containsKey(c)) charIndexMap.put(c, charIndexMap.size());
		});
		System.out.println("str2Freq:	treeMap:"+charIndexMap);
		int[] counts = new int[charIndexMap.size()];
		str.chars().sorted().forEach(c -> counts[counts.length-1-charIndexMap.get(c)]++);
		return new Freq(counts);
	}
}