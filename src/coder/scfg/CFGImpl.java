package coder.scfg;



public class CFGImpl implements CFG{

	Rule[] rules_;
	Symbol startSymbol_;

	public static void main(String[] args){
		Symbol E = new Symbol(false, "E");
		Symbol F = new Symbol(false, "F");
		Symbol a = new Symbol(true, "a");
		Symbol mul = new Symbol(true, "*");
		Symbol plus = new Symbol(true, "+");
		Rule rE1 = new Rule(E, 	new Symbol[]{ F });
		Rule rE2 = new Rule(E, new Symbol[]{ E, plus, F });
		Rule rF1 = new Rule(F, new Symbol[]{ a });
		Rule rF2 = new Rule(F, new Symbol[]{ F, mul, a });
		CFG cfg = new CFGImpl(new Rule[]{ rE1, rE2, rF1, rF2 });
		System.out.println("cfg:"+cfg);
	}

	CFGImpl(Rule[] rules){
		rules_ = rules;
	}

	@Override
	public String toString(){
		String str = "\n{\n";
		for(Rule r : rules_) str += "\t"+r+"\n";
		str += "}\n";
		return str;
	}

	@Override
	public Rule[] getRules(){
		return rules_;
	}

	@Override
	public Symbol getStartSymbol() {
		return startSymbol_;
	}

}
