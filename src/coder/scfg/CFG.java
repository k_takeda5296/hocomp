package coder.scfg;

import java.util.Arrays;

public interface CFG {

	Rule[] getRules();
	Symbol getStartSymbol()	;
	
	
	static class Rule{
		Symbol left_;
		Symbol[] right_;
		Symbol[] nonTerminals_;

		Rule(Symbol left, Symbol[] right){
			left_ = left;
			right_ =right;
			nonTerminals_ = Arrays.stream(right).distinct().toArray(Symbol[]::new);
		}

		@Override
		public String toString(){
			String str = left_.toString()+" -> "+Arrays.toString(right_);
			return str;
		}
		Symbol ithNonTerminal(int i){
			return nonTerminals_[i];
		}
	}
	
	static class Symbol{
		boolean isTerminal_;
		String name_;

		Symbol(boolean isTerminal, String name){
			isTerminal_ = isTerminal;
			name_ = name;
		}

		@Override
		public String toString(){
			return name_;
		}

		@Override
		public boolean equals(Object obj){
			if(!(obj instanceof Symbol)) return false;
			return this.name_.equals(((Symbol)obj).name_);
		}

		@Override
		public int hashCode(){
			return name_.hashCode();
		}
	}
}
