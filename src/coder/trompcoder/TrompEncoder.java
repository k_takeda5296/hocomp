package coder.trompcoder;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.MyUtil;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import compressor.HOCompressor;

public class TrompEncoder {

	public static long trompCompSizeOf(TyRoot root, String fname){
		String trompFile = fname+".tromp";
		OutputStream output;
		try {
			output = new BufferedOutputStream(new FileOutputStream(trompFile));
			bitCount(root.getChild(), root, new HashMap<Integer, Integer>(), new boolean[8], 0, output);
			output.flush();
			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String compFile = compressTrompFile(trompFile);
		long compFileLen = (new File(compFile)).length();
		return compFileLen;
	}
	
	public static String compressTrompFile(String inputFile){
		String cmd = "bzip2 -f " + inputFile;
		if(HOCompressor.PRINT_CMD) System.out.println("TrompEncoder#compressLabelFile	cmd:"+cmd);
		Runtime r = Runtime.getRuntime();
		try {//pythonコマンドが通らないときはpyコマンドを試す
			Process p = r.exec(cmd);
			p.waitFor();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return inputFile+".bz2";
	}
	
	private static int bitCount(TyTerm term, TyRoot root, Map<Integer, Integer> varMap, boolean[] bools, int index, OutputStream out) throws IOException{
		if(term instanceof TySymbol){
			int s = root.getSymbolId((TySymbol)term);
			index = addBitsVar(bools, index, s, out);
			return index;
		}else if(term instanceof TyVar){
			TyVar var = (TyVar)term;
			int v = varMap.get(var.getName()) + 2;
			index = addBitsVar(bools, index, v, out);
			return index;
		}else if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			varMap.put(abst.getName(), varMap.size() + root.getSymbols().size());
			index = addBit(false, bools, index, out);
			index = addBit(false, bools, index, out);
			index = bitCount(abst.getChild(), root, varMap, bools, index, out);
			varMap.remove(abst.getName());
			return index;
		}else if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			index = addBit(false, bools, index, out);
			index = addBit(true, bools, index, out);
			index = bitCount(app.getLeftChild(), root, varMap, bools, index, out);
			index = bitCount(app.getRightChild(), root, varMap, bools, index, out);
			return index;
		}else{
			throw new IllegalArgumentException();
		}
	}
	
	private static int addBit(boolean b, boolean[] bools, int index, OutputStream out) throws IOException{
		int i = index;
		index = (index+1)%8;
		bools[i] = b;
		if(index==0){
			int bb = 0;
			for(int k=0; k<8; k++) bb = bools[k] ? bb*2 + 1 : bb*2;
			if(bb < 0 || bb > 255) throw new IllegalArgumentException("bb out of bounds: "+bb);
			byte[] bbb = new byte[]{ (byte)bb };
			out.write(bbb);
		}
		return index;
	}
	
	private static int addBitsVar(boolean[] bools, int index, int var, OutputStream out) throws IOException{
		for(int i=0; i < var+1; i++){
			index = addBit(true, bools, index, out);
		}
		index = addBit(false, bools, index, out);
		return index;
	}
}
