package coder.repreader;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;

import main.Checker;
import util.MyUtil;
import coder.tytermcoder.TyTermCoder;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyId;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.type.Fun;
import component.tyterm.type.O;
import component.tyterm.type.Type;
import compressor.HOCompressor;
import compressor.encoder.Encoder;
import compressor.encoder.EncoderImpl;

public class RepairReader {
	
	private final static String DIR = "syuronnBench/all/";
	static String FILE = "1998statistics.xml";
	private final static String EXT = ".nonames.reptxt";
	
	public static void main(String[] args){
		compress(null);

		HOCompressor.LOGGER.close();
	}		
	public static void compress(String logName){
		HOCompressor.COMP_AS_BINARY_TREE = false;
		
		String fname = FILE+EXT;
		String path = DIR+fname;
		
		long realStart, end;
		long cpuStart, cpuEnd;
		long userStart, userEnd;
		ThreadMXBean bean = ManagementFactory.getThreadMXBean();
		cpuStart = bean.getCurrentThreadCpuTime();
		userStart = bean.getCurrentThreadUserTime();
		realStart = System.currentTimeMillis();
		
		RepairReader rr = new RepairReader(path);
		
		cpuEnd = bean.getCurrentThreadCpuTime();
		userEnd = bean.getCurrentThreadUserTime();
		end = System.currentTimeMillis();
		long realTime = end - realStart;
		long cpuTime = cpuEnd - cpuStart;
		long userTime = userEnd - userStart;
		
		HOCompressor.outputTermInfo(rr.root_, DIR, fname, 0, realTime, cpuTime, userTime, logName);
		System.out.println("RepairReader#main	rr.type:"+rr.root_.getChild().getType()+"	rr.valid:"+Checker.validTyTermCheck(rr.root_));
		//エンコードと書き出し
		Encoder encoder = new EncoderImpl(rr.root_, DIR, fname, HOCompressor.TYPE_CLASSIFYING_ENCODE);
		encoder.encode();
		
		Encoder termCoder = new TyTermCoder(DIR, fname, rr.root_);
		termCoder.encode();
	}
	
	private TyRoot root_;
	private Map<Integer, Rank> symbolRankMap_;
	
	public RepairReader(String path){
		root_ = readRepair(path);
	}

	public static enum Rank{
		C00(0), C01(1), C10(1), C11(2);
		
		private int rank_;
		
		private Rank(int rank){ rank_ = rank; }
		
		private int getRank(){ return rank_; }
	};

	private TyRoot readRepair(String path){
		try {
			Scanner sc = new Scanner(new File(path));
			int terminalNum = readTerminalNum(sc) + 1;
			symbolRankMap_ = readRankInfo(sc);
			List<String> nameList = readNameInfo(sc);
			TyRoot root = readTerm(sc, symbolRankMap_, terminalNum);
			addNameInfo(root, nameList, symbolRankMap_);
			return root;
		} catch (FileNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		throw new IllegalArgumentException();
	}
	
	private static void addNameInfo(TyRoot root, List<String> nameList, Map<Integer, Rank> symbolRankMap){
		for(int i=0; i < nameList.size(); i++){
			String name = nameList.get(i);
			int rank = symbolRankMap.get(i+1).getRank();
			Type type = typeOfRank(rank);
			root.addSymbolType(name, type);
		}
	}

	/*
	 * 文字列"# terminals N"をパースしてN:intを返す
	 */
	private static int readTerminalNum(Scanner sc){
		String terminalInfo = sc.nextLine();
		String[] strList = terminalInfo.split(" ");
		return Integer.parseInt(strList[2]);
	}

	private static Map<Integer, Rank> readRankInfo(Scanner sc){
		System.out.println("RePairReader#readRankInfo:\t"+sc.nextLine());//"RANK_INFO"の行を読み捨て
		Map<Integer, Rank> map = new HashMap<>();
		readRankInfoAux(sc, Rank.C00, map);
		readRankInfoAux(sc, Rank.C01, map);
		readRankInfoAux(sc, Rank.C10, map);
		readRankInfoAux(sc, Rank.C11, map);
		return map;
	}

	private static void readRankInfoAux(Scanner sc, Rank rank, Map<Integer, Rank> map){
		String line = sc.nextLine();
		String[] strList = line.split(" ");
		if(strList.length < 4) return;
		String[] idList = strList[3].split("=");
		for(String idStr : idList){
			int idInt = Integer.parseInt(idStr);
			map.put(idInt, rank);
		}
	}

	private static List<String> readNameInfo(Scanner sc){
		System.out.println("RePairReader#readNameInfo:\t"+sc.nextLine());//"NAME_INFO"の行を読み捨て
		String line = sc.nextLine();
		return Arrays.stream(line.split("=")).collect(Collectors.toList());
	}

	private static TyRoot readTerm(Scanner sc, Map<Integer, Rank> symbolRankMap, int terminalNum){
		System.out.println("RePairReader#readTerm:\t"+sc.nextLine());//"PRODUCTION_INFO~"の行を読み捨て
		Map<Integer, TyAbst> abstMap = new HashMap<>();
		TyRoot root = TyRoot.getRoot(null);
		TyAbst termFoot = root;
		List<String> symbolList = null;
		while(sc.hasNextLine()){
			List<String> prodAndSymbolList = readProdAndSymbolList(sc);
			String prodStr = prodAndSymbolList.remove(0);
			symbolList = prodAndSymbolList;
			if(prodStr.charAt(0)=='0') break;//最後の行は開始記号('0')なので別処理へ
			TyAbst abstF = makeAbstAndUpdateMap(prodStr, abstMap);
			TyTerm decTerm = makeTerm(symbolList, symbolRankMap, terminalNum, abstMap);
			TyApp app = new TyApp(abstF, decTerm, O.O);
			abstF.setParent(app); decTerm.setParent(app);
			termFoot.setChild(app);app.setParent(termFoot);
			termFoot = abstF;
		}
		TyTerm bodyTerm = makeTerm(symbolList, symbolRankMap, terminalNum, abstMap);
		termFoot.setChild(bodyTerm);bodyTerm.setParent(termFoot);
		
		return root;
	}

	private static List<String> readProdAndSymbolList(Scanner sc){
		List<String> prodAndSymbolList = new ArrayList<>();
		String line = sc.nextLine();
		String[] strList = line.split(" ");//P=rank -> s0=s1=...
		String prodStr = strList[0];//"P=rank"
		prodAndSymbolList.add(prodStr);
		String[] symbolList = strList[2].split("=");
		for(String symbol : symbolList) prodAndSymbolList.add(symbol);
		return prodAndSymbolList;
	}

	private static TyAbst makeAbstAndUpdateMap(String prodStr, Map<Integer, TyAbst> abstMap){
		String[] strList = prodStr.split("=");
		int prodId = Integer.parseInt(strList[0]);
		int rank = Integer.parseInt(strList[1]);
		Type type = new Fun(typeOfRank(rank), O.O);
		TyAbst abst =  new TyAbst(true, prodId, type);//f側
		abstMap.put(prodId, abst);
		return abst;
	}

	private static TyTerm makeTerm(List<String> symbolList, Map<Integer, Rank> symbolRankMap, int terminalNum, Map<Integer, TyAbst> abstMap){
		List<String> varList = symbolList.stream().filter(str -> str.charAt(0)=='y').collect(Collectors.toList());
		List<TyAbst> abstList = varList.stream().map(str -> {
				int varId = Integer.parseInt(str.substring(1));
				return new TyAbst(false, varId, null);
			}).collect(Collectors.toList());
		Type abstType = O.O;
		for(int i=abstList.size()-1; i > -1; i--){
			abstType = new Fun(O.O, abstType);
			abstList.get(i).setType(abstType);
		}
		abstList.stream().forEach(abst -> {
			assert(!abstMap.containsKey(abst.getName()));
			abstMap.put(abst.getName(), abst);
			});
		TyTerm decBody = makeTermAux(MyUtil.reversedArrayList(symbolList), symbolRankMap, terminalNum, abstMap);
		TyTerm decTerm = decBody;
		for(int i=abstList.size()-1; i > -1; i--){
			TyAbst abst = abstList.get(i);
			abst.setChild(decTerm);
			decTerm.setParent(abst);
			decTerm = abst;
		}
		abstList.stream().forEach(abst -> abstMap.remove(abst.getName(), abst));
		
		return decTerm;
	}

	private static TyTerm makeTermAux(List<String> revSymbolList, Map<Integer, Rank> symbolRankMap, int terminalNum, Map<Integer, TyAbst> abstMap){
		int varId = Integer.parseInt(revSymbolList.remove(revSymbolList.size()-1).substring(1));
		TyId var = varId < terminalNum
				? new TySymbol(-varId, typeOfRank(symbolRankMap.get(varId).getRank()))
				: abstMap.get(varId).makeVar();
		int rank = rankOfVar(var);
		
		TyTerm term = var;
		for(int i=0; i < rank; i++){
			TyTerm arg = makeTermAux(revSymbolList, symbolRankMap, terminalNum, abstMap);
			TyApp app = new TyApp(term, arg, ((Fun)term.getType()).getRight());
			term.setParent(app); arg.setParent(app);
			term = app;
		}
		
		return term;
	}
	
	private static Type typeOfRank(int rank){
		return rank < 1 ? O.O : new Fun(O.O, typeOfRank(rank-1));
	}
	
	private static int rankOfVar(TyId var){
		Type ty = var.getType();
		if(ty.equals(O.O)) return 0;
		int rank = 0;
		while(ty instanceof Fun){
			rank++;
			ty = ((Fun)ty).getRight();
		}
		return rank;
	}
}
