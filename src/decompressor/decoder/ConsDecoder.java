package decompressor.decoder;


import java.util.ArrayList;
import java.util.List;

import compressor.HOCompressor;
import util.MyUtil;


public final class ConsDecoder {


	public static List<String> decodedConsList(String consFile){
		List<String> consList = HOCompressor.COMP_AS_BINARY_TREE ? makeConsListBin(consFile) : makeConsList(consFile);
		return consList;
	}
	
	private static List<String> makeConsList(String consFile){
		List<Byte> bytes = MyUtil.inputByteList(consFile);
		List<String> consList = new ArrayList<String>();
		List<Byte> byteName = new ArrayList<Byte>();
		for(int i=0; i < bytes.size(); i++){
			if(bytes.get(i)==0){
				byte[] name = new byte[byteName.size()];
				for(int j=0; j < name.length; j++){
					name[j] = byteName.get(j);
				}
				consList.add(new String(name));
				byteName.clear();
			}else{
				byteName.add(bytes.get(i));
			}
		}
		return consList;
	}
	
	private static List<String> makeConsListBin(String consFile){
		List<String> consList00 = makeConsList(consFile+"00");
		List<String> consList01 = makeConsList(consFile+"01");
		List<String> consList10 = makeConsList(consFile+"10");
		List<String> consList11 = makeConsList(consFile+"11");
		
		List<String> consList = new ArrayList<String>();
		consList00.forEach(name -> consList.add("00"+name));
		consList01.forEach(name -> consList.add("01"+name));
		consList10.forEach(name -> consList.add("10"+name));
		consList11.forEach(name -> consList.add("11"+name));
		return consList;
	}

}
