package decompressor.decoder;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.LinkedHashMap;
import java.util.List;

import util.MyUtil;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyId;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Type;
import compressor.HOCompressor;

import decompressor.HODecompressor;

public final class Decoder {
	
	public static void main(String[] args){
		TyRoot root = decode("syuronnBench/test/", "test", true);
		System.out.println("Decoder#main	root:"+root);
	}

	public static TyRoot decode(String dir, String file, boolean typeClassifying){
		String outputDir = dir+file+"_output/";
		String outputDirRaw = outputDir+"raw/";
		String labelFile = outputDirRaw+file+".label";
		String typeFile = outputDirRaw+file+".ty";
		String consFile = outputDirRaw+file+".cons";
		String yrcFile = outputDir+file+".label.yrc";

		if(HODecompressor.CHECK_RANGE_CODER){
			try {
				String script = "python/adapt_range_coder/adapt_range_coder_ycomp.py";
				String decFile = yrcFile + ".dec";
				String cmd = "py" + " " + script + " -d " + yrcFile + " " + decFile;
				if(HOCompressor.PRINT_CMD) System.out.println("LabelCoder#compressLabelFile	cmd:"+cmd);
				Runtime r = Runtime.getRuntime();
				Process p = r.exec(cmd);
				p.waitFor();
				cmd = "diff -s " + labelFile + " " + decFile;
				p = r.exec(cmd);
				if(HOCompressor.PRINT_CMD) System.out.println("LabelCoder#compressLabelFile	cmd:"+cmd);
				p.waitFor();
				MyUtil.printInputStream(p.getInputStream());
			} catch (IOException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			} catch (InterruptedException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}
		}

		return decodedTerm(labelFile, typeFile, consFile, typeClassifying);
	}


	public static TyRoot decodedTerm(String labelFile, String typeFile, String consFile, boolean typeClassifying){



		/*
		 * 圧縮とその時間計測
		 */
		long realStart, end;
		long cpuStart, cpuEnd;
		long userStart, userEnd;
		ThreadMXBean bean = ManagementFactory.getThreadMXBean();

		long millis = 0 * 1000;
		MyUtil.sleepMillis(millis);

		cpuStart = bean.getCurrentThreadCpuTime();
		userStart = bean.getCurrentThreadUserTime();
		realStart = System.currentTimeMillis();


		List<Integer> labelList = LabelDecoder.decodedLabelList(labelFile);
		List<String> consList = ConsDecoder.decodedConsList(consFile);
		List<Type> typeList = HOCompressor.COMP_AS_BINARY_TREE ? TypeDecoder.decodedTypeListBin(typeFile, consList) : TypeDecoder.decodedTypeList(typeFile);
		Type type = typeList.remove(0);


		LinkedHashMap<Integer, Type> Kd = typeClassifying ? TypeClassifyingDec.makeKd(typeList, consList) : UniformDec.makeKd(typeList, consList);
		TyRoot root = TyRoot.getRoot(null);
		List<TyId> Gamma = typeClassifying ? TypeClassifyingDec.makeGamma(typeList, consList, root) : UniformDec.makeGamma(typeList, consList, root);
		List<Integer> s = MyUtil.reversedArrayList(labelList);

		TyTerm term = typeClassifying ? TypeClassifyingDec.Dec(Kd, Gamma, s, type, root) : UniformDec.Dec(Kd, Gamma, s, type, root);
		if(root.getAbstsDecoded()!=null){ 
			//ここでλ式中に現れない変数を削除
			for(TyAbst abst : root.getAbstsDecoded()){
				TyVar varToRemove = null;
				for(TyVar var : abst.getVars()){
					if(var.getParent()==null){
						varToRemove = var;
						break;
					}
				}
				abst.getVars().remove(varToRemove);
			}
		}
		root.setChild(term); term.setParent(root);


		cpuEnd = bean.getCurrentThreadCpuTime();
		userEnd = bean.getCurrentThreadUserTime();
		end = System.currentTimeMillis();

		HODecompressor.printLog("DEC_ELAPSED_TIME(REAL):	"+(end-realStart)/(double)1000);
		HODecompressor.printLog("DEC_ELAPSED_TIME(CPU):	"+(cpuEnd-cpuStart)/((double)1000*1000*1000));
		HODecompressor.printLog("DEC_ELAPSED_TIME(USER):	"+(userEnd-userStart)/((double)1000*1000*1000));

		return root;
	}


}
