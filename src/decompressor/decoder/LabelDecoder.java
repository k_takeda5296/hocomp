package decompressor.decoder;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import util.MyUtil;
import coder.rangecoder.DecompressedInputStream;
import coder.variablecoder.VariableByteCode;
import decompressor.HODecompressor;


public final class LabelDecoder {

	public static List<Integer> decodedLabelList(String labelFile){
		List<Byte> byteList = MyUtil.inputByteList(labelFile);
		byte[] byteArray = new byte[byteList.size()];
		for(int i=0; i < byteArray.length; i++){
				byteArray[i] = byteList.get(i);
		}
		List<Integer> decodedList = HODecompressor.USE_VARIABLE_BYTE ? VariableByteCode.decode(byteArray) : decodeRawLabelBytes(byteArray);
		return decodedList;
	}
	public static List<Integer> decodedYrcList(String yrcFile){
		String tmpFile = yrcFile + ".tmp";
		decompressLabelFile(yrcFile, tmpFile);
		return decodedLabelList(tmpFile);
	}
	
	private static List<Integer> decodeRawLabelBytes(byte[] byteArray){
		int bytePerInt = byteArray[0];
		if(bytePerInt < 1 || bytePerInt > 4) throw new IllegalArgumentException("invalid count byte");
		if((byteArray.length-1)%bytePerInt != 0) throw new IllegalArgumentException("invalid bytes length");
	
		List<Integer> labelList = new ArrayList<Integer>();
		for(int i=1; i < byteArray.length; i+=bytePerInt){
			byte[] bytes = new byte[bytePerInt];
			for(int j=0; j < bytePerInt; j++){
				bytes[j] = byteArray[i+j];
			}
			labelList.add(byteArray2Int(bytes));
		}
		return labelList;
	}
	
	private static int byteArray2Int(byte[] bytes){
		byte[] integer = new byte[4];
		System.arraycopy(bytes, 0, integer, 4-bytes.length, bytes.length);
		return ByteBuffer.wrap(integer).getInt();
	}
	
	private static void decompressLabelFile(String inputFile, String outputFile){
		try {
			InputStream in = new DecompressedInputStream(new BufferedInputStream(new FileInputStream(inputFile)));
			OutputStream out = new BufferedOutputStream(new FileOutputStream(outputFile));

			while(true){
				int ch = in.read();
				if (ch == -1) {
					in.close();
					out.close();
					return;
				}
				out.write(ch);
			}
			
		} catch(IOException e) {
			e.printStackTrace();
		} catch(Throwable e) {
			e.printStackTrace();
		}
	}

}
