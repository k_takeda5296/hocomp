package decompressor.decoder;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyId;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Fun;
import component.tyterm.type.Type;

class UniformDec{
	
	/*
	 * Kdはインデックスからそれに対応するラムダ抽象の型（ラムダ抽象の返り値の型までを含む）へのマップ
	 */
	static LinkedHashMap<Integer, Type> makeKd(List<Type> typeList, List<String> consList){
		int abstNum = typeList.size() - consList.size();
		LinkedHashMap<Integer, Type> Kd = new LinkedHashMap<Integer, Type>();
		for(int i=0; i < abstNum; i++){
			Kd.put(i, typeList.get(i));
		}
		return Kd;
	}

	/*
	 * Gammaは型環境（追加・削除の容易さから向きを逆としている）
	 */
	static List<TyId> makeGamma(List<Type> typeList, List<String> consList, TyRoot root){
		int abstNum = typeList.size() - consList.size();
		List<TyId> Gamma = new ArrayList<TyId>();
		for(int i=abstNum; i < typeList.size(); i++){
			Type type = typeList.get(i);
			int name = root.addSymbolType(consList.get(i-abstNum), type);
			TySymbol symbol = new TySymbol(name, type);
			Gamma.add(symbol);
		}
		return Gamma;
	}


	/*
	 * Gの向きを逆にする（追加が後ろで良いようにする）
	 */
	static TyTerm Dec(LinkedHashMap<Integer, Type> Kd, List<TyId> G, List<Integer> s, Type t, TyRoot root){
		int s1 = s.remove(s.size()-1);

		if(kType(Kd, s1, t)!=null){//kdSize(Kd,t)を用いた書き方でもよい（そのほうがよい？）
			Type kType = kType(Kd, s1, t);
			if(kType.equals(t)){
				TyAbst lambdaX = new TyAbst(false, kType); root.addAbstDecoded(lambdaX);
				TyVar x = lambdaX.makeVar();
				G.add(x);
				Type tRight = ((Fun)t).getRight();
				TyTerm e0 = Dec(Kd, G, s, tRight, root);
				G.remove(G.size()-1);
				lambdaX.setChild(e0);e0.setParent(lambdaX);

				return lambdaX;
			}else{
				TyAbst lambdaF = new TyAbst(true, kType); root.addAbstDecoded(lambdaF);
				TyVar f = lambdaF.makeVar();
				G.add(f);
				Type kTypeRight = ((Fun)kType).getRight();
				TyTerm e0 = Dec(Kd, G, s, kTypeRight, root);
				G.remove(G.size()-1);
				lambdaF.setChild(e0);e0.setParent(lambdaF);

				return constructApps(lambdaF, Kd, G, s, t, root);
			}
		}else{
			int kdSize = kdSize(Kd, t);
			TyId id = (TyId) possibilityId(G, s1-kdSize, t).copy();//変数のコピーを生成。これにより各λ抽象は丁度一つだけλ式中に現れない変数を束縛することになる
			if(id.getType().equals(t)) return id;
			return constructApps(id, Kd, G, s, t, root);
		}
	}

	/*
	 * typeを返り値に取りうる型を持つラムダ抽象を数えて、
	 * s1がその個数以下であれば対応するλ抽象の型を返す。
	 * その個数より多ければnullを返す。
	 */
	static boolean FLAG = false;
	private static Type kType(LinkedHashMap<Integer, Type> Kd, int s1, Type type){
		if(FLAG){
			System.out.println("kType	s1:"+s1);
			System.out.println("kType	s1:"+s1+"	type:"+type);
			System.out.println("kType	s1:"+s1+"	type:"+type+"	Kd:"+Kd);
		}
		int index = -1;
		for(int i=0; i < Kd.size(); i++){
			Type t = Kd.get(i);
			if(FLAG){
				System.out.println("kType	s1:"+s1+"	type:"+type+"	Kd:"+Kd+"	index:"+index);
				System.out.println("kType	s1:"+s1+"	type:"+type+"	Kd:"+Kd+"	index:"+index+"	t:"+t);
			}
			if(true) index++;
			if(index==s1) return t;
		}
		return null;//汚い
	}

	//型typeを返しうるラムダ抽象の型の数
	private static int kdSize(LinkedHashMap<Integer, Type> Kd, Type type){
		int size = 0;
		for(Integer i : Kd.keySet()){
			if(true) size++;
		}
		return size;
	}

	//Gammaに登録されたIdのうち、型typeを返しうる型を持つIdを数えてs番目を返す。
	private static TyId possibilityId(List<TyId> G, int s, Type type){
		int index = -1;
		for(TyId id : G){
			if(true) index++;
			if(index==s) return id;
		}
		throw new IllegalArgumentException("no match id!");
	}

	private static TyApp constructApps(TyTerm term, LinkedHashMap<Integer, Type> Kd, List<TyId> G, List<Integer> s, Type t, TyRoot root){
		List<TyTerm> argList = makeArgList(Kd, G, s, term.getType(), t, root);
		TyTerm arg1 = argList.remove(0);//引数が必ず1つは存在//リストを逆順にできないか？
		TyApp app = new TyApp(term, arg1, ((Fun)term.getType()).getRight());
		term.setParent(app); arg1.setParent(app);
		for(TyTerm arg : argList){
			Type type = ((Fun)app.getType()).getRight();
			app = new TyApp(app, arg, type);
			arg.setParent(app);
		}
		return app;
	}

	private static List<TyTerm> makeArgList(LinkedHashMap<Integer, Type> Kd, List<TyId> G, List<Integer> s, Type kType, Type type, TyRoot root){
		List<TyTerm> argList = new ArrayList<TyTerm>();
		List<Type> typeList = kType.toArgTypeList(type);
		for(Type t : typeList){
			TyTerm term = Dec(Kd, G, s, t, root);
			argList.add(term);
		}
		return argList;
	}
}