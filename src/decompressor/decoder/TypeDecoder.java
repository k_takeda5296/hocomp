package decompressor.decoder;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import util.MyUtil;

import component.tyterm.type.Fun;
import component.tyterm.type.O;
import component.tyterm.type.Type;
import component.tyterm.type.TypeBitConverter;
import compressor.HOCompressor;


public final class TypeDecoder {

	public static List<Type> decodedTypeList(String typeFile){
		List<Boolean> bitList = MyUtil.byteList2BooleanList(MyUtil.inputByteList(typeFile));
		List<Type> typeList = new ArrayList<Type>();
		while(!bitList.isEmpty()){//処理順を反転させた方が速いがあまり変わらなそう
			List<Boolean> bits = trimTypeBits(bitList);
			if(bits.isEmpty()) break;
			Type type = TypeBitConverter.bits2Type(bits);
			typeList.add(type);
		}
		//bufferに残っていても特に何もせず読み捨てる
		return typeList;
	}
	
	public static List<Type> decodedTypeListBin(String typeFile, List<String> consList){
		List<Boolean> bitList = MyUtil.byteList2BooleanList(MyUtil.inputByteList(typeFile));
		List<Type> typeList = new ArrayList<>();
		while(!bitList.isEmpty()){//処理順を反転させた方が速いがあまり変わらなそう
			List<Boolean> bits = trimTypeBits(bitList);
			if(bits.isEmpty()) break;
			Type type = TypeBitConverter.bits2Type(bits);
			typeList.add(type);
		}
		addConsTypesTo(typeList, consList);
		//bufferに残っていても特に何もせず読み捨てる
		return typeList;
	}
	
	private static void addConsTypesTo(List<Type> typeList, List<String> consList){
		consList.stream().forEach(symbolName -> {
					String childNum = symbolName.substring(0, 2);
					if(childNum.equals("00")){
						typeList.add(O.O);
					}else if(childNum.equals("11")){
						typeList.add(new Fun(O.O, new Fun(O.O, O.O)));
					}else{
						typeList.add(new Fun(O.O, O.O));
					}
				});
	}
	
	private static List<Boolean> trimTypeBits(List<Boolean> bitList){
		List<Boolean> bits = new ArrayList<>();
		int nodes = 1;
		do{
			boolean b = bitList.remove(0);
			bits.add(b);
			if(b) nodes++;
			else nodes--;
		}while(nodes > 0 && !bitList.isEmpty());
		
		if(nodes > 0) return new ArrayList<>();
		return bits;
	}
}
