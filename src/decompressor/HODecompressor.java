package decompressor;



import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import util.CommandLineParser;
import util.MyLogger;
import util.MyUtil;
import component.cheapterm.CheapTerm;
import component.tyterm.term.TyRoot;
import compressor.HOCompressor;
import decompressor.decoder.Decoder;
import decompressor.eval.RecursiveReducer;
import decompressor.saver.Saver;
import decompressor.saver.XmlSaver;




public final class HODecompressor {


	//プロファイリングしたいときに待つ秒数（5秒くらいあると良い）
	private static final int WAIT_MILLIS = 0 * 1000;

	//ファイルにログを出力するか
	public static final boolean LOGGING_INTO_FILE = true;
	public static MyLogger LOGGER;

	//圧縮前のλ式を出力するかどうか
	public static boolean PRINT_ORIGINAL_TERM = false;

	//圧縮後のλ式を出力するかどうか
	public static boolean PRINT_COMPRESSED_TERM = false;
	//上記の出力時に高階なパターンのみを表示
	public static final boolean PRINT_ONLY_HO_PATTERN = true;
	//上記の出力時に同値穴を持つパターンのみを表示
	public static final boolean PRINT_ONLY_EH_PATTERN = false;

	public static final boolean PRINT_ABST_TYPES = false;



	//解凍結果をファイルに書き出すか
	public static final boolean OUTPUT_DECOMP = false;

	//圧縮ファイルから読み出した値が書き出した値と一致するかチェック
	public static final boolean DECODE_FILE_CHECK = false;

	//圧縮後のTyTermと、それをエンコードしたファイルから読み出してデコードしたTyTermが等しいかチェック
	public static final boolean DECODE_CHECK = true;

	//デコードしたtermを簡約したものが元ファイルのtermと等しいかチェック
	public static final boolean REDUCE_CHECK = false;

	//ポインタが双方向になっているかをチェック
	public static final boolean BIDIRECTIONAL_CHECK = false;

	//ポインタが双方向になっているか、束縛が外れていないか、正しく型付けされているかチェック
	public static final boolean TERM_VARIDITY_CHECK = false;

	public static final boolean CHECK_MAX_MEMORY = true;
	public static final int CHECKING_TERM = 500;

	public static final boolean CHECK_RANGE_CODER = false;

	public static final boolean USE_VARIABLE_BYTE = false;

	private static final boolean TYPE_CLASSIFYING_DECODE = false;//Remark 3.2

	public static final String OPT_FILE = "-f";
	public static final String OPT_DNA_TEXT = "-dna_text";
	public static final String OPT_EN_TEXT = "-en_text";
	public static final String OPT_PROG_TEXT = "-prog_text";
	public static final String OPT_CSIZE = "-c";
	public static final String OPT_LOG = "-log";

	private static final String USAGE = "\n" +
			OPT_FILE+"	圧縮するファイル（英文テキストの時はディレクトリ）のパス\n" +
			OPT_DNA_TEXT+"	対象がDNAテキストの時に長さを指定（値は長さ）\n" + 
			OPT_EN_TEXT+"	対象が英文テキストの時に指定（値は最大ファイル数）\n" + 
			OPT_PROG_TEXT+"	対象がプログラムのソースコード（Python除く）の時に指定（値なし）\n" + 
			OPT_CSIZE+"	文脈の最大ノード数\n" +
			OPT_LOG+"	ログファイル名を指定する\n";

	public static void main(String[] args){
		//System.out.println("HODecompressor#main	args:"+Arrays.toString(args));
		
		CommandLineParser clp = new CommandLineParser()
		.addOption(OPT_FILE, true)
		.addOption(OPT_DNA_TEXT)
		.addOption(OPT_EN_TEXT)
		.addOption(OPT_PROG_TEXT)
		.addOption(OPT_CSIZE, true)
		.addOption(OPT_LOG);
		if(!clp.parse(args)) throw new IllegalArgumentException("invalid options!" + System.getProperty("line.seperator") + USAGE);
		File path = new File(HOCompressor.checkOption(clp));
		String dir = path.getParent() + "/";
		String file = path.getName();

		MyUtil.sleepMillis(WAIT_MILLIS);
		HOCompressor.init();
		String logFileName = clp.hasOptionValue(OPT_LOG) ? clp.getOptionValue(OPT_LOG) : (new SimpleDateFormat("yyyyMMdd-HHmmss")).format(Calendar.getInstance().getTime())+"_"+file+"_"+clp.getOptionValue(OPT_CSIZE)+".dec.log";

		try {
			LOGGER = new MyLogger(logFileName);
			HODecompressor main = new HODecompressor();
			main.execute(dir, file);
		} catch(NumberFormatException e){
			System.err.println(OPT_CSIZE+" should take positive integer!");
			e.printStackTrace();
		}finally{
			if(LOGGING_INTO_FILE) LOGGER.close();
		}
	}


	public static void printLog(Object obj){
		if(LOGGER==null){
			System.out.println("LOGGER is null!");
			return;
		}
		if(LOGGING_INTO_FILE) LOGGER.println(obj);
		System.out.println(obj);
	}
	
	public static void printLog(){
		printLog("");
	}

	private void execute(String dir, String file){

		TyRoot root = Decoder.decode(dir, file, TYPE_CLASSIFYING_DECODE);
		CheapTerm reducedTerm = RecursiveReducer.reducedTerm(root);
		Saver saver = new XmlSaver(dir+file+".dec");
		saver.saveTerm(reducedTerm);
		printLog("");
	}

}
