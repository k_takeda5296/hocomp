package decompressor.saver;

import component.cheapterm.CheapTerm;
import compressor.loader.Loader;

public interface Saver {
	public static final String END_TAG = Loader.END_TAG;
	
	void saveTerm(CheapTerm term);//ベータ正規形を受け取ってファイルに出力する
	String getFile();
}
