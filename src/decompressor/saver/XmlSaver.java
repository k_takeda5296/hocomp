package decompressor.saver;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import component.cheapterm.CheapSymbol;
import component.cheapterm.CheapTerm;
import component.tree.SymbolTree;
import compressor.HOCompressor;
import compressor.loader.BinaryTreeConverter;



public class XmlSaver implements Saver{

	private final String file_;
	
	public XmlSaver(String file){
		file_ = file;
	}

	public void saveTerm(CheapTerm term) {
		SymbolTree tree = term2Tree(term);
		if(HOCompressor.COMP_AS_BINARY_TREE) tree = BinaryTreeConverter.binaryTree2Tree(tree);
		saveTreeAsXml(tree, file_);
	}
	
	public String getFile(){
		return file_;
	}
	
	private SymbolTree term2Tree(CheapTerm term){
		List<CheapTerm> terms = new ArrayList<CheapTerm>();
		term.splitTerm(terms);
		CheapTerm node = terms.get(0);
		if(!(node instanceof CheapSymbol)) throw new IllegalArgumentException("this is not beta-normal or something wrong!	"+term);
		
		CheapSymbol symbol = (CheapSymbol)node;
		SymbolTree tree = new SymbolTree(symbol.getName());
		for(int i=1; i < terms.size(); i++){
			CheapTerm arg = terms.get(i);
			if(arg.getName().equals(END_TAG)) continue;
			SymbolTree child = term2Tree(arg);
			tree.addChild(child);
		}
		return tree;
	}
	
	private void saveTreeAsXml(SymbolTree tree, String file){
		PrintWriter out;
		try{
			out = new PrintWriter(new BufferedWriter(new FileWriter(file)));
			outputTreeAsXml(tree, out);
			out.flush();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void outputTreeAsXml(SymbolTree node, PrintWriter pw) throws IOException{
		String startElem = "<" + node.getSymbolName() + ">\n";
		pw.write(startElem);
		for(SymbolTree child : node.getChildren()){
			outputTreeAsXml(child, pw);
		}
		String endElem = "</" + node.getSymbolName() + ">\n";
		pw.write(endElem);
	}
	
	public static void outputByteArray(byte[] bytes, String fname) {
		FileOutputStream output;
		try {
			output = new FileOutputStream(fname);
			output.write(bytes);
			output.flush();
			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}