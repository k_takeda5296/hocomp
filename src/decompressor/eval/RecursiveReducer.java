package decompressor.eval;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadMXBean;
import java.util.HashMap;

import util.MyUtil;

import component.cheapterm.CheapTerm;
import component.tyterm.term.TyRoot;

import decompressor.HODecompressor;

public class RecursiveReducer {

	public static CheapTerm reducedTerm(TyRoot root){
		/*
		 * 圧縮とその時間計測
		 */
		long realStart, end;
		long cpuStart, cpuEnd;
		long userStart, userEnd;
		ThreadMXBean bean = ManagementFactory.getThreadMXBean();

		long millis = 0 * 1000;
		MyUtil.sleepMillis(millis);

		cpuStart = bean.getCurrentThreadCpuTime();
		userStart = bean.getCurrentThreadUserTime();
		realStart = System.currentTimeMillis();

		CheapTerm rTerm = Eval.eval(root.getChild().cheapCopy(root.getSymbolNames()), new HashMap<String, CheapTerm>());

		cpuEnd = bean.getCurrentThreadCpuTime();
		userEnd = bean.getCurrentThreadUserTime();
		end = System.currentTimeMillis();

		HODecompressor.printLog("RED_ELAPSED_TIME(REAL):	"+(end-realStart)/(double)1000);
		HODecompressor.printLog("RED_ELAPSED_TIME(CPU):	"+(cpuEnd-cpuStart)/((double)1000*1000*1000));
		HODecompressor.printLog("RED_ELAPSED_TIME(USER):	"+(userEnd-userStart)/((double)1000*1000*1000));

		return rTerm;
	}
}
