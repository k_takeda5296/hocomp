package decompressor.eval;



import java.util.HashMap;
import java.util.Map;

import component.cheapterm.CheapAbst;
import component.cheapterm.CheapApp;
import component.cheapterm.CheapSymbol;
import component.cheapterm.CheapTerm;
import component.cheapterm.CheapVar;




public final class EvalCBN {

	public static int EVAL_SYMBOL = 0;
	public static int EVAL_APP = 0;
	public static int EVAL_ABST = 0;
	public static int EVAL_VAR = 0;
	
	private static final boolean FLAG = false;
	public static CheapTerm eval(CheapTerm term, Map<String, CheapTerm> env, Map<CheapAbst, Map<String, CheapTerm>> funEnv){
		if(FLAG) System.out.println("Eval#eval	class:"+term.getClass());
		if(FLAG) System.out.println("Eval#eval	class:"+term.getClass()+"	term:"+term);
		if(term instanceof CheapSymbol){
			EVAL_SYMBOL++;
			CheapSymbol symbol = (CheapSymbol)term;
			return symbol/*.copy()*/;
		}else if(term instanceof CheapVar){//ここを通るのはappの
			EVAL_VAR++;
			CheapVar var = (CheapVar)term;
			return env.containsKey(var.getName()) ? env.get(var.getName()) : var;//envに含まれないときはfunクロージャの中身なのでそのまま
		}else if(term instanceof CheapApp){
			EVAL_APP++;
			CheapApp app = (CheapApp)term;
			return evalApp(app.getLeftChild(), app.getRightChild(), env, funEnv);
		}else if(term instanceof CheapAbst){//ここを通るのはappの右側のみ。すなわちfunクロージャとして評価
			EVAL_ABST++;
			CheapAbst abst = (CheapAbst)term;
			CheapTerm child = eval(abst.getChild(), env, new HashMap<CheapAbst, Map<String, CheapTerm>>(funEnv));
			funEnv.put(abst, new HashMap<String, CheapTerm>(env));//クロージャを追加.これによりappが更新されたfunenvを使える
			return new CheapAbst(abst.getName(), child);
		}else{
			throw new IllegalArgumentException("no match class!");
		}
	}
	
	public static int EVALAPP = 0;
	private static CheapTerm evalApp(CheapTerm left, CheapTerm right, Map<String, CheapTerm> env, Map<CheapAbst, Map<String, CheapTerm>> funEnv){
		EVALAPP++;
		if(FLAG) System.out.println("Eval#evalApp	left:"+left);
		if(FLAG) System.out.println("Eval#evalApp	left:"+left+"	right:"+right);

		if(left instanceof CheapAbst){//leftがabstのときはlet式として評価する
			CheapAbst abst = (CheapAbst)left;
			Map<String, CheapTerm> newEnv = new HashMap<String, CheapTerm>(env);
			Map<CheapAbst, Map<String, CheapTerm>> newFunEnv = new HashMap<CheapAbst, Map<String, CheapTerm>>(funEnv);//rightの先頭がabstなら追加される
			CheapTerm arg = eval(right, env, newFunEnv);
			if(FLAG) System.out.println("Eval#evalApp	arg:"+arg);
			newEnv.put(abst.getName(), arg);//newFunEnvが更新される
			return eval(abst.getChild(), newEnv, newFunEnv);
		}else if(left instanceof CheapVar){
			CheapVar var = (CheapVar)left;
			if(env.containsKey(var.getName())){
				CheapTerm entity = env.get(var.getName());
				return evalApp(entity, right, env, funEnv);
			}else{
				return new CheapApp(left, eval(right, env, funEnv));
			}
		}else{//そうでなければ値として構成
			CheapTerm evaledLeft = eval(left, env, funEnv);
			if(evaledLeft instanceof CheapAbst){
				return evalApp(evaledLeft, right, env, funEnv);
			}
			return new CheapApp(evaledLeft, eval(right, env, funEnv));
		}
	}
	
}
