package decompressor.eval;



import java.util.HashMap;
import java.util.Map;

import component.cheapterm.CheapAbst;
import component.cheapterm.CheapApp;
import component.cheapterm.CheapSymbol;
import component.cheapterm.CheapTerm;
import component.cheapterm.CheapVar;




public final class OldEval {

	public static CheapTerm eval(CheapTerm term, Map<String, CheapTerm> env, Map<CheapAbst, Map<String, CheapTerm>> funEnv){
		if(term instanceof CheapSymbol){
			CheapSymbol symbol = (CheapSymbol)term;
			return symbol/*.copy()*/;
		}else if(term instanceof CheapVar){
			CheapVar var = (CheapVar)term;
			return env.containsKey(var.getName()) ? env.get(var.getName())/*.copy()*/ : var/*.copy()*/;//envに含まれないときはfunクロージャの中身なのでそのまま
		}else if(term instanceof CheapApp){
			CheapApp app = (CheapApp)term;
			CheapTerm left = eval(app.getLeftChild(), env, funEnv);
			Map<CheapAbst, Map<String, CheapTerm>> newFunEnv = new HashMap<CheapAbst, Map<String, CheapTerm>>(funEnv);//rightの先頭がabstならエントリが追加される
			CheapTerm right = eval(app.getRightChild(), env, newFunEnv);
			if(left instanceof CheapAbst){//leftがabstのときはlet式として評価する
				CheapAbst abst = (CheapAbst)left;
				Map<String, CheapTerm> newEnv = new HashMap<String, CheapTerm>(env);
				newEnv.put(abst.getName(), right);
				return eval(abst.getChild(), newEnv, newFunEnv);
			}else{//そうでなければ値として構成
				return new CheapApp(left, right);
			}
		}else if(term instanceof CheapAbst){//ここを通るのはappの右側のみ。すなわちfunクロージャとして評価
			CheapAbst abst = (CheapAbst)term;
			CheapTerm child = eval(abst.getChild(), env, funEnv);//さらにfunクロージャが続くことがあるためfunEnvはそのまま渡す
			funEnv.put(abst, new HashMap<String, CheapTerm>(env));//クロージャを追加.すなわちnewしたenvをエントリに追加.これにより親が更新されたfunenvを使える
			return new CheapAbst(abst.getName(), child);
		}else{
			throw new IllegalArgumentException("no match class!");
		}
	}

}
