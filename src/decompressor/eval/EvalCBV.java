package decompressor.eval;



import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import component.cheapterm.CheapAbst;
import component.cheapterm.CheapApp;
import component.cheapterm.CheapSymbol;
import component.cheapterm.CheapTerm;
import component.cheapterm.CheapVar;




public final class EvalCBV {

	public static int EVAL_SYMBOL = 0;
	public static int EVAL_APP = 0;
	public static int EVAL_ABST = 0;
	public static int EVAL_VAR = 0;
	
	private static final boolean FLAG = false;
	public static CheapTerm eval(CheapTerm term, Map<String, CheapTerm> env, Map<CheapAbst, Map<String, CheapTerm>> funEnv){
		if(FLAG) System.out.println("Eval#eval	class:"+term.getClass());
		if(FLAG) System.out.println("Eval#eval	class:"+term.getClass()+"	term:"+term);
		if(FLAG) System.out.println("Eval#eval	class:"+term.getClass()+"	term:"+term+"	env:"+env);
		if(term instanceof CheapSymbol){
			EVAL_SYMBOL++;
			CheapSymbol symbol = (CheapSymbol)term;
			return symbol;
		}else if(term instanceof CheapVar){//ここを通るのはappの
			EVAL_VAR++;
			CheapVar var = (CheapVar)term;
			if(!env.containsKey(var.getName())) return var;//throw new IllegalArgumentException("no entity for the var of body!");
			return env.get(var.getName());//値呼びなのでそのまま
		}else if(term instanceof CheapApp){
			EVAL_APP++;
			CheapApp app = (CheapApp)term;
			return evalApp(app.getLeftChild(), app.getRightChild(), env, funEnv);
		}else if(term instanceof CheapAbst){//ここを通るのはappの右側のみ。すなわちfunクロージャとして評価
			EVAL_ABST++;
			throw new IllegalArgumentException("abst cannot be body!");
			/*
			CheapAbst abst = (CheapAbst)term;
			CheapTerm child = evalBody(abst.getChild(), env, new HashMap<CheapAbst, Map<String, CheapTerm>>(funEnv));
			funEnv.put(abst, new HashMap<String, CheapTerm>(env));//クロージャを追加.これによりappが更新されたfunenvを使える
			return new CheapAbst(abst.getName(), child);
			*/
		}else{
			throw new IllegalArgumentException("no match class!");
		}
	}
	
	public static int EVALAPP = 0;
	private static CheapTerm evalApp(CheapTerm left, CheapTerm right, Map<String, CheapTerm> env, Map<CheapAbst, Map<String, CheapTerm>> funEnv){
		EVALAPP++;
		if(FLAG) System.out.println("Eval#evalApp	left:"+left);
		if(FLAG) System.out.println("Eval#evalApp	left:"+left+"	right:"+right);
		Map<String, CheapTerm> newEnv = new HashMap<String, CheapTerm>(env);
		Map<CheapAbst, Map<String, CheapTerm>> newFunEnv = new HashMap<CheapAbst, Map<String, CheapTerm>>(funEnv);
		AtomicBoolean isReduced = new AtomicBoolean(false);
		CheapTerm body = evalLeft(left, right, newEnv, newFunEnv, isReduced);//各環境が更新され、rightが環境に追加されたか(isReduced)もチェックされる
		if(FLAG) System.out.println("Eval#evalApp	left:"+left+"	right:"+right+"	body:"+body);
		if(FLAG) System.out.println("Eval#evalApp	left:"+left+"	right:"+right+"	body:"+body+"	env:"+newEnv);
		if(FLAG) System.out.println("Eval#evalApp	left:"+left+"	right:"+right+"	body:"+body+"	env:"+newEnv+"	isReduced:"+isReduced);
		return !isReduced.get() ? new CheapApp(eval(left, env, funEnv), eval(right, env, funEnv)) : eval(body, newEnv, newFunEnv);
	}
	
	//左がbodyになるまで環境を更新しながらbodyを取得
	private static CheapTerm evalLeft(CheapTerm left, CheapTerm right, Map<String,CheapTerm> env, Map<CheapAbst, Map<String, CheapTerm>> funEnv, AtomicBoolean isReduced){
		if(left instanceof CheapAbst){
			isReduced.set(true);
			if(FLAG) System.out.println("Eval#evalLeft	abst:	"+left+"	right:"+right);
			if(FLAG) System.out.println("Eval#evalLeft	abst:	"+left+"	right:"+right+"	env:"+env);
			CheapAbst abst = (CheapAbst)left;
			updateFunEnv(right, env, funEnv);
			env.put(abst.getName(), evalArg(right, env, funEnv));//値呼び
			return abst.getChild();//varでその先がabstのときがある
		}else if(left instanceof CheapApp){
			if(FLAG) System.out.println("Eval#evalLeft	app:	"+left+"	right:"+right);
			if(FLAG) System.out.println("Eval#evalLeft	app:	"+left+"	right:"+right+"	env:"+env);
			CheapApp app = (CheapApp)left;
			CheapTerm body = evalLeft(app.getLeftChild(), app.getRightChild(), env, funEnv, isReduced);
			if(FLAG) System.out.println("Eval#evalLeft	app_:	"+left+"	right:"+right+"	env:"+env+"	body:"+body);
			return evalLeft(body, right, env, funEnv, isReduced);//bodyがabstの可能性があるため掛け直す
		}else if(left instanceof CheapVar){
			if(FLAG) System.out.println("Eval#evalLeft	var:	"+left+"	right:"+right);
			if(FLAG) System.out.println("Eval#evalLeft	var:	"+left+"	right:"+right+"	env:"+env);
			CheapVar var = (CheapVar)left;
			if(!env.containsKey(var.getName())) return var;//throw new IllegalArgumentException("no entity for the var of body!");
			CheapTerm entity = env.get(var.getName());
			if(entity instanceof CheapAbst){
				
			}
			return evalLeft(entity, right, env, funEnv, isReduced);//bodyがabstの可能性があるため掛け直す
		}else{//symbolのとき
			if(FLAG) System.out.println("Eval#evalLeft	symbol:	"+left+"	right:"+right);
			if(FLAG) System.out.println("Eval#evalLeft	symbol:	"+left+"	right:"+right+"	env:"+env);
			return left;
		}
	}
	
	private static CheapTerm evalArg(CheapTerm arg, Map<String, CheapTerm> env, Map<CheapAbst, Map<String, CheapTerm>> funEnv){
		if(arg instanceof CheapAbst){
			CheapAbst abst = (CheapAbst)arg;
			return new CheapAbst(abst.getName(), evalArg(abst.getChild(), env, funEnv));
		}else{
			return eval(arg, env, funEnv);
		}
	}
	
	private static void updateFunEnv(CheapTerm arg, Map<String, CheapTerm> env, Map<CheapAbst, Map<String, CheapTerm>> funEnv){
		if(arg instanceof CheapAbst){
			CheapAbst abst = (CheapAbst)arg;
			funEnv.put(abst, env);
			updateFunEnv(abst.getChild(), env, funEnv);
		}
	}
}
