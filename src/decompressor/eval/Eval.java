package decompressor.eval;



import java.util.Map;

import component.cheapterm.CheapAbst;
import component.cheapterm.CheapApp;
import component.cheapterm.CheapFun;
import component.cheapterm.CheapSymbol;
import component.cheapterm.CheapTerm;
import component.cheapterm.CheapVar;




public final class Eval {

	public static int EVAL_SYMBOL = 0;
	public static int EVAL_APP = 0;
	public static int EVAL_ABST = 0;
	public static int EVAL_VAR = 0;
	
	private static final boolean FLAG = false;
	public static CheapTerm eval(CheapTerm term, Map<String, CheapTerm> env){
		if(FLAG) System.out.println("Eval#eval	class:"+term.getClass());
		if(FLAG) System.out.println("Eval#eval	class:"+term.getClass()+"	term:"+term);
		if(FLAG) System.out.println("Eval#eval	class:"+term.getClass()+"	term:"+term+"	env:"+env);
		if(term instanceof CheapSymbol){
			EVAL_SYMBOL++;
			CheapSymbol symbol = (CheapSymbol)term;
			return symbol;
		}else if(term instanceof CheapVar){//ここを通るのはappの
			EVAL_VAR++;
			CheapVar var = (CheapVar)term;
			if(!env.containsKey(var.getName())) return var;//throw new IllegalArgumentException("no entity for the var of body!");
			return env.get(var.getName());//値呼びなのでそのまま
		}else if(term instanceof CheapApp){
			EVAL_APP++;
			CheapApp app = (CheapApp)term;
			return evalApp(app, env);
		}else if(term instanceof CheapAbst){//（先頭だけ）funクロージャとして評価
			EVAL_ABST++;
			CheapAbst abst = (CheapAbst)term;
			CheapFun fun = new CheapFun(abst.getName(), abst.getChild(), env);
			return fun;
		}else{
			throw new IllegalArgumentException("no match class!");
		}
	}
	
	public static int EVALAPP = 0;
	private static CheapTerm evalApp(CheapApp app, Map<String, CheapTerm> env){
		CheapTerm left = app.getLeftChild();
		CheapTerm right = app.getRightChild();
		EVALAPP++;
		if(FLAG) System.out.println("Eval#evalApp	left:"+left);
		if(FLAG) System.out.println("Eval#evalApp	left:"+left+"	right:"+right);
		CheapTerm body = eval(left, env);
		if(FLAG) System.out.println("Eval#evalApp	left:"+left+"	right:"+right+"	body:"+body);
		if(FLAG) System.out.println("Eval#evalApp	left:"+left+"	right:"+right+"	body:"+body+"	env:"+env);

		if(body instanceof CheapFun){
			if(FLAG) System.out.println("Eval#evalApp#FUN	left:"+left+"	right:"+right+"	body:"+body+"	env:"+env);
			CheapFun fun = (CheapFun)body;
			Map<String, CheapTerm> newEnv = fun.getEnv();
			CheapTerm oldValue = newEnv.put(fun.getArg(), eval(right, env));
			CheapTerm evaledBody = eval(fun.getBody(), newEnv);
			if(oldValue==null)//環境を元に戻す
				newEnv.remove(fun.getArg());
			else
				newEnv.put(fun.getArg(), oldValue);
			return evaledBody;
		}else if(body instanceof CheapAbst){
			if(FLAG) System.out.println("Eval#evalApp#ABST	left:"+left+"	right:"+right+"	body:"+body+"	env:"+env);
			CheapAbst abst = (CheapAbst)body;
			CheapTerm oldValue = env.put(abst.getName(), eval(right, env));
			CheapTerm evaledBody = eval(abst.getChild(), env);
			if(oldValue==null)//環境を元に戻す
				env.remove(abst.getName());
			else
				env.put(abst.getName(), oldValue);
			return evaledBody;
		}else{
			if(FLAG) System.out.println("Eval#evalApp#ELSE	left:"+left+"	right:"+right+"	body:"+body+"	env:"+env);
			return new CheapApp(body, eval(right, env));
		}
	}
	
}
