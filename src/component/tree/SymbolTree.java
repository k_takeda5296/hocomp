package component.tree;

import java.util.ArrayList;
import java.util.List;

public class SymbolTree{
	
	private String symbol_;
	private SymbolTree parent_;
	private List<SymbolTree> children_ = new ArrayList<SymbolTree>();
	
	public static SymbolTree getRoot(){
		return new SymbolTree("__ROOT__");
	}
	
	public SymbolTree(String symbol){
		symbol_ = symbol;
	}
	
	public String getSymbolName(){
		return symbol_;
	}
	
	public SymbolTree getParent() {
		return parent_;
	}
	public void setParent(SymbolTree parent) {
		this.parent_ = parent;
	}
	
	public void addChild(SymbolTree child){
		children_.add(child);
	}
	
	public List<SymbolTree> getChildren(){
		return children_;
	}
	
	public int size(){
		int size = 1;
		for(SymbolTree child : children_){
			size += child.size();
		}
		return size;
	}
	
	@Override
	public String toString(){
		String str = symbol_;
		for(SymbolTree child : children_){
			str += " (" + child.toString() + ")";
		}
		return str;
	}
}