package component.occurrenceset;


import java.util.LinkedHashSet;
import java.util.Set;

import component.context.Context;
import component.context.term.CApp;
import component.context.term.CTerm;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyTerm;

final class OccListExclusiveOverlap implements OccurrenceSet{

	private Context context_;
	private Set<TyApp> nodes_;
	private boolean isDownRight_;

	public OccListExclusiveOverlap(Context context){
		context_ = context;
		nodes_ = new LinkedHashSet<TyApp>();
		CTerm cTerm = context.getCTerm();
		isDownRight_ = cTerm instanceof CApp && ((CApp)cTerm).getRightChild() instanceof CApp;
	}

	public Context getContext(){
		return context_;
	}

	//β展開によって減るラベルサイズの概算（同値穴にはラベルサイズ１の部分木（シンボル）が入っていると仮定）
	public int reductionLabelSize(){
		int reducLabelPerOcc = context_.getHoleNum() - context_.getK() + context_.labelSize();
		int newLabels = 1 + nodes_.size() + context_.getHoleNum() + context_.getK() + context_.labelSize();
		return nodes_.size() > 1 ? nodes_.size() * reducLabelPerOcc - newLabels : -1;
	}

	//β展開によって減るラベルサイズの概算（同値穴にはラベルサイズ１の部分木（シンボル）が入っていると仮定）
	public int reductionLabelSizeOfBody(){
		int reducLabelOfBodyPerOcc = context_.getHoleNum() - context_.getK() + context_.labelSize() - 1;
		return nodes_.size() > 1 ? nodes_.size() * reducLabelOfBodyPerOcc : -1;
	}

	@Override
	public boolean isDownRight(){
		return isDownRight_;
	}

	@Override
	public void removeNodes(Set<TyApp> nodes){
		nodes.forEach(node -> {
			nodes_.remove(node);
		});
	}

	@Override
	public boolean addNode(TyApp node){
		if(!overlapCheck(node)){//出現の上下のTyAppですでに追加されているものが無いか確認する（ある場合は追加しない）
			nodes_.add(node);
			return true;
		}else
			return false;
	}

	private boolean overlapCheck(TyApp node){
		//必要数だけ辿った祖先で追加されているものが無いかチェック
		int parentNum = context_.nodeSize()/2 - 1;
		TyTerm parent = node.getParent();
		while(parentNum > 0){
			if(parent instanceof TyAbst) break;//文脈はλ抽象ノードを含まないという仮定から
			if(nodes_.contains(parent)) return true;
			parent = parent.getParent();
			parentNum--;
		}
		//子で既に追加されているものが無いかチェック
		Set<TyTerm> children = context_.correspondingNodeSet(node);
		for(TyTerm child : children){
			if(child instanceof TyApp && nodes_.contains(child)) return true;
		}
		return false;
	}

	@Override
	public boolean isEmpty(){
		return size() < 1;
	}

	@Override
	public Set<TyApp> getC2Nodes(){
		return nodes_;
	}

	@Override
	public int size(){
		return nodes_.size();
	}

	/*
	 * Objectメソッド
	 */
	@Override
	public String toString(){
		String str = "{ context("+reductionLabelSize()+"):"+context_+", nodes["+nodes_.size()+", hash:"+context_.hashCode()+"] }";
		return str;
	}
}
