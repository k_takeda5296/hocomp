package component.occurrenceset;


import java.util.Set;

import component.context.Context;
import component.tyterm.term.TyApp;


public interface OccurrenceSet {

	/**
	 * nodeを追加しようとし、追加できたかを返す。
	 * @param node
	 * @return 追加できたときはtrue, そうでないときはfalse
	 */
	boolean addNode(TyApp node);
	void removeNodes(Set<TyApp> nodes);
	int size();//TODO:sizeはOccSetの空判定のみに用いられているのでbooleanの関数を用意する．また、sizeの取得はOccWithOverlapでは定数時間ではない．定数時間だった．-> occSize_
	Context getContext();
	boolean isEmpty();
	Set<TyApp> getC2Nodes();
	int reductionLabelSize();
	int reductionLabelSizeOfBody();
	boolean isDownRight();

}
