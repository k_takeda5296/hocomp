package component.occurrenceset;


import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import component.context.Context;
import component.context.term.CApp;
import component.context.term.CTerm;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyTerm;

/*
 * オーバーラップしうる文脈のうちa(a []), [] a a, []1([]1 []2), []1 []2 []2のみに対応した出現集合で、同時に置き換えられる最大数の出現を保持する
 * 一般にはグラフの問題に落とし込める（？）
 */

final class OccListWithOverlap implements OccurrenceSet{

	/*
	 * segMap_とSegmentをstaticにするかどうか
	 */
	private Map<TyApp, Segment> segMap_ = new LinkedHashMap<TyApp, Segment>();

	private class Segment{

		private TyApp top_;
		private TyApp tail_;
		private int size_;

		Segment(TyApp app){
			top_ = tail_ = app;
			size_ = 1;
		}

		int size(){
			return size_;
		}

		void addTop(TyApp app){
			top_ = app;
			size_++;
		}

		void addTail(TyApp app){
			tail_ = app;
			size_++;
		}

		/*
		 * セグメントのサイズが1の時にremoveは行われないことを仮定している。（segmentはノード数が1以上を仮定している）
		 * また、セグメントの分割は起こらないことを仮定している
		 * すなわち、removeは必ず異なるtopかtailであるノードでどちらかに対して行われる
		 */
		void remove(TyApp app){
			//if(size_ < 2) throw new IllegalArgumentException();
			size_--;
			if(top_==app){
				try{
					TyApp newTop = isDownRight_ ? (TyApp)top_.getRightChild() : (TyApp)top_.getLeftChild();
					segMap_.remove(app);segMap_.put(newTop, this);
					top_ = newTop;
				}catch(Exception e){
					System.err.println("OccLisWithOverlap#Segment#remove size:"+size_+"	context:"+context_);
					System.err.println("OccLisWithOverlap#Segment#remove	tail:"+tail_+"	tail.parent:"+tail_.getParent());
					System.err.println("OccLisWithOverlap#Segment#remove	top:"+top_+"	top.parent:"+top_.getParent());
					System.err.println("OccLisWithOverlap#Segment#remove	segMap["+segMap_.size()+"]:"+segMap_);
					//throw e;
				}
				return;
			}else{// if(tail_==app){//ここのif文は不要
				try{
					tail_ = (TyApp)tail_.getParent();
					segMap_.remove(app);segMap_.put(tail_, this);
				}catch(Exception e){
					System.err.println("OccLisWithOverlap#Segment#remove	tail:"+tail_+"	size:"+size_+"	context:"+context_);
					System.err.println("OccLisWithOverlap#Segment#remove	segMap:"+segMap_);
					//throw e;
				}
				return;
			}
		}

		void obtainNodes(Set<TyApp> nodes){
			TyApp app = tail_;
			while(true){
				nodes.add(app);
				if(!(app.getParent() instanceof TyApp)) break;
				if(app==top_) break;
				app = (TyApp)app.getParent();
				if(!(app.getParent() instanceof TyApp)) break;
				if(app==top_) break;
				app = (TyApp)app.getParent();
			}
		}

		@Override
		public String toString(){
			return "{["+size_+"]}";//, top:"+top_+"	tail:"+tail_+"}";
		}
	}

	private Context context_;
	private Set<Segment> segments_;
	private boolean isDownRight_;
	private int occSize_;

	public OccListWithOverlap(Context context){
		context_ = context;
		segments_ = new LinkedHashSet<Segment>();
		CTerm cTerm = context.getCTerm();
		isDownRight_ = cTerm instanceof CApp && ((CApp)cTerm).getRightChild() instanceof CApp;
		occSize_ = 0;
	}

	public Context getContext(){
		return context_;
	}

	/*
	 * 必要呼びにする
	 */
	private int occSize(){
		return occSize_;
	}

	//β展開によって減るラベルサイズの概算（同値穴にはラベルサイズ１の部分木（シンボル）が入っていると仮定）
	public int reductionLabelSize(){
		int reducLabelPerOcc = context_.getHoleNum() - context_.getK() + context_.labelSize();
		int newLabels = 1 + occSize() + context_.getHoleNum() + context_.getK() + context_.labelSize();
		return occSize() > 1 ?  occSize() * reducLabelPerOcc - newLabels : -1;
	}

	//β展開によって減るラベルサイズの概算（同値穴にはラベルサイズ１の部分木（シンボル）が入っていると仮定）
	public int reductionLabelSizeOfBody(){
		int reducLabelPerOcc = context_.getHoleNum() - context_.getK() + context_.labelSize() - 1;
		return occSize() > 1 ? occSize() * reducLabelPerOcc : -1;
	}


	@Override
	public boolean isDownRight(){
		return isDownRight_;
	}

	@Override
	public void removeNodes(Set<TyApp> nodes){
		//ノードを所属するセグメントに振り分ける
		Map<Segment, Set<TyApp>> groupedNodes = groupNodesBySegment(nodes);

		//各セグメントについて、末尾から削除する。末尾のノードが無くなった時点で他のノードが残っている場合はエラー
		for(Segment s : groupedNodes.keySet()){
			removeNodesFromSegment(s, groupedNodes.get(s));
		}
	}

	private void removeNodesFromSegment(Segment s, Set<TyApp> nodes){
		while(s.size() > 0 && nodes.size() > 0){
			TyApp tail = s.tail_;
			if(!nodes.contains(tail)) throw new IllegalArgumentException("tail should be contained!");
			removeNode(tail);
			nodes.remove(tail);
		}
		if(s.size() < 1 && nodes.size() > 0) throw new IllegalArgumentException("s is already empty!");
	}

	private Map<Segment, Set<TyApp>> groupNodesBySegment(Set<TyApp> nodes){
		Map<Segment, Set<TyApp>> nodesToRemove = new HashMap<>();
		for(TyApp node : nodes){
			if(!segMap_.containsKey(node)){
				System.err.println("OccListWithOverlap	context:"+context_+"	segMap["+segMap_.size()+"]:"+segMap_);
				System.err.println("OccListWithOverlap	context:"+context_+"	node:"+node);
				throw new IllegalArgumentException("no segment for the node!");
			}
			Segment s = segMap_.get(node);
			if(!nodesToRemove.containsKey(s)) nodesToRemove.put(s, new HashSet<>());
			nodesToRemove.get(s).add(node);
		}
		return nodesToRemove;
	}

	private void removeNode(TyApp node){
		Segment s = segMap_.get(node);
		if(s.size() % 2 == 1) occSize_--;//s.sizeが奇数のとき、s.size-1は偶数なので、s.sizeが1減ると(s.size-1)/2も1減る
		if(s.size()==1){
			segMap_.remove(node);//ノード数が1のsegmentはsegMapからremoveするだけ。（segmentはノード数が常に1以上を仮定）
			segments_.remove(s);
			return;
		}
		s.remove(node);
	}

	/*
	 * addNodeの分岐順を変えるべき？
	 */
	public boolean addNode(TyApp term){
		if(!(term instanceof TyApp)) throw new IllegalArgumentException();
		TyApp node = (TyApp)term;
		TyTerm child = isDownRight_ ? node.getRightChild() : node.getLeftChild();
		if(segMap_.containsKey(child)){
			Segment s = segMap_.get(child);
			if(s.size() % 2 == 0) occSize_++;//s.sizeが偶数のとき、s.size-1は奇数なので、s.sizeが1増えると(s.size-1)/2も1増える
			if(s.size()!=1) segMap_.remove(child);//top==tailのとき、removeしない
			segMap_.put(node, s);
			s.addTop(node);
		}else if(segMap_.containsKey(node.getParent())){
			Segment s = segMap_.get(node.getParent());
			if(s.size() % 2 == 0) occSize_++;//s.sizeが偶数のとき、s.size-1は奇数なので、s.sizeが1増えると(s.size-1)/2も1増える
			if(s.size()!=1) segMap_.remove(node.getParent());//top==tailのとき、removeしない
			segMap_.put(node, s);
			s.addTail(node);
		}else{
			Segment s = new Segment(node);
			segMap_.put(node, s);
			segments_.add(s);
			occSize_++;
		}
		return true;
	}


	@Override
	public boolean isEmpty(){
		return occSize() < 1;
	}

	public Set<TyApp> getC2Nodes(){
		Set<TyApp> nodes = new LinkedHashSet<TyApp>();
		for(Segment s : segments_){
			s.obtainNodes(nodes);
		}
		return nodes;
	}

	/*
	 * 正しい？？？？
	 * TODO:sizeを保持する．たぶん出来る．
	 * 保持してた．-> occSize_
	 */
	public int size(){
		return occSize();
	}

	/*
	 * Objectメソッド
	 */
	@Override
	public String toString(){
		String str = "{ context("+reductionLabelSize()+"):"+context_+", nodes["+occSize()+"], segMap.size:"+segMap_.size()+"	segments:"+segments_+" }";
		return str;
	}

	@Override
	public boolean equals(Object obj){
		if(obj==this) return true;
		if(!(obj instanceof OccListWithOverlap)) return false;
		OccListWithOverlap occList = (OccListWithOverlap)obj;
		return this.context_.equals(occList.context_);
	}

	@Override
	public int hashCode(){
		return context_.hashCode();
	}
}
