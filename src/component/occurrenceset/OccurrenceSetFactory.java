package component.occurrenceset;

import java.util.HashMap;
import java.util.Map;

import component.context.Context;
import compressor.HOCompressor;

public class OccurrenceSetFactory {

	public static Map<Integer, Context> hashCodes = new HashMap<Integer, Context>();
	public static int totalNum = 0;
	public static int maxHash = Integer.MIN_VALUE;
	public static int minHash = Integer.MAX_VALUE;
	public static OccurrenceSet makeOccSet(Context c){
		if(HOCompressor.CONTEXT_HASH_CONFLICTS_CHECK){
			totalNum++;
			int hash = c.hashCode();
			if(hashCodes.containsKey(hash)){
				Context c0 = hashCodes.put(hash, c);
				System.out.println("Tables#makeOccSet["+hash+"]["+c.equals(c0)+"]	c0:"+c0);
				System.out.println("Tables#makeOccSet["+hash+"]["+c0.equals(c)+"]	c1:"+c);
				if(c.equals(c0)) totalNum--;
			}else{
				hashCodes.put(hash, c);
			}
			maxHash = hash > maxHash ? hash : maxHash;
			minHash = hash < minHash ? hash : minHash;
		}
		return selectOccSet(c);
	}
	
	private static OccurrenceSet selectOccSet(Context c){
		return new OccListExclusiveOverlap(c);//c instanceof OverlappableContext ? new OccListWithOverlap(c) : new OccListExclusiveOverlap(c);
	}
}
