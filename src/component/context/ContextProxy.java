package component.context;

import java.util.Arrays;

import util.MyUtil;
import component.context.term.CApp;
import component.tyterm.term.TyAbst;
import component.tyterm.type.Type;

/*
 * Contextのflyweight化を図るにあたって、mapのキーにTyAppを使う訳には行かないため、
 * 代わりにContextのキーを担うクラス。Contextと1対1の情報を持つ。
 * Proxyパターンとかでは全然ない。
 * builderの役割も持っているが、無い方が良いか。
 */
class ContextProxy {
	
	private CApp cTerm_;
	private Type type_;
	private int[] ids_;
	private Type[] idTypes_;
	private TyAbst[] absts_;//ids_の型をTyId[]にしておけば持っている必要は無いが、その場合はTyIdのダミーを作る必要がある。TyVarのダミーを作るとTyAbst.vars_とかの処理が面倒そう（ダミー.binder.varsにダミーは含まれないなど）
	private int[] holeIds_;
	private Type[] holeTypes_;
	
	ContextProxy(CApp cTerm, Type type, int[] ids, Type[] idTypes, TyAbst[] absts, int[] holeIds, Type[] holeTypes){
		cTerm_ = cTerm;
		type_ = type;
		ids_ = ids;
		idTypes_ = idTypes;
		absts_ = absts;
		holeIds_ = holeIds;
		holeTypes_ = holeTypes;
	}
	
	@Override
	public String toString(){
		String str = this.getClass().getSimpleName();
		str += "{ ";
		str += cTerm_.toString()+", ";
		str += "ids:"+Arrays.toString(ids_)+", ";
		str += "holes:"+Arrays.toString(holeIds_)+", ";
		str += type_+" }";
		return str;
	}
	
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof ContextProxy)) return false;
		ContextProxy p = (ContextProxy)obj;
		return this.cTerm_.equals(p.cTerm_) && Arrays.equals(this.ids_, p.ids_) && Arrays.equals(this.absts_, p.absts_)
				&& Arrays.equals(this.holeIds_, p.holeIds_) && Arrays.equals(this.holeTypes_, p.holeTypes_);
	}
	
	@Override
	public int hashCode(){
		return MyUtil.makeHashCode(new Object[]{ 
				cTerm_, type_, Arrays.hashCode(ids_), Arrays.hashCode(idTypes_),
				Arrays.hashCode(absts_), Arrays.hashCode(holeIds_), Arrays.hashCode(holeTypes_) });
	}
	
	Context supplyContext(){
		if(overlappable()) return new OverlappableContext(cTerm_, type_, ids_, idTypes_, absts_, holeIds_, holeTypes_);
		else return new UnOverlappableContext(cTerm_, type_, ids_, idTypes_, absts_, holeIds_, holeTypes_);
	}
	
	/**
	 * TODO:Overlappableチェックの一般化（その文脈を持ち得るappが丁度2つであること）
	 * @return
	 */
	private boolean overlappable(){
		if(cTerm_.nodeSize()!=5) return false;
		if(ids_.length==2 && Arrays.stream(ids_).distinct().count()==1) return true;//a(a [])か[] a aのとき
		if(ids_.length==0 && Arrays.stream(holeIds_).distinct().count()==2) return true;//[]1([]1 []2)か[]1 []2 []2のとき
		return false;
	}

	/**
	 * 出現数によってラベルサイズが減少し得るかを返す
	 */
	public boolean isEffective() {
		int decPerOcc = ids_.length + holeIds_.length - (int)Arrays.stream(holeIds_).distinct().count() - 1;
		return decPerOcc > 0;
	}
}
