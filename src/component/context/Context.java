package component.context;

import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import component.context.term.CApp;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Type;

public interface Context {
	
	/**
	 * CTermを返す
	 * @return
	 */
	CApp getCTerm();
	
	/**
	 * 穴の最大の添字を返す
	 * @return
	 */
	int getK();
	
	/**
	 * ノードサイズを返す
	 * @return
	 */
	int nodeSize();
	
	/**
	 * ラベルサイズを返す
	 * @return
	 */
	int labelSize();
	
	/**
	 * 穴の数を返す
	 * @return
	 */
	int getHoleNum();
	
	/**
	 * 出現数によってβ展開でラベルサイズを減少させ得るかを返す
	 * @return
	 */
	boolean isEffective();
	
	/**
	 * 穴以外に対応する全てのノードのSetを返す
	 * @param term
	 * @return
	 */
	Set<TyTerm> correspondingNodeSet(TyTerm term);
	
	/**
	 * 最深の束縛子をOptionalで返す
	 * @return
	 */
	Optional<TyAbst> getC0Foot();
	
	/**
	 * β展開で抜き出したλ項（特に穴がある時はλ抽象）をoccurrenceを出来るだけ再利用しながら作る。
	 * 生成されたλ抽象のうち一番深いものはsimplifySetに追加される（η簡約可能の可能性あり）
	 * @param occurrence
	 * @param simplifySet
	 * @return 生成されたλ項
	 */
	TyTerm makeCorrespondingTerm(TyApp occurrence, Set<TyAbst> simplifySet);
	
	/**
	 * 各添字の穴に対応するλ項の配列を返す。同時に、同値穴により削除される変数ノードを集める（削除されるノードと再利用されるノードの整合性を保つため）
	 * @param occurrence
	 * @param removedVars
	 * @return
	 */
	TyTerm[] getHoleTermsAndCollectRemovedHoleVars(TyApp occurrence, Set<TyVar> removedVars);
	
	/**
	 * β展開で抜き出した時に生成されるλ項の型を返す
	 * @return
	 */
	Type getCorrespondingTermType();
	
	/**
	 * 関数適用ノードに対応するTyAppをappsに追加する
	 * @param occurrence
	 * @param apps
	 */
	void collectCorrespondingApps(TyApp occurrence, LinkedHashSet<TyApp> apps);
	
	/**
	 * 文脈の変数ノードに対応するλ項の変数ノードを集める
	 * @param term
	 * @param removedVars
	 */
	void collectRemovedVars(TyTerm term, Set<TyVar> removedVars);
}
