package component.context.term;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class CTermEnumerator {

	public static void main(String[] args){
		for(int i=0; i < 8; i++){
			new CTermEnumerator(i);
		}
	}

	private Map<Integer, List<AbstractCTerm>> cTermsMap_ = new HashMap<>();
	private int maxSize_;

	public CTermEnumerator(int maxSize){
		maxSize_ = maxSize%2 == 0 ? maxSize-1 : maxSize;
		init();
		System.out.println("maxSize:"+maxSize_);
		System.out.println("cTermsList:");
		cTermsMap_.keySet().forEach(i -> {
			System.out.println(i+":["+cTermsMap_.get(i).size()+"]"+cTermsMap_.get(i));
		});
	}

	public List<CApp> getCTerms(){
		return cTermsMap_.values()
				.stream()
				.flatMap(c -> c.stream())
				.filter(c -> c instanceof CApp)
				.map(c -> (CApp)c)
				.collect(Collectors.toList());
	}

	private void init(){
		List<AbstractCTerm> cTerms0 = new ArrayList<>();
		cTerms0.add(CId.getCId());
		cTerms0.add(CHole.getCHole());
		cTermsMap_.put(1, cTerms0);
		for(int i=3; i < maxSize_+1; i+=2){
			List<AbstractCTerm> cTerms = new ArrayList<>();
			int size = i;
			for(int j=1; j < size; j+=2){
				List<AbstractCTerm> lCTerms = cTermsMap_.get(j);
				List<AbstractCTerm> rCTerms = cTermsMap_.get(i-j-1);
				for(AbstractCTerm lCTerm : lCTerms){
					for(AbstractCTerm rCTerm : rCTerms){
						CApp app = new CApp(lCTerm, rCTerm);
						cTerms.add(app);
					}
				}
			}
			cTermsMap_.put(i, cTerms);
		}
	}

}
