package component.context.term;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyId;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Type;



public class CApp extends AbstractCTerm {

	private AbstractCTerm leftChild_;
	private AbstractCTerm rightChild_;

	private Integer size_ = null;
	private Integer labelSize_ = null;

	public CApp(AbstractCTerm leftChild, AbstractCTerm rightChild){
		leftChild_ = leftChild;
		rightChild_ = rightChild;
	}

	@Override
	public String toString(){
		String str = "";
		boolean isAbstLeft = false;
		str += isAbstLeft ? "(" : "";
		str += leftChild_.toString();
		str += isAbstLeft ? ")" : "";
		str += " ";
		boolean isIdRight = rightChild_ instanceof CId;
		str += isIdRight ? "" : "(";
		str += rightChild_.toString();
		str += isIdRight ? "" : ")";
		return str;
	}

	public CTerm getLeftChild(){
		return leftChild_;
	}

	public CTerm getRightChild(){
		return rightChild_;
	}
	

	@Override
	public int nodeSize(){
		if(size_==null){
			size_ = leftChild_.nodeSize() + rightChild_.nodeSize() + 1;
		}
		return size_;
	}

	@Override
	public int sizeWOHoles() {
		return leftChild_.sizeWOHoles() + rightChild_.sizeWOHoles() + 1;
	}


	@Override
	void correspondingNodeSet(TyTerm node, Set<TyTerm> set) {
		if(!(node instanceof TyApp)) throw new IllegalArgumentException("CApp#correspondingNodeSet:typeError!	this:"+this+"	node:"+node);
		TyApp app = (TyApp)node;
		set.add(app);
		leftChild_.correspondingNodeSet(app.getLeftChild(), set);
		rightChild_.correspondingNodeSet(app.getRightChild(), set);
	}

	@Override
	public int labelSize() {
		if(labelSize_==null){
			labelSize_ = leftChild_.labelSize() + rightChild_.labelSize();
		}
		return labelSize_;
	}


	@Override
	public void getAllHoleTerms(TyTerm term, List<TyTerm> holeTerms) {
		if(!(term instanceof TyApp)) throw new IllegalArgumentException("TyApp#getAllHoleTerms:	Match Error!	term.class:"+term.getClass());
		TyApp app = (TyApp)term;
		leftChild_.getAllHoleTerms(app.getLeftChild(), holeTerms);
		rightChild_.getAllHoleTerms(app.getRightChild(), holeTerms);
	}

	@Override
	public int holeNum() {
		return leftChild_.holeNum() + rightChild_.holeNum();
	}

	@Override
	public TyTerm makeCorrespondsTerm(TyTerm term, TyAbst[] newAbsts, int[] holeIds, AtomicInteger index) {
		if(!(term instanceof TyApp)) throw new IllegalArgumentException("term should be TyApp!");
		TyApp app = (TyApp) term;
		TyTerm left = leftChild_.makeCorrespondsTerm(app.getLeftChild(), newAbsts, holeIds, index);
		TyTerm right = rightChild_.makeCorrespondsTerm(app.getRightChild(), newAbsts, holeIds, index);
		app.setLeftChild(left);
		left.setParent(app);
		app.setRightChild(right);
		right.setParent(app);
		return app;
	}


	@Override
	void obtainTyIdList(TyTerm term, List<TyId> tyIdList) {
		if(!(term instanceof TyApp)) throw new IllegalArgumentException("term should be TyApp!");
		TyApp app = (TyApp)term;
		leftChild_.obtainTyIdList(app.getLeftChild(), tyIdList);
		rightChild_.obtainTyIdList(app.getRightChild(), tyIdList);
	}

	@Override
	void obtainHoleIdList(TyTerm term, AtomicInteger index, Map<Integer, Integer> tyIdSet, List<Integer> holeIdList) {
		if(!(term instanceof TyApp)) throw new IllegalArgumentException("term should be TyApp!");
		TyApp app = (TyApp)term;
		leftChild_.obtainHoleIdList(app.getLeftChild(), index, tyIdSet, holeIdList);
		rightChild_.obtainHoleIdList(app.getRightChild(), index, tyIdSet, holeIdList);
	}

	@Override
	void obtainHoleTypeList(TyTerm term, List<Type> holeTypeList) {
		if(!(term instanceof TyApp)) throw new IllegalArgumentException("term should be TyApp!");
		TyApp app = (TyApp)term;
		leftChild_.obtainHoleTypeList(app.getLeftChild(), holeTypeList);
		rightChild_.obtainHoleTypeList(app.getRightChild(), holeTypeList);
	}

	@Override
	public boolean matchCheck(TyTerm term) {
		if(!(term instanceof TyApp)) return false;
		TyApp app = (TyApp)term;
		return leftChild_.matchCheck(app.getLeftChild()) && rightChild_.matchCheck(app.getRightChild());
	}

	@Override
	public void collectRemoveNodes(TyTerm term, LinkedHashSet<TyApp> removeNodes) {
		if(!(term instanceof TyApp)) throw new IllegalArgumentException("term should be TyApp!");
		TyApp app = (TyApp)term;
		removeNodes.add(app);
		leftChild_.collectRemoveNodes(app.getLeftChild(), removeNodes);
		rightChild_.collectRemoveNodes(app.getRightChild(), removeNodes);
	}
	
	@Override
	public void collectRemovedVars(TyTerm term, Set<TyVar> removeVars){
		if(!(term instanceof TyApp)) throw new IllegalArgumentException("term should be TyApp!");
		TyApp app = (TyApp)term;
		leftChild_.collectRemovedVars(app.getLeftChild(), removeVars);
		rightChild_.collectRemovedVars(app.getRightChild(), removeVars);
	}

}
