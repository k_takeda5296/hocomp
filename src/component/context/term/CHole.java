package component.context.term;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyId;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Type;


public final class CHole extends AbstractCTerm {

	public static CHole getCHole(){
		return CHOLE;
	}
	private static CHole CHOLE = new CHole();
	private CHole(){}

	@Override
	public String toString(){
		return "[_]";
	}
	
	@Override
	public int nodeSize(){
		return 1;
	}

	@Override
	public int sizeWOHoles() {
		return 0;
	}
	
	@Override
	void correspondingNodeSet(TyTerm node, Set<TyTerm> set) {}

	@Override
	public int labelSize() {
		return 0;
	}


	@Override
	public void getAllHoleTerms(TyTerm term, List<TyTerm> holeTerms) {
		holeTerms.add(term);
	}


	@Override
	public int holeNum() {
		return 1;
	}

	@Override
	public TyTerm makeCorrespondsTerm(TyTerm term, TyAbst[] newAbsts, int[] holeIds, AtomicInteger index) {
		return newAbsts[holeIds[index.getAndIncrement()]].makeVar();
	}


	@Override
	void obtainTyIdList(TyTerm term, List<TyId> tyIdList) {}

	@Override
	void obtainHoleIdList(TyTerm term, AtomicInteger index, Map<Integer, Integer> tyIdSet, List<Integer> holeIdList) {
		if(term instanceof TyId){
			TyId id = (TyId)term;
			if(!tyIdSet.containsKey(id.getName())) tyIdSet.put(id.getName(), index.getAndIncrement());
			holeIdList.add(tyIdSet.get(id.getName()));
		}else{
			holeIdList.add(index.getAndIncrement());
		}
	}

	@Override
	void obtainHoleTypeList(TyTerm term, List<Type> holeTypeList) {
		holeTypeList.add(term.getType());
	}

	@Override
	public boolean matchCheck(TyTerm term) {
		return true;
	}

	@Override
	public void collectRemoveNodes(TyTerm term, LinkedHashSet<TyApp> removeNodes) {}

	@Override
	public void collectRemovedVars(TyTerm term, Set<TyVar> removeVars) {}
}
