package component.context.term;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import component.tyterm.term.TyId;
import component.tyterm.term.TyTerm;
import component.tyterm.type.Type;

abstract class AbstractCTerm implements CTerm{
	
	@Override
	public Set<TyTerm> correspondingNodeSet(TyTerm node) {
		Set<TyTerm> set = new LinkedHashSet<TyTerm>();
		correspondingNodeSet(node, set);
		return set;
	}
	abstract void correspondingNodeSet(TyTerm node, Set<TyTerm> set);
	
	
	@Override
	public final List<TyTerm> getAllHoleTerms(TyTerm term){
		List<TyTerm> holeTermList = new ArrayList<>();
		getAllHoleTerms(term, holeTermList);
		return holeTermList;
	}
	abstract void getAllHoleTerms(TyTerm term, List<TyTerm> holeTermList);
	
	
	@Override
	public final List<TyId> obtainTyIdList(TyTerm term){
		List<TyId> tyIdList = new ArrayList<>();
		obtainTyIdList(term, tyIdList);
		return tyIdList;
	}
	abstract void obtainTyIdList(TyTerm term, List<TyId> tyIdList);
	
	
	@Override
	public final List<Integer> obtainHoleIdList(TyTerm term){
		List<Integer> holeIdList = new ArrayList<>();
		Map<Integer, Integer> tyIdSet = new HashMap<>();
		obtainHoleIdList(term, new AtomicInteger(0), tyIdSet, holeIdList);
		return holeIdList;
	}
	abstract void obtainHoleIdList(TyTerm term, AtomicInteger index, Map<Integer, Integer> tyIdSet, List<Integer> holeIdList);
	
	
	@Override
	public List<Type> obtainHoleTypeList(TyTerm term){
		List<Type>  holeTypeList = new ArrayList<>();
		obtainHoleTypeList(term, holeTypeList);
		return holeTypeList;
	}
	abstract void obtainHoleTypeList(TyTerm term, List<Type> holeTypeList);
}
