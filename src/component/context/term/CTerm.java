package component.context.term;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyId;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Type;

public interface CTerm {

	/**
	 * ノード数
	 * @return
	 */
	int nodeSize();
	
	/**
	 * CIdのノード数
	 * @return
	 */
	int labelSize();
	
	/**
	 * CHoleのノード数
	 * @return
	 */
	int holeNum();
	
	/**
	 * CHole以外のノード数
	 * @return
	 */
	int sizeWOHoles();
	
	/**
	 * CHole以外に対応するノードのSetを返す
	 * @param node
	 * @return
	 */
	Set<TyTerm> correspondingNodeSet(TyTerm node);
	
	/**
	 * termに現れるCHoleに対応するλ項の行きがけ順のリストを返す
	 * @param term
	 * @return
	 */
	List<TyTerm> getAllHoleTerms(TyTerm term);
	
	/**
	 * このCTermに対応するλ項をtermから作る。（CHole以外に対応するノードは再利用し、CHoleには新しいTyVarを生成する）
	 * @param term
	 * @param newAbsts
	 * @param holeIds
	 * @param index
	 * @return
	 */
	TyTerm makeCorrespondsTerm(TyTerm term, TyAbst[] newAbsts, int[] holeIds, AtomicInteger index);
	
	/**
	 * λ項がマッチするかチェック
	 * @param term
	 * @return
	 */
	boolean matchCheck(TyTerm term);
	
	/**
	 * CIdに対応するTyIdのリストを返す
	 * @param term
	 * @return
	 */
	List<TyId> obtainTyIdList(TyTerm term);
	
	/**
	 * CHoleに割り当てるid列を返す（等しいλ項の穴には必ず等しいidを振る。等しくないidの振り方も返した方が良い？？）
	 * @param term
	 * @return
	 */
	List<Integer> obtainHoleIdList(TyTerm term);
	
	/**
	 * CHoleに対応するのλ項の型の行きがけ順のリストを返す
	 * @param term
	 * @return
	 */
	List<Type> obtainHoleTypeList(TyTerm term);
	
	/**
	 * CAppに対応するTyAppをremoveNodesに追加する
	 * @param term
	 * @param removeNodes
	 */
	void collectRemoveNodes(TyTerm term, LinkedHashSet<TyApp> removeNodes);
	
	/**
	 * CIdに対応するTyVarをremoveVarsに追加する
	 * @param term
	 * @param removeVars
	 */
	void collectRemovedVars(TyTerm term, Set<TyVar> removeVars);
}
