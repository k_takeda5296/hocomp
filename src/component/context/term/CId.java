package component.context.term;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyId;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Type;


public final class CId extends AbstractCTerm{

	public static CId getCId(){ 
		return CID;
	}
	private static CId CID = new CId();
	private CId(){}
	
	@Override
	public String toString(){
		return "$";
	}
	
	@Override
	public int nodeSize(){
		return 1;
	}

	@Override
	public int sizeWOHoles() {
		return 1;
	}

	@Override
	void correspondingNodeSet(TyTerm node, Set<TyTerm> set) {
		if(!(node instanceof TyVar || node instanceof TySymbol)) throw new IllegalArgumentException("CId#correspondingNodeSet:typeError:	node is "+node.getClass());
		set.add(node);
	}

	@Override
	public int labelSize() {
		return 1;
	}


	@Override
	public void getAllHoleTerms(TyTerm term, List<TyTerm> terms) {
		if(!(term instanceof TyVar || term instanceof TySymbol)) throw new IllegalArgumentException("TyVar#getAllHoleTerms:	Match Error!	term.class:"+term.getClass());
		return;
	}

	@Override
	public int holeNum() {
		return 0;
	}

	@Override
	public TyTerm makeCorrespondsTerm(TyTerm term, TyAbst[] newAbsts, int[] holeIds, AtomicInteger index) {
		if(!(term instanceof TyId)) throw new IllegalArgumentException("term should be TyId!");
		return term;
	}

	@Override
	void obtainTyIdList(TyTerm term, List<TyId> tyIdList) {
		if(!(term instanceof TyId)) throw new IllegalArgumentException("term should be TyId!");
		tyIdList.add((TyId)term);
	}

	@Override
	void obtainHoleIdList(TyTerm term, AtomicInteger index, Map<Integer, Integer> tyIdSet, List<Integer> holeIdList) {}

	@Override
	void obtainHoleTypeList(TyTerm term, List<Type> holeTypeList) {}

	@Override
	public boolean matchCheck(TyTerm term) {
		return term instanceof TyId;
	}

	@Override
	public void collectRemoveNodes(TyTerm term, LinkedHashSet<TyApp> removeNodes) {
		if(!(term instanceof TyId)) throw new IllegalArgumentException("term should be TyId!");
	}

	@Override
	public void collectRemovedVars(TyTerm term, Set<TyVar> removeVars) {
		if(!(term instanceof TyId)) throw new IllegalArgumentException("term should be TyId!");
		if(term instanceof TyVar) removeVars.add((TyVar)term);
	}
}
