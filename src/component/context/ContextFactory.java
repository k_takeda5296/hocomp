package component.context;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import component.context.term.CApp;
import component.context.term.CTermEnumerator;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyId;
import component.tyterm.term.TyVar;
import component.tyterm.type.Type;


public class ContextFactory {
	
	private static Map<Integer, List<CApp>> C_TERM_LIST_MAP = new HashMap<>();
	private static final Map<ContextProxy, Context> PROXY_TO_CONTEXT_MAP = new HashMap<>();
	private static final Map<Context, ContextProxy> CONTEXT_TO_PROXY_MAP = new HashMap<>();
	
	public static void printContexts(){
		PROXY_TO_CONTEXT_MAP.keySet().forEach((ContextProxy c) -> System.out.println(PROXY_TO_CONTEXT_MAP.get(c)));
	}
	
	/**
	 * 文脈cの情報を削除する
	 * @param c
	 */
	public static void removeContext(Context c){
		if(!CONTEXT_TO_PROXY_MAP.containsKey(c)) throw new IllegalArgumentException("The factory doesn't have the context:"+c);
		ContextProxy p = CONTEXT_TO_PROXY_MAP.remove(c);
		if(!PROXY_TO_CONTEXT_MAP.containsKey(p)) throw new IllegalArgumentException("Invalid map situation for the context:"+c);
		PROXY_TO_CONTEXT_MAP.remove(p);
	}
	
	private static final boolean USE_LIST = true;
	/**
	 * λ項appが持っているノードサイズがcontextSizeまでの文脈の集合を返す
	 * @param app
	 * @param contextSize
	 * @return
	 */
	public static Collection<Context> makeContexts(TyApp app, int contextSize){
		if(!C_TERM_LIST_MAP.containsKey(contextSize)) C_TERM_LIST_MAP.put(contextSize, (new CTermEnumerator(contextSize)).getCTerms());
		List<CApp> cTermList = C_TERM_LIST_MAP.get(contextSize);
		return USE_LIST ? makeContextList(app, cTermList) : makeContextSet(app, cTermList);
	}
	private static List<Context> makeContextList(TyApp term, List<CApp> cTermList){
		return cTermList.stream()
				.filter((CApp cApp) -> cApp.matchCheck(term))
				.map((CApp cApp) -> makeContextProxy(term, cApp))
				.filter(p -> p.isEffective())
				.map((ContextProxy p) -> proxy2Context(p)).collect(Collectors.toList());
	}
	
	private static Set<Context> makeContextSet(TyApp term, List<CApp> cTermList){
		return cTermList.stream()
				.filter((CApp cApp) -> cApp.matchCheck(term))
				.map((CApp cApp) -> makeContextProxy(term, cApp))
				.filter(p -> p.isEffective())
				.map((ContextProxy p) -> proxy2Context(p)).collect(Collectors.toSet());
	}
	
	/**
	 * 文脈プロキシに対応する文脈を返す。Mapに存在しないときは新たに生成する。
	 * @param p
	 * @return
	 */
	private static Context proxy2Context(ContextProxy p){
		if(!checkContain(p)){
			Context c = p.supplyContext();
			PROXY_TO_CONTEXT_MAP.put(p, c);
			CONTEXT_TO_PROXY_MAP.put(c, p);
		}
		return PROXY_TO_CONTEXT_MAP.get(p);
	}
	
	private static boolean checkContain(ContextProxy p){
		return PROXY_TO_CONTEXT_MAP.containsKey(p);
	}
	
	//同じtraverseを何回も行っているので、ContextProxyBuilderとか作ると良い？
	/*
	 * idTypesとabstsのラムダ式が３分の２程度の実行時間を占めていて、idTypesとabstsはList<TyId>のみからもとまるから、ContextProxyにはList<TyId>のみ持っておくようにしたい。それだけだとTyIdの参照がいつまでも残ることになるので、要考察
	 * 問題：TyIdがλ項から削除されたときに、ContextProxyもなくなっていなければならない
	 * 解決手順：
	 * １、ContextFactory.CONTEXTS_MAPから用済みのContextProxyを削除する関数を定義する
	 * ２、TablesでOccMapから文脈が削除されるたびにContextFactory.CONTEXTS_MAPからContextProxyを削除する
	 * 
	 * １のために、Context -> ContextProxyのMapも持っている必要がある
	 * というか、今までContextFactoryの削除機構を作っていなかったのって良くない
	 */
	
	private static ContextProxy makeContextProxy(TyApp term, CApp cTerm){
		List<TyId> tyIds = cTerm.obtainTyIdList(term);
		if(cTerm.labelSize()!=tyIds.size()) throw new IllegalArgumentException("wrong # of tyIds or cTerm!");
		int[] ids = tyIds.stream().mapToInt((TyId id) -> id.getName()).toArray();
		Type[] idTypes = tyIds.stream().map((TyId id) -> id.getType()).toArray(Type[]::new);
		TyAbst[] absts = tyIds.stream().filter((TyId id) -> id instanceof TyVar).map(var -> ((TyVar)var).getBinder()).toArray(TyAbst[]::new);
		int[] holeIds = cTerm.obtainHoleIdList(term).stream().mapToInt(i -> i).toArray();
		Type[] holeTypes = cTerm.obtainHoleTypeList(term).stream().toArray(Type[]::new);
		return new ContextProxy(
				cTerm, term.getType(), ids, idTypes, 
				absts, holeIds, holeTypes);
	}

}
