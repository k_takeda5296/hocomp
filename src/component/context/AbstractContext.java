package component.context;

import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import component.context.term.CApp;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Fun;
import component.tyterm.type.Type;

abstract class AbstractContext implements Context{

	protected CApp cTerm_;
	protected int k_;
	protected Type type_;
	protected int[] ids_;
	protected Type[] idTypes_;
	protected TyAbst[] absts_;//ids_の型をTyId[]にしておけば持っている必要は無いが、その場合はTyIdのダミー的なものを作る必要がある。TyVarのダミーを作るとTyAbst.vars_とかの処理が面倒そう（ダミー.binder.varsにダミーは含まれないなど）
	protected int[] holeIds_;//idSize程度の長さのprimitiveでよい。byte?
	protected Type[] holeTypes_;

	protected int hashCode_;
	protected boolean hashed_ = false;

	AbstractContext(CApp cTerm, Type type, int[] ids, Type[] idTypes, TyAbst[] absts, int[] holeIds, Type[] holeTypes){
		cTerm_ = cTerm;
		k_ = (int)Arrays.stream(holeIds).distinct().count();
		type_ = type;
		ids_ = ids;
		idTypes_ = idTypes;
		absts_ = absts;
		holeIds_ = holeIds;
		holeTypes_ = holeTypes;
	}
	
	/*
	 * Objectメソッド
	 */
	@Override
	public String toString(){
		String str = this.getClass().getSimpleName();
		str += "{ ";
		str += cTerm_.toString()+", ";
		str += "ids:"+Arrays.toString(ids_)+", ";
		str += "holes:"+Arrays.toString(holeIds_)+", ";
		str += type_+" }";
		return str;
	}

	@Override
	public CApp getCTerm(){
		return cTerm_;
	}

	@Override
	public int getK() {
		return k_;
	}


	@Override
	public Optional<TyAbst> getC0Foot() {
		return Arrays.stream(absts_).max((x, y) -> x.depth() - y.depth());
	}

	@Override
	public int nodeSize() {
		return cTerm_.nodeSize();
	}

	@Override
	public int labelSize() {
		return cTerm_.labelSize();
	}

	@Override
	public int getHoleNum() {
		return cTerm_.holeNum();
	}

	@Override
	public TyTerm[] getHoleTermsAndCollectRemovedHoleVars(TyApp term, Set<TyVar> removedVars) {
		List<TyTerm> allHoleTerms = cTerm_.getAllHoleTerms(term);
		TyTerm[] holeTerms = new TyTerm[k_];
		for(int i=0; i < holeIds_.length; i++){
			int index = holeIds_[i];
			TyTerm holeTerm = allHoleTerms.get(i);
			if(holeTerms[index]==null)
				holeTerms[index] = holeTerm;
			else
				if(holeTerm instanceof TyVar) removedVars.add((TyVar)holeTerm);
		}
		return holeTerms;
	}
	
	@Override
	public void collectRemovedVars(TyTerm term, Set<TyVar> removedVars){
		cTerm_.collectRemovedVars(term, removedVars);
	}

	@Override
	public boolean isEffective() {
		int decPerOcc = ids_.length + holeIds_.length - (int)Arrays.stream(holeIds_).distinct().count() - 1;
		return decPerOcc > 0;
	}

	@Override
	public Set<TyTerm> correspondingNodeSet(TyTerm node) {
		return cTerm_.correspondingNodeSet(node);
	}


	private Type[] fixedHoleTypes(){
		Type[] holeTypes = new Type[k_];
		for(int i=0; i < holeTypes_.length; i++){
			holeTypes[holeIds_[i]] = holeTypes_[i];
		}
		return holeTypes;
	}

	@Override
	public TyTerm makeCorrespondingTerm(TyApp occurrence, Set<TyAbst> simplifySet) {
		for(TyAbst abst : absts_){
			if(abst != null) simplifySet.add(abst);//η簡約の可能性
		}
		if(k_ < 1) return occurrence;
		
		/*
		 * k_ > 0のときはラムダ抽象をつくる
		 */
		Type[] fixedHoleTypes = fixedHoleTypes();
		if(fixedHoleTypes.length!=k_) throw new IllegalArgumentException("invalid length of fixedHoleTypes:"+fixedHoleTypes.length+" where k="+k_+" this:"+this);
		
		//ラムダ抽象ノード列をまず作る
		TyAbst[] newAbsts = new TyAbst[k_];
		newAbsts[k_-1] = new TyAbst(false, new Fun(fixedHoleTypes[k_-1], occurrence.getType()));
		for(int i=1; i < k_; i++){
			newAbsts[k_-1-i] = new TyAbst(false, new Fun(fixedHoleTypes[k_-1-i], newAbsts[k_-i].getType()));
			TyAbst parent = newAbsts[k_-1-i];
			TyAbst child = newAbsts[k_-i];
			parent.setChild(child);
			parent.addAbstChild(child);
			child.setParent(parent);
			child.setAbstParent(parent);
		}
		TyTerm corTerm = cTerm_.makeCorrespondsTerm(occurrence, newAbsts, holeIds_, new AtomicInteger(0));
		corTerm.setParent(newAbsts[k_-1]);
		newAbsts[k_-1].setChild(corTerm);
		return newAbsts[0];
	}

	@Override
	public Type getCorrespondingTermType(){
		Type[] fixedHoleTypes = fixedHoleTypes();
		if(fixedHoleTypes.length!=k_) throw new IllegalArgumentException("invalid length of fixedHoleTypes:"+fixedHoleTypes.length+" where k="+k_+" this:"+this);
		Type type = type_;
		for(int i=0; i < k_; i++){
			Type argType = fixedHoleTypes[k_-1-i];
			type = new Fun(argType, type);
		}
		return type;
	}

	@Override
	public void collectCorrespondingApps(TyApp occurrence, LinkedHashSet<TyApp> removeNodes){
		cTerm_.collectRemoveNodes(occurrence, removeNodes);
	}
}
