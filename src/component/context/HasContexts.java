package component.context;

import java.util.Collection;


/*
 * 抽象クラスのほうがいい？
 */
public interface HasContexts {
	/**
	 * 文脈の最大ノード数を受け取り、文脈を数え上げる
	 * @param contextSize
	 */
	void countContext(int contextSize);
	
	/**
	 * 持っている文脈の集合を返す（内部で計算は行わない）
	 * @return 現在持っている文脈の集合
	 */
	Collection<Context> getContexts();
	
	/**
	 * 文脈集合からcを削除する
	 * @param c
	 */
	void removeContext(Context c);
	
	/**
	 * 持っている文脈の集合は数えなおす必要があるフラグを立てる
	 */
	void invalidateContexts();
	
	/**
	 * 持っている文脈の集合は数えなおす必要があるかを返す
	 * @return
	 */
	boolean hasInvalidatedContexts();
}
