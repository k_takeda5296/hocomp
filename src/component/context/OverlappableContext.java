package component.context;

import component.context.term.CApp;
import component.tyterm.term.TyAbst;
import component.tyterm.type.Type;

public class OverlappableContext extends AbstractContext{

	OverlappableContext(CApp cTerm, Type type, int[] ids, Type[] idTypes,
			TyAbst[] absts, int[] holeIds, Type[] holeTypes) {
		super(cTerm, type, ids, idTypes, absts, holeIds, holeTypes);
	}
}
