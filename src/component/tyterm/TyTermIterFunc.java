package component.tyterm;

import component.tyterm.term.TyTerm;

public interface TyTermIterFunc {
	public void iterFunc(TyTerm term);
}
