package component.tyterm.term;

import java.util.LinkedHashSet;
import java.util.Set;

import component.tyterm.type.Type;

abstract class AbstractTyTerm implements TyTerm{
	
	/*
	 * ポインタ：64*2 + 32 = 160 bit = 20 B
	 */
	protected Type type_;
	protected TyTerm parent_;

	@Override
	public final Type getType() {
		return type_;
	}

	@Override
	public final void setType(Type type){
		type_ = type;
	}

	@Override
	public final TyTerm getParent(){
		return parent_;
	}

	@Override
	public final void setParent(TyTerm parent) {
		parent_ = parent;
	}

	@Override
	public final Set<TyTerm> getAncestors(int num) {
		Set<TyTerm> ancestors = new LinkedHashSet<TyTerm>();
		getAncestors(num, ancestors);
		return ancestors;
	}

	@Override
	public final void getAncestors(int num, Set<TyTerm> ancestors) {
		if(num < 1 || this.parent_ instanceof TyRoot || parent_ == null) return;
		ancestors.add(parent_);
		parent_.getAncestors(num-1, ancestors);
	}


	public void clear() {
		parent_ = null;
	}

}
