package component.tyterm.term;


import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import component.cheapterm.CheapTerm;
import component.cheapterm.CheapVar;
import component.tyterm.TyTermIterFunc;
import component.tyterm.type.Fun;


public final class TyVar extends TyId{
	
	private TyAbst binder_;
	private int id_;
	private boolean fx_;
	
	public TyVar(TyAbst binder){
		id_ = binder.getId();
		fx_ = binder.getFX();
		type_ = ((Fun)binder.getType()).getLeft();
		binder_ = binder;
	}

	@Override
	public int getName() {
		return binder_.getId();
	}

	public TyAbst getBinder() {
		return binder_;
	}

	@Override
	public void replaceChild(TyTerm oldChild, TyTerm newChild) {
		throw new IllegalArgumentException("TyVar#replaceChild:	no child.");
	}

	@Override
	public boolean isSame(TyTerm tyterm) {//呼ばれ得ない
		if(!(tyterm instanceof TyVar)) return false;
		TyVar var = (TyVar)tyterm;
		return this.id_==var.id_ && this.fx_==var.fx_;
	}

	@Override
	public boolean isSame(TyTerm tyterm, Map<TyAbst, TyAbst> abstPairs) {
		if(!(tyterm instanceof TyVar)) return false;
		TyVar var = (TyVar)tyterm;
		return (this.id_==var.id_ && this.fx_==var.fx_)|| abstPairs.get(this.binder_)==var.binder_;
	}

	@Override
	public void iterPostOreder(TyTermIterFunc iterFunc) {
		iterFunc.iterFunc(this);
	}



	@Override
	public Set<TyTerm> getPosterity() {
		return new LinkedHashSet<TyTerm>();
	}

	@Override
	public void getPosterity(Set<TyTerm> posterity) {
		posterity.add(this);
		return;
	}

	@Override
	public String checkBidrectional() {
		return "";
	}

	@Override
	public int size() {
		return 1;
	}

	@Override
	public int labelSize() {
		return 1;
	}
	
	public String idStr(){
		return binder_.idStr();
	}

	@Override
	public String toStringAsFunctionalProgram(TyRoot root) {
		return idStr();
	}
	
	
	@Override
	public void toStringAsFunctionalProgram(TyRoot root, StringBuffer buffer) {
		buffer.append(idStr());
	}


	@Override
	public String toString(){
		return idStr();
	}
	
	@Override
	public void toString(StringBuffer buffer){
		buffer.append(toString());
	}


	@Override
	public String toStringWithType() {
		return getName()+":["+type_+"]";
	}

	@Override
	public void splitedTerm(List<TyTerm> terms) {
		terms.add(this);
	}

	@Override
	public TyTerm copy() {
		return binder_.makeVar();//束縛子がコピーするルートより上にあるため
	}
	
	@Override
	public TyTerm copy(Map<Integer, TyAbst> abstMap) {
		TyAbst abst = abstMap.get(getName());
		return abst==null ? binder_.makeVar() : abst.makeVar();//束縛子がコピーするルートの上か下か（名前が一意になる実装のため必ず前者のみ実行される）
	}

	@Override
	public CheapTerm cheapCopy(List<String> symbolNames) {
		return new CheapVar(idStr());
	}

	@Override
	public TyTerm copyWithAbsts(List<TyAbst> abstList) {
		return binder_.makeVar();//束縛子がコピーするルートより上にあるため
	}

	@Override
	public TyTerm copyWithAbsts(List<TyAbst> abstList,
			Map<Integer, TyAbst> abstMap) {
		TyAbst abst = abstMap.get(getName());
		return abst==null ? binder_.makeVar() : abst.makeVar();//束縛子がコピーするルートの上か下か(元々あるabstか新しいabstか)
	}

	@Override
	public boolean isWellBinded() {
		return false;
	}

	@Override
	public boolean isWellBinded(Set<TyAbst> absts) {
		return absts.contains(binder_);
	}

	@Override
	public boolean isWellTyped() {
		return true;
	}



}
