package component.tyterm.term;


import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import component.cheapterm.CheapSymbol;
import component.cheapterm.CheapTerm;
import component.tyterm.TyTermIterFunc;
import component.tyterm.type.Type;


/*
 * 子供の数が違う同名タグが考慮されていない．
 */
public final class TySymbol extends TyId {

	private int name_;

	public TySymbol(int name, Type type){
		name_ = name;
		type_ = type;
	}

	public int getName() {
		return name_;
	}

	@Override
	public void replaceChild(TyTerm oldChild, TyTerm newChild) {
		throw new IllegalArgumentException("TySymbol#replaceChild:	no child.");
	}

	@Override
	public boolean isSame(TyTerm tyterm) {
		if(!(tyterm instanceof TySymbol)) return false;
		TySymbol symbol = (TySymbol)tyterm;
		return this.name_==symbol.name_;
	}
	
	@Override
	public boolean isSame(TyTerm tyterm, Map<TyAbst, TyAbst> abstPairs) {
		return isSame(tyterm);
	}

	@Override
	public void iterPostOreder(TyTermIterFunc iterFunc) {
		iterFunc.iterFunc(this);
	}

	@Override
	public Set<TyTerm> getPosterity() {
		return new LinkedHashSet<TyTerm>();
	}

	@Override
	public void getPosterity(Set<TyTerm> posterity) {
		posterity.add(this);
		return;
	}

	@Override
	public String checkBidrectional() {
		return "";
	}

	@Override
	public int size() {
		return 1;
	}

	@Override
	public int labelSize() {
		return 1;
	}

	private String symbolName(TyRoot root){
		return root.getSymbolName(this);
	}

	@Override
	public String toStringAsFunctionalProgram(TyRoot root) {
		return symbolName(root);
	}
	
	@Override
	public void toStringAsFunctionalProgram(TyRoot root, StringBuffer buffer) {
		buffer.append(symbolName(root));
	}


	@Override
	public TyTerm copy() {
		return new TySymbol(name_, type_);
	}
	
	@Override
	public TyTerm copy(Map<Integer, TyAbst> abstMap){
		return copy();
	}

	@Override
	public String toString(){
		return name_+"";
	}
	
	@Override
	public void toString(StringBuffer buffer){
		buffer.append(name_);
	}

	@Override
	public String toStringWithType() {
		return name_+":["+type_+"]";
	}

	@Override
	public void splitedTerm(List<TyTerm> terms) {
		terms.add(this);
	}

	@Override
	public CheapTerm cheapCopy(List<String> symbolNames) {
		return new CheapSymbol(symbolNames.get(-1-name_));
	}

	@Override
	public TyTerm copyWithAbsts(List<TyAbst> abstList) {
		return copy();
	}

	@Override
	public TyTerm copyWithAbsts(List<TyAbst> abstList,
			Map<Integer, TyAbst> abstMap) {
		return copy();
	}

	@Override
	public boolean isWellBinded() {
		return true;
	}

	@Override
	public boolean isWellBinded(Set<TyAbst> absts) {
		return true;
	}

	@Override
	public boolean isWellTyped() {
		return true;
	}

}
