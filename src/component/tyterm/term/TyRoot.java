package component.tyterm.term;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import util.MyUtil;

import component.tyterm.type.Fun;
import component.tyterm.type.O;
import component.tyterm.type.Type;
import compressor.HOCompressor;



public final class TyRoot extends TyAbst{

	private class SymbolKey{
		String name_;
		Type type_;
		SymbolKey(String name, Type type){
			name_ = name;
			type_ = type;
		}
		@Override
		public boolean equals(Object obj){
			if(!(obj instanceof SymbolKey)) return false;
			SymbolKey sk = (SymbolKey)obj;
			return this.type_.equals(sk.type_) && this.name_.equals(sk.name_);
		}
		@Override
		public int hashCode(){
			return MyUtil.makeHashCode(new Object[]{ name_, type_ });
		}
	}
	private static final String NAME = "_ROOT_";
	private static final Type TYPE = O.getO();
	private List<Type> symbolTypes_ = new ArrayList<Type>();
	private LinkedHashMap<SymbolKey, Integer> symbolMap_ = new LinkedHashMap<SymbolKey, Integer>();//入力順を覚える
	private List<String> symbolNames_ = new ArrayList<String>();
	
	private List<TyAbst> abstsDecoded_;

	private TyRoot(String name, Type type) {
		super(false, type);
	}
	
	@Override
	public String toString(){
		return NAME + "." + child_.toString();
	}

	public static TyRoot getRoot(TyTerm tyterm){
		TyRoot root = new TyRoot(NAME, TYPE);
		root.setChild(tyterm);
		return root;
	}
	
	/**
	 * 
	 * @param rankNames 各indexはrankを示し、中身のList<String>がそのrankのシンボル名のリストを表す
	 */
	public void addSymbolTypes(List<List<String>> rankNames){
		for(int rank=0; rank < rankNames.size(); rank++){
			Type type = typeOf(rank);
			for(String name : rankNames.get(rank)){
				addSymbolType(name, type);
			}
		}
	}
	
	private Type typeOf(int rank){
		if(rank < 1) return O.O;
		return new Fun(O.O, typeOf(rank-1));
	}
	
	public int addSymbolType(String name, Type type){
		SymbolKey sk = new SymbolKey(name, type);
		if(symbolMap_.containsKey(sk)) return symbolMap_.get(sk);

		if(!(symbolMap_.size()==symbolTypes_.size() && symbolTypes_.size()==symbolNames_.size()))
			throw new IllegalArgumentException("something wrong!");
		int id = -1 - symbolMap_.size();
		symbolMap_.put(sk, id);
		symbolNames_.add(name);
		symbolTypes_.add(type);
		return id;
	}
	
	public int getIntNameOf(String name, Type type){
		SymbolKey sk = new SymbolKey(name, type);
		if(!symbolMap_.containsKey(sk)) throw new IllegalArgumentException();
		return symbolMap_.get(sk);
	}

	public List<Type> getSymbolTypes(){
		return symbolTypes_;
	}
	
	public List<TySymbol> getSymbols(){
		List<TySymbol> symbols = new ArrayList<>();
		for(Type type : symbolTypes_){
			symbols.add(new TySymbol(-1 - symbols.size(), type));
		}
		return symbols;
	}
	
	/**
	 * 
	 * @param kindMap
	 */
	public void updateSymbolTypes(){
		Type[] newTypes = new Type[symbolTypes_.size()];
		substituteTypes(child_, newTypes);
		if(newTypes.length != symbolTypes_.size()) throw new IllegalArgumentException("different size!");
		symbolTypes_ = new ArrayList<Type>();
		for(Type type : newTypes){
			symbolTypes_.add(type);
		}
	}

	//TyIdならidTypeMapの型を、それ以外なら型付け規則から導かれる方をセットする
	private void substituteTypes(TyTerm term, Type[] newTypes){
		try{
			if(term instanceof TyApp){
				TyApp app = (TyApp)term;
				TyTerm left = app.getLeftChild();
				TyTerm right = app.getRightChild();
				substituteTypes(left, newTypes);
				substituteTypes(right, newTypes);
				if(!(left.getType() instanceof Fun) || !((Fun)left.getType()).getLeft().equals(right.getType()))
					throw new IllegalArgumentException("type does not match!	left.type:"+left.getType()+"	right.type:"+right.getType()+"	left:"+left+"	right:"+right);
				app.setType(((Fun)left.getType()).getRight());
			}else if(term instanceof TyAbst){
				TyAbst abst = (TyAbst)term;
				TyTerm child = abst.getChild();
				substituteTypes(child, newTypes);
				Type varType = MyUtil.topOfSet(abst.getVars()).getType();//置き換え済みの型
				Type childType = child.getType();
				abst.setType(new Fun(varType, childType));
			}else if(term instanceof TyVar){
			}else if(term instanceof TySymbol){
				TySymbol symbol = (TySymbol)term;
				newTypes[-1 - symbol.getName()] = symbol.getType();//実装に依存している！！！！！
			}
		}catch(Exception e){
			System.err.println("substituteTypes:	term.class:"+term.getClass());
			System.err.println("substituteTypes:	term:"+term);
			throw e;
		}
	}
	
	public List<String> getSymbolNames(){
		return symbolNames_;
	}
	
	public String getSymbolName(TySymbol symbol){
		return symbolNames_.get(-1 - symbol.getName());
	}
	
	public int getSymbolId(TySymbol symbol){
		String name = getSymbolName(symbol);
		return -1 - symbolMap_.get(new SymbolKey(name, symbol.getType()));
	}
	
	public String getAllSymbolTypesAsString(){
		String str = "";
		for(int i=0; i < symbolNames_.size(); i++){
			str += symbolNames_.get(i) + "	:"+symbolTypes_.get(i) +"\n";
		}
		return str;
	}
	
	public Set<Integer> obtainAllSymbolKinds(int border){
		Map<Integer, Integer> kinds = new HashMap<Integer, Integer>();
		for(Type type : symbolTypes_){
			type.obtainAllKinds(kinds);
		}
		Set<Integer> kindSet = new HashSet<Integer>();
		for(Integer kind : kinds.keySet()){
			if(kinds.get(kind) > border) kindSet.add(kind);
		}
		return kindSet;
	}

	public String getAllVarTypesAsString(){
		boolean onlyHO = HOCompressor.PRINT_ONLY_HO_PATTERN;
		String str = "";
		int index = 0;
		str += "\nSYMBOL:["+symbolMap_.size()+"]\n";
		for(SymbolKey sk : symbolMap_.keySet()){
			boolean hoCond = !onlyHO || symbolTypes_.get(index).isHO();
			if(hoCond) str += "["+index+"]\t"+sk.name_+":	"+symbolTypes_.get(index)+System.getProperty("line.separator");
			index++;
		}
		List<TyAbst> absts = new ArrayList<TyAbst>();
		//getAllAbstChildren(absts);
		obtainAllAbsts(child_, absts);
		str += "\nABST:\n";
		for(TyAbst abst : absts){
			boolean hoCond = !onlyHO || abst.getArgType().isHO();
			if(hoCond) str += "λ"+abst.idStr()+":	"+/*((Fun)abst.getType()).getLeft()*/abst.getType()+System.getProperty("line.separator");
		}
		return str;
	}
	
	public List<TyAbst> getAllAbstChildren(){
		List<TyAbst> all = new ArrayList<TyAbst>();
		getAllAbstChildren(all);
		return all;
	}
	
	private static void obtainAllAbsts(TyTerm term, Collection<TyAbst> absts){
		if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			obtainAllAbsts(app.getLeftChild(), absts);
			obtainAllAbsts(app.getRightChild(), absts);
			return;
		}else if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			absts.add(abst);
			obtainAllAbsts(abst.getChild(), absts);
			return;
		}
		return;
	}
	
	public List<TyAbst> getAllAbstChildrenByTraverseAll(){
		List<TyAbst> all = new ArrayList<TyAbst>();
		gatherAbstsByTraverseAll(child_, all);
		return all;
	}
	private static void gatherAbstsByTraverseAll(TyTerm term, List<TyAbst> absts){
		if(term instanceof TyApp){
			TyApp app =(TyApp)term;
			gatherAbstsByTraverseAll(app.getLeftChild(), absts);
			gatherAbstsByTraverseAll(app.getRightChild(), absts);
		}else if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			absts.add(abst);
			gatherAbstsByTraverseAll(abst.getChild(), absts);
		}
	}


	@Override
	public int labelSize(){
		return child_.labelSize();
	}

	@Override
	public int depth() {
		return 0;
	}
	
	public void addAbstDecoded(TyAbst abst){
		if(abstsDecoded_==null) abstsDecoded_ = new ArrayList<TyAbst>();
		abstsDecoded_.add(abst);
	}
	public List<TyAbst> getAbstsDecoded(){
		return abstsDecoded_;
	}

	
	public void printTermInfo(){
		System.out.println("TyRoot#printTermInfo:{");
		for(SymbolKey sk : symbolMap_.keySet()){
			System.out.println("	"+sk.name_+":"+sk.type_);
		}
		System.out.println("}");
	}
}
