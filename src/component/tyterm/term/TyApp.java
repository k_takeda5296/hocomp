package component.tyterm.term;


import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import component.cheapterm.CheapApp;
import component.cheapterm.CheapTerm;
import component.context.Context;
import component.context.ContextFactory;
import component.context.HasContexts;
import component.tyterm.TyTermIterFunc;
import component.tyterm.type.Fun;
import component.tyterm.type.Type;


public final class TyApp extends AbstractTyTerm implements HasContexts{

	private TyTerm leftChild_;
	private TyTerm rightChild_;
	
	private Collection<Context> contextSet_;
	private boolean isNewContexts_;
	
	private static int ID = 0;
	private int id_;

	public int getId(){
		return id_;
	}
	
	public TyApp(TyTerm leftChild, TyTerm rightChild, Type type){
		leftChild_ = leftChild;leftChild.setParent(this);
		rightChild_ = rightChild;rightChild.setParent(this);
		type_ = type;
		id_ = ID++;
	}
	
	public TyApp(TyTerm leftChild, TyTerm rightChild){
		this(leftChild, rightChild, ((Fun)leftChild.getType()).getRight());
		assert(leftChild.getType() instanceof Fun);
		Fun fun = (Fun)leftChild.getType();
		assert fun.getLeft().equals(rightChild.getType()) : "left.type:"+leftChild.getType()+"	right.type:"+rightChild.getType();
	}

	public void setLeftChild(TyTerm tyterm) {
		leftChild_ = tyterm;
	}

	public TyTerm getLeftChild() {
		return leftChild_;
	}

	public void setRightChild(TyTerm tyterm){
		rightChild_ = tyterm;
	}

	public TyTerm getRightChild() {
		return rightChild_;
	}

	@Override
	public void replaceChild(TyTerm oldChild, TyTerm newChild) {
		if(leftChild_==oldChild){
			leftChild_ = newChild;
			return;
		}else if(rightChild_==oldChild){
			rightChild_ = newChild;
			return;
		}
		throw new IllegalArgumentException("TyApp#replaceChild:	not my child!	this:"+this+"	leftChild:"+leftChild_+"	rightChild:"+rightChild_+"	oldChild:"+oldChild+"	newChild:"+newChild);
	}

	@Override
	public boolean isSame(TyTerm tyterm) {
		if(!(tyterm instanceof TyApp)) return false;
		TyApp app = (TyApp)tyterm;
		return this.leftChild_.isSame(app.leftChild_) && this.rightChild_.isSame(app.rightChild_);
	}

	@Override
	public boolean isSame(TyTerm tyterm, Map<TyAbst, TyAbst> abstPairs) {
		if(!(tyterm instanceof TyApp)) return false;
		TyApp app = (TyApp)tyterm;
		return this.leftChild_.isSame(app.leftChild_, abstPairs) && this.rightChild_.isSame(app.rightChild_, abstPairs);
	}

	@Override
	public void iterPostOreder(TyTermIterFunc iterFunc) {
		leftChild_.iterPostOreder(iterFunc);
		rightChild_.iterPostOreder(iterFunc);
		iterFunc.iterFunc(this);
	}

	@Override
	public Set<TyTerm> getPosterity() {
		Set<TyTerm> posterity = new LinkedHashSet<TyTerm>();
		leftChild_.getPosterity(posterity);
		rightChild_.getPosterity(posterity);
		return posterity;
	}

	@Override
	public void getPosterity(Set<TyTerm> posterity) {
		leftChild_.getPosterity(posterity);
		rightChild_.getPosterity(posterity);
		posterity.add(this);
	}

	@Override
	public String checkBidrectional() {
		String str = leftChild_.getParent()==this ? "" : "ERROR:TyApp	this:"+this+"	left:"+leftChild_+"	left.parent:"+leftChild_.getParent() + System.getProperty("line.separator");
		str += rightChild_.getParent()==this ? "" : "ERROR:TyApp	this:"+this+"	right:"+rightChild_+"	right.parent:"+rightChild_.getParent() + System.getProperty("line.separator");
		str += leftChild_.checkBidrectional() + rightChild_.checkBidrectional();
		return str;
	}

	@Override
	public int size() {
		return leftChild_.size() + rightChild_.size() + 1;
	}

	@Override
	public int labelSize() {
		return leftChild_.labelSize() + rightChild_.labelSize();
	}

	@Override
	public String toStringAsFunctionalProgram(TyRoot root) {
		StringBuffer buffer = new  StringBuffer();
		toStringAsFunctionalProgram(root, buffer);
		return buffer.toString();
	}

	@Override
	public void toStringAsFunctionalProgram(TyRoot root, StringBuffer buffer) {
		if(leftChild_ instanceof TyAbst){
			TyAbst abst = (TyAbst)leftChild_;
			buffer.append("let ");
			buffer.append(abst.idStr());
			buffer.append(" = ");
			rightChild_.toStringAsFunctionalProgram(root, buffer);
			buffer.append(" in");
			buffer.append(System.getProperty("line.separator"));
			abst.getChild().toStringAsFunctionalProgram(root, buffer);
			return;
		}
		boolean isAbstLeft = leftChild_ instanceof TyAbst;
		if(isAbstLeft) buffer.append("(");
		leftChild_.toStringAsFunctionalProgram(root, buffer);
		if(isAbstLeft) buffer.append(")");
		buffer.append(" ");
		boolean isIdRight = rightChild_ instanceof TySymbol || rightChild_ instanceof TyVar;
		if(!isIdRight) buffer.append("(");
		rightChild_.toStringAsFunctionalProgram(root, buffer);
		if(!isIdRight) buffer.append(")");
		return;
	}


	@Override
	public void toString(StringBuffer buffer){
		boolean isAbstLeft = leftChild_ instanceof TyAbst;
		if(isAbstLeft) buffer.append("(");
		leftChild_.toString(buffer);
		if(isAbstLeft) buffer.append(")");
		buffer.append(" ");
		boolean isIdRight = rightChild_ instanceof TySymbol || rightChild_ instanceof TyVar;
		if(!isIdRight) buffer.append("(");
		rightChild_.toString(buffer);
		if(!isIdRight) buffer.append(")");
	}

	@Override
	public String toString(){
		StringBuffer buffer = new StringBuffer();
		toString(buffer);
		return "["+id_+"]"+buffer.toString();
	}

	@Override
	public String toStringWithType() {
		boolean isAbstLeft = leftChild_ instanceof TyAbst;
		String l = leftChild_.toStringWithType();
		String left = isAbstLeft ? "("+l+")" : l; 
		boolean isIdRight = rightChild_ instanceof TySymbol || rightChild_ instanceof TyVar;
		String r = rightChild_.toStringWithType();
		String right = isIdRight ? r : "("+r+")";
		return left+" "+right+":["+type_+"]";
	}

	@Override
	public void splitedTerm(List<TyTerm> terms) {
		leftChild_.splitedTerm(terms);
		terms.add(rightChild_);
	}

	@Override
	public TyTerm copy() {
		TyTerm left = leftChild_.copy();
		TyTerm right = rightChild_.copy();
		TyApp app = new TyApp(left, right, type_);
		left.setParent(app);
		right.setParent(app);
		return app;
	}
	
	@Override
	public TyTerm copy(Map<Integer, TyAbst> abstMap) {
		TyTerm left = leftChild_.copy(abstMap);
		TyTerm right = rightChild_.copy(abstMap);
		TyApp app = new TyApp(left, right, type_);
		left.setParent(app);
		right.setParent(app);
		return app;
	}

	@Override
	public CheapTerm cheapCopy(List<String> symbolNames) {
		return new CheapApp(leftChild_.cheapCopy(symbolNames), rightChild_.cheapCopy(symbolNames));
	}

	@Override
	public TyTerm copyWithAbsts(List<TyAbst> abstList) {
		TyTerm left = leftChild_.copyWithAbsts(abstList);
		TyTerm right = rightChild_.copyWithAbsts(abstList);
		TyApp app = new TyApp(left, right, type_);
		left.setParent(app);
		right.setParent(app);
		return app;
	}

	@Override
	public TyTerm copyWithAbsts(List<TyAbst> abstList,
			Map<Integer, TyAbst> abstMap) {
		TyTerm left = leftChild_.copyWithAbsts(abstList, abstMap);
		TyTerm right = rightChild_.copyWithAbsts(abstList, abstMap);
		TyApp app = new TyApp(left, right, type_);
		left.setParent(app);
		right.setParent(app);
		return app;
	}

	@Override
	public boolean isWellBinded() {
		return leftChild_.isWellBinded() && rightChild_.isWellBinded();
	}

	@Override
	public boolean isWellBinded(Set<TyAbst> absts) {
		return leftChild_.isWellBinded(absts) && rightChild_.isWellBinded(absts);
	}

	@Override
	public boolean isWellTyped() {
		if(!(leftChild_.getType() instanceof Fun)) throw new IllegalArgumentException();
		Fun leftType = (Fun)leftChild_.getType();
		return leftType.getLeft().equals(rightChild_.getType()) && leftType.getRight().equals(type_) 
				&& leftChild_.isWellTyped() && rightChild_.isWellTyped();
	}

	/*
	 * 以下はHasContextsのメソッド
	 */

	@Override
	public void countContext(int contextSize) {
		/*
		 * 問題：
		 * オーバーラップする文脈を抜き出した時に、Tables#update内でoccSetからノードを一つずつ削除するが、
		 * 削除の順序が各segmentにおいて必ず端からにならなければいけない
		 * 
		 * 解決案：
		 * 1：TablesにおいてpopしたoccSetはupdate時に参照されないように破棄する（popされた文脈の出現は必ず1回になるので問題ない）
		 * ２：アルゴリズムを通して削除の順序が端からになるようにする。（すなわち、nodesToRemove,nodesToUpdateに入る順番が端からになるようにする）
		 * ３、細かい操作をおこなう（各出現について、再利用したノードとそうでないノードを分けて特別処理）
		 * 
		 * とりあえず３でいく。つまり以下の２行は無駄
		 * １の具体的な方法：
		 * popしたoccSetへの参照をすべて除く。
		 * 再利用された出現の@ノードはnodesToUpdateに入れる（[]1 ([]1 []2)を考慮する必要がある）
		 * 
		 * ３の具体的な方法：
		 * popされたoccSetは破棄（再利用したノードの更新時には新しくoccSetが作られるようにする（これはcontextHashかり除いておくだけで実現できる）
		 * 再利用したノードの文脈集合をクリアしたのち、nodesToUpdateに追加（変数になった部分があるため新しい文脈を持つため（抜き出されえないが（xを含む文脈が他に無いため）））
		 * （クリアする必要は無く、文脈集合からの属だけで良いか
		 * 再利用されなかったノードのnodesToRemoveにおいては破棄されたoccSetからのremoveは行わない（各ノードの文脈集合から除いておく）
		 * →a(a []1)を抜き出したときに[]1 ([]1 []2)からノードを削除するときにsegmentで同様の問題が起こる。
		 * →さらなる解決策：
		 * １、ノードの削除時に子供がその文脈を持っているかをチェックし、持っていればそちらを先に除外する。
		 * ２、同値穴を持つ文脈を考慮しない
		 * →まず２をやってみる（ContextFacotryで生成しないようにする）
		 */
		contextSet_ = ContextFactory.makeContexts(this, contextSize);
		//System.out.println("TyApp#countContext["+id_+"]	contexts.size:"+contextSet_.size());
		isNewContexts_ = true;
	}

	@Override
	public Collection<Context> getContexts(){
		return contextSet_;
	}
	
	@Override
	public void removeContext(Context c){
		if(contextSet_==null || !contextSet_.contains(c)) throw new IllegalArgumentException("doesn't have context:"+c);
		contextSet_.remove(c);
	}

	@Override
	public final void invalidateContexts(){
		isNewContexts_ = false;
	}
	
	@Override
	public final boolean hasInvalidatedContexts(){
		return !isNewContexts_;
	}

}
