package component.tyterm.term;


import java.util.List;
import java.util.Map;
import java.util.Set;

import component.cheapterm.CheapTerm;
import component.tyterm.TyTermIterFunc;
import component.tyterm.type.Type;


public interface TyTerm{
	
	Type getType();
	void setType(Type type);

	void setParent(TyTerm parent);
	TyTerm getParent();
	void replaceChild(TyTerm oldChild, TyTerm newChild);
	
	boolean isSame(TyTerm tyterm);
	boolean isSame(TyTerm tyterm, Map<TyAbst, TyAbst> abstPairs);//CAbstを使わないので使われていない．

	void iterPostOreder(TyTermIterFunc iterFunc);
	
	Set<TyTerm> getAncestors(int num);
	void getAncestors(int num, Set<TyTerm> ancestors);
	
	Set<TyTerm> getPosterity();//自身を含まない子孫.いちいちposterityをゲットしてからcontainで判定している所があるが、LTerm自体にcontainsを実装した方が良い？
	void getPosterity(Set<TyTerm> posterity);

	String checkBidrectional();
	int size();
	int labelSize();
	
	String toStringAsFunctionalProgram(TyRoot root);
	void toStringAsFunctionalProgram(TyRoot root, StringBuffer buffer);
	void toString(StringBuffer buffer);
	String toStringWithType();
	
	TyTerm copy();
	TyTerm copy(Map<Integer, TyAbst> abstMap);
	CheapTerm cheapCopy(List<String> symbolNames);
	
	//エンコーディングで使用
	void splitedTerm(List<TyTerm> terms);
	TyTerm copyWithAbsts(List<TyAbst> abstList);
	TyTerm copyWithAbsts(List<TyAbst> abstList, Map<Integer, TyAbst> abstMap);
	boolean isWellBinded();
	boolean isWellBinded(Set<TyAbst> absts);
	boolean isWellTyped();
}
