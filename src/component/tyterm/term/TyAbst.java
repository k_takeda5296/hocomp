package component.tyterm.term;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import component.cheapterm.CheapAbst;
import component.cheapterm.CheapTerm;
import component.tyterm.TyTermIterFunc;
import component.tyterm.type.Fun;
import component.tyterm.type.Type;


public class TyAbst extends AbstractTyTerm{
	
	protected int id_;
	protected boolean fx_;
	protected TyTerm child_;
	private Set<TyVar> vars_;
	protected TyAbst abstParent_;
	protected List<TyAbst> abstChildren_;//参照ごとの順序が保たれるようにリストを用いている
	
	//0を使わない
	public static int F_ID = 0;
	public static int X_ID = 0;
	
	public static void init(){
		F_ID = 0;
		X_ID = 0;
	}
	
	public TyAbst(boolean fx, Type type){
		this(fx, fx ? 2*(F_ID++)+1 : 2*(X_ID++), type);//f側は奇数、x側は偶数
	}
	public TyAbst(boolean fx, int id, Type type){
		if(id==4955) System.out.println("TyAbst##	f4955 instantiated!!");
		fx_ = fx;
		id_ = id;
		type_ = type;
		vars_ = new LinkedHashSet<TyVar>();
		abstChildren_ = new ArrayList<TyAbst>();
	}
	
	public int getName(){
		return id_;
	}
	
	int getId(){
		return id_;
	}
	boolean getFX(){
		return fx_;
	}

	
	public Type getArgType(){
		return ((Fun)type_).getLeft();
	}

	public void setChild(TyTerm child){
		child_ = child;
	}

	public TyTerm getChild(){
		return child_;
	}

	public TyVar makeVar() {
		TyVar newVar = new TyVar(this);
		vars_.add(newVar);
		return newVar;
	}

	public Set<TyVar> getVars() {
		return vars_;
	}

	public void setAbstParent(TyAbst abst){
		abstParent_ = abst;
	}

	public TyAbst getAbstParent(){
		return abstParent_;
	}

	public void addAbstChild(TyAbst abst) {
		abstChildren_.add(abst);
	}

	public void addAbstChildren(Collection<TyAbst> absts){
		abstChildren_.addAll(absts);
	}

	public void removeAbstChild(TyAbst abst){
		abstChildren_.remove(abst);
	}

	public void removeAbstChildren(Collection<TyAbst> absts){
		abstChildren_.removeAll(absts);
	}

	public List<TyAbst> getAbstChildren(){
		return abstChildren_;
	}

	public void getAllAbstChildren(List<TyAbst> absts) {
		absts.addAll(abstChildren_);
		for(TyAbst abst : abstChildren_){
			abst.getAllAbstChildren(absts);
		}
	}

	public int depth() {
		int depth = 0;
		TyAbst parent = abstParent_;
		while(!(parent instanceof TyRoot)){
			depth++;
			parent = parent.getAbstParent();
		}
		return depth;
	}

	@Override
	public void replaceChild(TyTerm oldChild, TyTerm newChild) {
		if(oldChild != child_) throw new IllegalArgumentException("TyAbst#replaceChild:	not my child!	child:"+child_+"	oldChild:"+oldChild+"	newChild:"+newChild);
		child_ = newChild;
	}

	@Override
	public boolean isSame(TyTerm tyterm) {
		if(!(tyterm instanceof TyAbst)) return false;
		TyAbst abst = (TyAbst)tyterm;
		Map<TyAbst, TyAbst> abstPairs = new HashMap<TyAbst, TyAbst>();
		abstPairs.put(this, abst);
		abstPairs.put(abst, this);//いらない
		return child_.isSame(abst.child_, abstPairs);
	}

	@Override
	public boolean isSame(TyTerm tyterm, Map<TyAbst, TyAbst> abstPairs) {
		if(!(tyterm instanceof TyAbst)) return false;
		TyAbst abst = (TyAbst)tyterm;
		abstPairs.put(this, abst);
		abstPairs.put(abst, this);//いらない
		return child_.isSame(abst.child_, abstPairs);
	}

	@Override
	public void iterPostOreder(TyTermIterFunc iterFunc) {
		child_.iterPostOreder(iterFunc);
		iterFunc.iterFunc(this);
	}

	@Override
	public Set<TyTerm> getPosterity() {
		Set<TyTerm> posterity = new LinkedHashSet<TyTerm>();
		child_.getPosterity(posterity);
		return posterity;
	}

	@Override
	public void getPosterity(Set<TyTerm> posterity) {
		child_.getPosterity(posterity);
		posterity.add(this);
	}

	@Override
	public String checkBidrectional() {
		String str = child_.getParent()==this ? "" : "ERROR:TyAbst	this:"+this+"	child:"+child_+"	child.parent:"+child_.getParent() + System.getProperty("line.separator");
		return child_.checkBidrectional() + str;
	}

	@Override
	public int size() {
		return child_.size() + 1;
	}

	@Override
	public int labelSize() {
		return child_.labelSize() + 1;
	}

	@Override
	public String toStringAsFunctionalProgram(TyRoot root) {
		StringBuffer buffer = new StringBuffer();
		toStringAsFunctionalProgram(root, buffer);
		return buffer.toString();
	}
	
	public String idStr(){
		return (fx_ ? "f" : "x") + String.format("%d", id_);
	}
	
	@Override
	public void toStringAsFunctionalProgram(TyRoot root, StringBuffer buffer) {
		buffer.append("λ");
		buffer.append(idStr());
		buffer.append(".");
		child_.toStringAsFunctionalProgram(root, buffer);
	}


	@Override
	public String toString(){
		StringBuffer buffer = new StringBuffer();
		toString(buffer);
		return buffer.toString();
	}
	
	@Override
	public void toString(StringBuffer buffer) {
		buffer.append("λ");
		buffer.append(idStr());
		buffer.append(".");
		child_.toString(buffer);
	}

	@Override
	public String toStringWithType() {
		return "λ"+(fx_ ? "f" : "x")+id_+":["+type_+"]."+child_.toStringWithType();
	}

	@Override
	public void splitedTerm(List<TyTerm> terms) {
		terms.add(this);
	}

	@Override
	public TyTerm copy() {
		Map<Integer, TyAbst> abstMap = new LinkedHashMap<Integer, TyAbst>();
		TyAbst abst = new TyAbst(fx_, type_);
		abstMap.put(getName(), abst);
		TyTerm child = child_.copy(abstMap);
		abst.setChild(child);
		child.setParent(abst);
		return abst;
	}
	
	@Override
	public TyTerm copy(Map<Integer, TyAbst> abstMap){
		TyAbst abst = new TyAbst(fx_, type_);
		abstMap.put(getName(), abst);
		TyTerm child = child_.copy(abstMap);
		abst.setChild(child);
		child.setParent(abst);
		return abst;
	}

	@Override
	public CheapTerm cheapCopy(List<String> symbolNames) {
		return new CheapAbst(idStr(), child_.cheapCopy(symbolNames));
	}

	public boolean isReducible() {
		return parent_!=null && parent_ instanceof TyApp && ((TyApp)parent_).getLeftChild()==this;
	}

	public TyTerm getArg() {
		if(!isReducible()) throw new IllegalArgumentException("this is not reducible!");
		return ((TyApp)parent_).getRightChild();
	}

	@Override
	public TyTerm copyWithAbsts(List<TyAbst> abstList) {
		Map<Integer, TyAbst> abstMap = new LinkedHashMap<Integer, TyAbst>();
		TyAbst abst = new TyAbst(fx_, id_, type_);
		abstMap.put(getName(), abst);
		TyTerm child = child_.copyWithAbsts(abstList, abstMap);
		abst.setChild(child);
		child.setParent(abst);
		abstList.add(abst);
		return abst;
	}

	@Override
	public TyTerm copyWithAbsts(List<TyAbst> abstList,
			Map<Integer, TyAbst> abstMap) {
		TyAbst abst = new TyAbst(fx_, id_, type_);
		abstMap.put(getName(), abst);
		TyTerm child = child_.copyWithAbsts(abstList, abstMap);
		abst.setChild(child);
		child.setParent(abst);
		abstList.add(abst);
		return abst;
	}

	@Override
	public boolean isWellBinded() {
		Set<TyAbst> absts = new HashSet<TyAbst>();
		absts.add(this);
		return child_.isWellBinded(absts);
	}

	@Override
	public boolean isWellBinded(Set<TyAbst> absts) {
		absts.add(this);
		if(child_.isWellBinded(absts)){
			absts.remove(this);
			return true;
		}
		return false;
	}

	@Override
	public boolean isWellTyped() {
		if(!(type_ instanceof Fun)){
			System.err.println("type:"+type_+"	name:"+idStr());
			throw new IllegalArgumentException();
		}
		Fun type = (Fun)type_;
		return type.getRight().equals(child_.getType());
	}

}
