package component.tyterm.type;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import util.MyUtil;

public class TypeBitConverter {
	
	
	public static byte[] typeList2ByteList(List<Type> typeList){
		List<Boolean> bitList = typeList2BitList(typeList);
		//8bitに漏れた分をtrueで埋める
		int remainBits = 8 - bitList.size() % 8; 
		for(int i=0; i < remainBits; i++){
			bitList.add(true);//trueを入れておくことで、型の個数を覚えておく必要が無くなる(decode時に簡約できないビットを読み捨てる)
		}
		return MyUtil.boolList2ByteArray(bitList);
	}

	public static List<Boolean> typeList2BitList(List<Type> typeList){
		//typeListをbitsに変換
		List<Boolean> bitList = new ArrayList<Boolean>();
		for(Type type : typeList){
			bitList.addAll(type.getBits());
		}
		return bitList;
	}

	public static Type bits2Type(List<Boolean> bits){
		boolean bit = bits.remove(0);
		return bit ? new Fun(bits2Type(bits), bits2Type(bits)) : O.O;//副作用が気持ち悪い書き方
	}

	public static Type bytes2Type(List<Byte> bytes){
		return bytes2Type(bytes, false);
	}

	private static Type bytes2Type(List<Byte> bytes, boolean dummy){//
		byte b = bytes.remove(bytes.size()-1);
		if(b < 1 || 3 < b) throw new IllegalArgumentException("invalid bytes!");
		if(b==1) return O.O;
		if(b==2){
			Type left = bytes2Type(bytes, false);
			Type right = bytes2Type(bytes, false);
			return new Fun(left, right);
		}
		if(b==3){
			byte b0 = bytes.remove(bytes.size()-1);
			byte b1 = bytes.remove(bytes.size()-1);
			byte b2 = bytes.remove(bytes.size()-1);
			byte b3 = bytes.remove(bytes.size()-1);
			ByteBuffer.allocate(4);
			ByteBuffer buffer = ByteBuffer.wrap(new byte[]{ b0, b1, b2, b3 });
			int id = buffer.getInt();
			return Kind.getKind(id - 4);
		}
		throw new IllegalArgumentException("cannot reachable!");
	}

	public static List<Byte> type2ByteList(Type type){
		List<Byte> bytes = new ArrayList<Byte>();
		type.obtainByteCode(bytes);
		return bytes;
	}
}
