package component.tyterm.type.subdivider;

import java.util.Map;

import component.tyterm.type.Kind;
import component.tyterm.type.Type;

/**
 * elementをKind型, representativeをType型としたUF
 * kindの取り得るidの最大値nに対してlog(n)で動かしたい
 * @author koutaro_t
 *
 */
interface TypeUF {

	Type find(Kind kind);
	void union(Kind kind, Type type);
	Map<Kind, Type> toKindMap();
	void print();
}
