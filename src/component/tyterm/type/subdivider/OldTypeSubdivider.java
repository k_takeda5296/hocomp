package component.tyterm.type.subdivider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import util.TimeMeasurer;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyId;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TyTerm;
import component.tyterm.type.Fun;
import component.tyterm.type.Kind;
import component.tyterm.type.Type;

public class OldTypeSubdivider {

	public static void subdivideTypes(TyRoot root){
		/*
		 * 圧縮とその時間計測
		 */
		TimeMeasurer tm = new TimeMeasurer();
		tm.start();


		Map<Integer, Kind> idTypeMap = initTypes(root.getChild(), new HashMap<Integer, Kind>());
		List<C> constraints = new ArrayList<C>();
		gatherConstraints(root.getChild(), constraints);

		tm.end();
		tm.print("SUB0");


		tm.start();
		
		List<S> substitutions = new ArrayList<S>();
		unify(constraints, substitutions);


		tm.end();
		tm.print("SUB1");
		
		//System.out.println("substitutions:"+substitutions.size());
		Map<Kind, Type> typeMap = substitutedIdTypeMap(substitutions, idTypeMap);
		substituteAll(root.getChild(), typeMap);
		//System.out.println("newTypes:	"+Arrays.toString(newTypes));
		root.updateSymbolTypes();
	}


	private static Map<Integer, Kind> initTypes(TyTerm term, Map<Integer, Kind> idTypeMap){
		if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			TyTerm left = app.getLeftChild();
			TyTerm right = app.getRightChild();
			initTypes(left, idTypeMap);
			initTypes(right, idTypeMap);
			Kind newType = Kind.makeNewKind();
			app.setType(newType);
		}else if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			TyTerm child = abst.getChild();
			initTypes(child, idTypeMap);
			Kind varKind = idTypeMap.get(abst.getName());//childのinitTypesで必ず型付けされている
			Type newType = new Fun(varKind, child.getType());
			abst.setType(newType);
		}else if(term instanceof TyId){
			TyId id = (TyId)term;
			if(idTypeMap.containsKey(id.getName())){
				id.setType(idTypeMap.get(id.getName()));
			}else{
				Kind newType = Kind.makeNewKind();
				idTypeMap.put(id.getName(), newType);
				id.setType(newType);
			}
		}
		return idTypeMap;
	}

	private static void gatherConstraints(TyTerm term, List<C> constraints){
		if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			TyTerm left = app.getLeftChild();
			TyTerm right = app.getRightChild();
			gatherConstraints(left, constraints);
			gatherConstraints(right, constraints);
			constraints.add(new C(left.getType(), new Fun(right.getType(), app.getType())));
		}else if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			TyTerm child = abst.getChild();
			gatherConstraints(child, constraints);
		}
	}

	private static void unify(List<C> constraints, List<S> substitutions){
		if(constraints.size() < 1) return;
		C constr = constraints.remove(constraints.size()-1);
		Type left = constr.getLeft();
		Type right = constr.getRight();
		if(left.equals(right)) {
			unify(constraints, substitutions);
			return;
		}else if(left instanceof Fun){
			if(right instanceof Fun){
				Fun lFun = (Fun)left;
				Fun rFun = (Fun)right;
				constraints.add(new C(lFun.getLeft(), rFun.getLeft()));
				constraints.add(new C(lFun.getRight(), rFun.getRight()));
				unify(constraints, substitutions);
				return;
			}else if(right instanceof Kind){
				Kind rKind = (Kind)right;
				S sub = new S(rKind, left);
				substitutions.add(sub);
				updateConstraints(constraints, sub);
				unify(constraints, substitutions);
				return;
			}
			throw new IllegalArgumentException("right should not be type O!");
		}else{
			Kind lKind = (Kind)left;
			boolean flag = right instanceof Fun || ((Kind)right).getId() < lKind.getId();
			S sub = flag ? new S(lKind, right) : new S((Kind)right, lKind);
			substitutions.add(sub);
			updateConstraints(constraints, sub);
			unify(constraints, substitutions);
			return;
		}
	}

	private static void updateConstraints(List<C> constraints, S sub){
		for(C c : constraints){
			c.substitute(sub);
		}
	}

	//TyIdに割り当てられた型から、それに代入列を適用した型へのマップを作る
	private static Map<Kind, Type> substitutedIdTypeMap(List<S> substitutions, Map<Integer, Kind> idTypeMap) {
		Map<Kind, Type> typeMap = new HashMap<Kind, Type>();
		for(Kind kind : idTypeMap.values()){
			Type type = substitutionsAppliedType(kind, substitutions);
			if(kind!=type) typeMap.put(kind, type);
		}
		return typeMap;
	}

	//型typeに代入列substitutionsを適用した型を返す
	private static Type substitutionsAppliedType(Type type, List<S> substitutions){
		for(S s : substitutions){
			type = substituteOne(type, s);
		}
		return type;
	}

	//型typeに代入sを適用した型を返す
	private static Type substituteOne(Type type, S s){
		if(type instanceof Fun){
			Fun fun = (Fun)type;
			Type left = substituteOne(fun.getLeft(), s);
			Type right = substituteOne(fun.getRight(), s);
			return new Fun(left, right);
		}else if(type instanceof Kind){
			Kind kind = (Kind)type;
			return kind.equals(s.getBefore()) ? s.getAfter() : kind;
		}
		throw new IllegalArgumentException("type O appears!");
	}

	private static class C{
		private Type left_;
		private Type right_;
		private Type getLeft(){
			return left_;
		}
		private Type getRight(){
			return right_;
		}
		private C(Type left, Type right){
			left_ = left;
			right_ = right;
		}
		private void substitute(S s){
			left_ = substituteOne(left_, s);
			right_ = substituteOne(right_, s);
		}
		@Override
		public String toString(){
			return left_+"="+right_;
		}
	}

	private static class S{
		private Kind before_;
		private Type after_;
		private Kind getBefore(){
			return before_;
		}
		private Type getAfter(){
			return after_;
		}
		private S(Kind before, Type after){
			before_ = before;
			after_ = after;
		}
		@Override
		public String toString(){
			return "["+before_+"=>"+after_+"]";
		}
	}
	
	private static void substituteAll(TyTerm term, Map<Kind, Type> kindMap){
		term.setType(substitute(term.getType(), kindMap));
		if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			substituteAll(app.getLeftChild(), kindMap);
			substituteAll(app.getRightChild(), kindMap);
		}else if(term instanceof TyAbst){
			substituteAll(((TyAbst)term).getChild(), kindMap);
		}
	}
	
	private static Type substitute(Type type, Map<Kind, Type> subs){
		if(type instanceof Kind){
			return subs.containsKey(type) ? substitute(subs.get(type), subs) : type;
		}else if(type instanceof Fun){
			Fun fun = (Fun)type;
			return new Fun(substitute(fun.getLeft(), subs), substitute(fun.getRight(), subs));
		}
		throw new IllegalArgumentException("O should have been replaced!");
	}
}
