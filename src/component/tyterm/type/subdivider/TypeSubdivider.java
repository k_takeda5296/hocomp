package component.tyterm.type.subdivider;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import util.TimeMeasurer;

import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyId;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TySymbol;
import component.tyterm.term.TyTerm;
import component.tyterm.term.TyVar;
import component.tyterm.type.Fun;
import component.tyterm.type.Kind;
import component.tyterm.type.Type;

public class TypeSubdivider {

	public static void subdivideTypes(TyRoot root){
		/*
		 * 圧縮とその時間計測
		 */


		TimeMeasurer tm = new TimeMeasurer();
		tm.start();


		Map<Integer, Type> idTypeMap = new LinkedHashMap<>();
		initTypes(root.getChild(), idTypeMap);

		tm.end();
		tm.print("SUB0");


		tm.start();

		/*
		 * unifyにより各KindのrepresentativeをMapで得る
		 */
		Map<Kind, Type> kindMap = new HashMap<>();
		W(idTypeMap, root.getChild(), kindMap);
		System.out.println("subdivideTyeps#	kindMap:"+kindMap);
		substituteAll(root.getChild(), kindMap);
		System.out.println("subdivideTyeps#	substtituetAll ended!");
		tm.end();
		tm.print("SUB1");

		root.updateSymbolTypes();
	}

	/**
	 * TySymbolにのみあらかじめ共通の型を割り当て、その割り当てをidTypeMapに格納する
	 * @param term
	 * @param idTypeMap
	 * @param kindId
	 * @return
	 */
	private static void initTypes(TyTerm term, Map<Integer, Type> idTypeMap){
		if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			TyTerm left = app.getLeftChild();
			TyTerm right = app.getRightChild();
			initTypes(left, idTypeMap);
			initTypes(right, idTypeMap);
			return;
		}else if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			TyTerm child = abst.getChild();
			initTypes(child, idTypeMap);
			return;
		}else if(term instanceof TySymbol){
			TySymbol symbol = (TySymbol)term;
			if(idTypeMap.containsKey(symbol.getName())){//すでに型付けしたidの場合はその型で型付け
				symbol.setType(idTypeMap.get(symbol.getName()));
			}else{
				Kind newType = Kind.makeNewKind();
				idTypeMap.put(symbol.getName(), newType);
				symbol.setType(newType);
			}
			if(symbol.getName()==-2) System.out.println("TypeSubdiv#initTypes	-2.kind:"+symbol.getType());
			return;
		}else if(term instanceof TyVar){
			return;
		}
		throw new IllegalArgumentException("No match class for the term:"+term.getClass());
	}
	
	/**
	 * アルゴリズムW。空の代入集合subsutitionsを受け取り、そこに新しい代入を入れて返る
	 * @param idTypeMap
	 * @param term
	 * @param substitutions
	 * @return
	 */
	private static Type W(Map<Integer, Type> idTypeMap, TyTerm term, Map<Kind, Type> subs){
		System.out.println("W##	term:"+term);
		if(subs.size() > 0) throw new IllegalArgumentException("subs should be empty!");
		if(term instanceof TyId){
			TyId id = (TyId)term;
			if(!idTypeMap.containsKey(id.getName())) throw new IllegalArgumentException("No type for the symbol!");
			Type type = idTypeMap.get(id.getName());
			id.setType(type);
			System.out.println("W##	term:"+term+"	id.retType:"+type);
			return type;
		}else if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			Map<Kind, Type> subs1 = new HashMap<>();
			Type t1 = W(idTypeMap, app.getLeftChild(), subs1);
			substituteMap(idTypeMap, subs1);//idTypeMapを更新しても問題ない？？？
			Map<Kind, Type> subs2 = new HashMap<>();
			Type t2 = W(idTypeMap, app.getRightChild(), subs2);
			Kind beta = Kind.makeNewKind();
			if(beta.getId()==45) System.out.println("W###	term:"+term);
			Map<Kind, Type> v = unify(substitute(t1, subs2), new Fun(t2, beta));
			subs.putAll(subs1);
			subs.putAll(subs2);
			subs.putAll(v);
			Type type = substitute(beta, v);
			System.out.println("W##	term:"+term+"	app.v:"+v+"	beta:"+beta+"	type:"+type);
			System.out.println("W##	term:"+term+"	app.v:"+v+"	beta:"+beta+"	type:"+type+"	subs1:"+subs1);
			System.out.println("W##	term:"+term+"	app.v:"+v+"	beta:"+beta+"	type:"+type+"	subs1:"+subs1+"	subs2:"+subs2);
			System.out.println("W##	term:"+term+"	app.v:"+v+"	beta:"+beta+"	type:"+type+"	subs1:"+subs1+"	subs2:"+subs2+"	t1:"+t1);
			System.out.println("W##	term:"+term+"	app.v:"+v+"	beta:"+beta+"	type:"+type+"	subs1:"+subs1+"	subs2:"+subs2+"	t1:"+t1+"	t2:"+t2);
			app.setType(type);
			System.out.println("W##	term:"+term+"	app.v:"+v+"	app.retType:"+type);
			return type;
		}else if(term instanceof TyAbst){
			TyAbst abst = (TyAbst)term;
			Type beta = Kind.makeNewKind();
			if(idTypeMap.containsKey(abst.getName())) throw new IllegalArgumentException("The var has already been contained!");
			idTypeMap.put(abst.getName(), beta);
			Type t1 = W(idTypeMap, abst.getChild(), subs);
			Type type = new Fun(substitute(beta, subs), t1);
			abst.setType(type);
			System.out.println("W##	term:"+term+"	abst.retType:"+type);
			return type;
		}
		throw new IllegalArgumentException("No match class for the term:"+term.getClass());
	}
	
	private static void substituteMap(Map<Integer, Type> idTypeMap, Map<Kind, Type> subs){
		for(int id : idTypeMap.keySet()){
			idTypeMap.put(id, substitute(idTypeMap.get(id), subs));
		}
	}
	
	private static Type substitute(Type type, Map<Kind, Type> subs){
		if(type instanceof Kind){
			return subs.containsKey(type) ? substitute(subs.get(type), subs) : type;
		}else if(type instanceof Fun){
			Fun fun = (Fun)type;
			return new Fun(substitute(fun.getLeft(), subs), substitute(fun.getRight(), subs));
		}
		throw new IllegalArgumentException("O should have been replaced!");
	}


	private static Map<Kind, Type> unify(Type t1, Type t2){
		Map<Kind, Type> kindMap = new HashMap<>();
		unifyOne(t1, t2, kindMap);
		return kindMap;
	}
	
	public static void unifyOne(Type tl, Type tr, Map<Kind, Type> kindMap){
		tl = fixType(tl, kindMap);
		tr = fixType(tr, kindMap);
		if(tl instanceof Fun){
			Fun fl = (Fun)tl;
			if(tr instanceof Fun){
				Fun fr = (Fun)tr;
				unifyOne(fl.getLeft(), fr.getLeft(), kindMap);
				unifyOne(fl.getRight(), fr.getRight(), kindMap);
			}else if(tr instanceof Kind){//すなわちtr instanceof Kind
				Kind kr = (Kind)tr;
				if(kindMap.containsKey(kr)) throw new IllegalArgumentException("already have!");
				kindMap.put(kr, tl);
			}else{
				throw new IllegalArgumentException("no match class for tr!");
			}
		}else if(tl instanceof Kind){
			Kind kl = (Kind)tl;
			if(tr instanceof Kind){
				Kind kr = (Kind)tr;
				if(kl.getId() < kr.getId()) kindMap.put(kr, tl);
			}else
				kindMap.put(kl, tr);
		}else{
			System.err.println("unifyOne	tl:"+tl);
			throw new IllegalArgumentException("no match class for tl!	tl.class:"+tl.getClass());
		}
	}
	
	/**
	 * 型tがKindなら、UFからrepresentativeにして返す
	 * @param t
	 * @param uf
	 * @return
	 */
	private static Type fixType(Type t, Map<Kind, Type> kindMap){
		if(kindMap.containsKey(t)) return kindMap.get(t);
		else return t;
	}

	private static void substituteAll(TyTerm term, Map<Kind, Type> kindMap){
		boolean flag = term instanceof TyVar;
		if(flag) System.out.println("substituteAll	beforeType:"+term.getType()+"	term:"+term);
		term.setType(substitute(term.getType(), kindMap));
		if(flag) System.out.println("substituteAll	afterType:"+term.getType()+"	term:"+term);
		if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			substituteAll(app.getLeftChild(), kindMap);
			substituteAll(app.getRightChild(), kindMap);
		}else if(term instanceof TyAbst){
			substituteAll(((TyAbst)term).getChild(), kindMap);
		}
	}

}
