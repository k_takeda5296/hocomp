package component.tyterm.type;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class O implements Type{

	private O(){}
	
	public static final O O = new O();

	public static O getO(){
		return O;
	}
	
	@Override
	public boolean equals(Object obj){
		return obj==O;
	}
	
	@Override
	public int hashCode(){
		return -1;
	}
	
	@Override
	public String toString(){
		return "O";
	}

	@Override
	public void getBits(List<Boolean> typeBits) {
		typeBits.add(false);
	}

	@Override
	public List<Boolean> getBits() {
		List<Boolean> bits = new ArrayList<Boolean>();
		getBits(bits);
		return bits;
	}

	@Override
	public boolean isHO() {
		return false;
	}

	@Override
	public int order() {
		return 0;
	}

	@Override
	public List<Type> toTypeList() {
		List<Type> typeList = new ArrayList<Type>();
		toTypeList(typeList);
		return typeList;
	}

	@Override
	public void toTypeList(List<Type> typeList) {
		typeList.add(O);
	}
	
	@Override
	public List<Type> toArgTypeList(Type retType) {
		throw new IllegalArgumentException("cannot have args!");
	}

	@Override
	public void toArgTypeList(List<Type> typeList, Type retType) {
		typeList.add(O);
	}

	@Override
	public boolean endWith(Type type) {
		return type==O;
	}

	@Override
	public int rightLength() {
		return 0;
	}

	@Override
	public void obtainByteCode(List<Byte> bytes) {
		bytes.add((byte) 1);
	}

	@Override
	public void obtainAllKinds(Map<Integer, Integer> kinds) {}
}
