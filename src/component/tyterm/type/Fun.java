package component.tyterm.type;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import util.MyUtil;

public final class Fun implements Type{

	private Type left_;
	private Type right_;
	
	public Fun(Type left, Type right){
		left_ = left;
		right_ = right;
	}
	
	public Type getLeft(){
		return left_;
	}
	
	public Type getRight(){
		return right_;
	}
	
	@Override
	public boolean equals(Object obj){
		if(!(obj instanceof Fun)) return false;
		Fun fun = (Fun)obj;
		return this.left_.equals(fun.left_) && this.right_.equals(fun.right_);
	}
	
	@Override
	public int hashCode(){
		return MyUtil.makeHashCode(new Object[]{ left_, right_ });
	}
	
	@Override
	public String toString(){
		boolean isOLeft = left_ instanceof O;
		boolean isORight = right_ instanceof O;
		String left = isOLeft ? left_.toString() : "("+left_.toString()+")";
		String right = isORight ? right_.toString() : "("+right_.toString()+")";
		return left+" -> "+right;
	}

	@Override
	public void getBits(List<Boolean> typeBits) {
		typeBits.add(true);
		left_.getBits(typeBits);
		right_.getBits(typeBits);
	}
	
	@Override
	public List<Boolean> getBits() {
		List<Boolean> bits = new ArrayList<Boolean>();
		getBits(bits);
		return bits;
	}

	@Override
	public boolean isHO() {
		return this.order() > 1;
	}

	@Override
	public int order() {
		return Math.max(left_.order()+1, right_.order());
	}

	@Override
	public List<Type> toTypeList() {
		List<Type> typeList = new ArrayList<Type>();
		toTypeList(typeList);
		return typeList;
	}

	@Override
	public void toTypeList(List<Type> typeList) {
		typeList.add(left_);
		right_.toTypeList(typeList);
	}
	@Override
	public List<Type> toArgTypeList(Type retType) {
		List<Type> typeList = new ArrayList<Type>();
		toArgTypeList(typeList, retType);
		return typeList;
	}

	@Override
	public void toArgTypeList(List<Type> typeList, Type retType) {
		typeList.add(left_);
		if(right_.equals(retType)) return;//重複してチェックしているので効率が悪い（型のサイズが小さいので問題にはならない？）
		right_.toArgTypeList(typeList, retType);
	}

	@Override
	public boolean endWith(Type type) {
		int gap = this.rightLength() - type.rightLength();
		return endWith(this, type, gap);
	}
	
	private static boolean endWith(Type type0, Type type1, int gap){
		if(gap < 0) return false;
		if(gap==0) return type0.equals(type1);
		if(!(type0 instanceof Fun)) return false;
		return endWith(((Fun)type0).getRight(), type1, gap-1);
	}

	@Override
	public int rightLength() {
		return 1 + right_.rightLength();
	}

	@Override
	public void obtainByteCode(List<Byte> bytes){
		bytes.add((byte)2);
		left_.obtainByteCode(bytes);
		right_.obtainByteCode(bytes);
	}

	@Override
	public void obtainAllKinds(Map<Integer, Integer> kinds) {
		left_.obtainAllKinds(kinds);
		right_.obtainAllKinds(kinds);
	}
}
