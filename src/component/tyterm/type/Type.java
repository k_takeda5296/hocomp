package component.tyterm.type;

import java.util.List;
import java.util.Map;

public interface Type {

	List<Boolean> getBits();
	void getBits(List<Boolean> typeBits);
	boolean isHO();
	int order();
	List<Type> toTypeList();
	List<Type> toArgTypeList(Type retType);
	void toTypeList(List<Type> typeList);
	void toArgTypeList(List<Type> typeList, Type retType);
	boolean endWith(Type type);
	int rightLength();
	void obtainByteCode(List<Byte> bytes);
	void obtainAllKinds(Map<Integer, Integer> kinds);
}
