package component.tyterm.type;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 0以上のidを持つ型
 * @author koutaro_t
 *
 */
public class Kind implements Type{
	
	private int id_;
	
	private Kind(int id){
		if(id < 0) throw new IllegalArgumentException("id shoud be non negative! id="+id);
		id_ = id;
	}
	
	private final static Map<Integer, Kind> KINDS = new HashMap<>();
	public static Kind makeNewKind(){
		int id = KINDS.size();
		if(!KINDS.containsKey(id)) KINDS.put(id, new Kind(id));
		return KINDS.get(id);
	}
	
	public static Kind getKind(int id){
		if(!KINDS.containsKey(id)) throw new IllegalArgumentException("No kind for the id!");
		return KINDS.get(id);
	}
	
	public int getId(){
		return id_;
	}

	@Override
	public List<Boolean> getBits() {
		throw new IllegalArgumentException("not supported!");
	}

	@Override
	public void getBits(List<Boolean> typeBits) {
		throw new IllegalArgumentException("not supported!");
	}

	@Override
	public boolean isHO() {
		return false;
	}

	@Override
	public int order() {
		return 0;
	}

	@Override
	public List<Type> toTypeList() {
		throw new IllegalArgumentException("not supported!");
	}

	@Override
	public void toTypeList(List<Type> typeList) {
		typeList.add(this);
	}
	
	@Override
	public List<Type> toArgTypeList(Type retType) {
		throw new IllegalArgumentException("cannot have args!");
	}

	@Override
	public void toArgTypeList(List<Type> typeList, Type retType) {
		typeList.add(this);
	}

	@Override
	public boolean endWith(Type type) {
		if(!(type instanceof Kind)) return false;
		Kind kind = (Kind)type;
		return this.id_==kind.id_;
	}

	@Override
	public int rightLength() {
		return 0;
	}

	@Override
	public void obtainByteCode(List<Byte> bytes) {
		bytes.add((byte)3);
		int fixedId = id_ + 4;
		bytes.add((byte)(fixedId >> 24));
		bytes.add((byte)(fixedId >> 16));
		bytes.add((byte)(fixedId >> 8));
		bytes.add((byte)(fixedId >> 0));
	}

	@Override
	public String toString(){
		return id_+"";
	}
	
	@Override
	public void obtainAllKinds(Map<Integer, Integer> kinds) {
		if(kinds.containsKey(id_)) kinds.put(id_, kinds.get(id_)+1);
		else kinds.put(id_, 1);
	}
}
