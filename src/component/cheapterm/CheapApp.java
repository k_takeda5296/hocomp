package component.cheapterm;

import java.util.List;


public class CheapApp implements CheapTerm{

	private CheapTerm leftChild_;
	private CheapTerm rightChild_;
	
	public CheapApp(CheapTerm leftChild, CheapTerm rightChild){
		leftChild_ = leftChild;
		rightChild_ = rightChild;
	}
	
	public CheapTerm getLeftChild(){
		return leftChild_;
	}
	
	public CheapTerm getRightChild(){
		return rightChild_;
	}

	@Override
	public CheapTerm copy() {
		CheapTerm left = leftChild_.copy();
		CheapTerm right = rightChild_.copy();
		return new CheapApp(left, right);
	}

	@Override
	public boolean isSame(CheapTerm term) {
		if(term==this) return true;
		if(!(term instanceof CheapApp)) return false;
		CheapApp app = (CheapApp)term;
		return this.leftChild_.isSame(app.leftChild_) && this.rightChild_.isSame(app.rightChild_);
	}
	
	@Override
	public String toString(){
		String str = "";
		boolean isAbstLeft = leftChild_ instanceof CheapAbst;
		str += isAbstLeft ? "(" : "";
		str += leftChild_.toString();
		str += isAbstLeft ? ")" : "";
		str += " ";
		boolean isIdRight = rightChild_ instanceof CheapVar || rightChild_ instanceof CheapSymbol;
		str += isIdRight ? "" : "(";
		str += rightChild_.toString();
		str += isIdRight ? "" : ")";
		return str;
	}
	
	@Override
	public void splitTerm(List<CheapTerm> terms) {
		leftChild_.splitTerm(terms);
		terms.add(rightChild_);
	}

	@Override
	public String getName() {
		return "@";
	}

	@Override
	public int nodeSize() {
		return 1 + leftChild_.nodeSize() + rightChild_.nodeSize();
	}
}
