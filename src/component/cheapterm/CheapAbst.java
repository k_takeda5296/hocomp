package component.cheapterm;

import java.util.List;

public class CheapAbst implements CheapTerm{

	private String name_;
	private CheapTerm child_;
	
	public CheapAbst(String name, CheapTerm child){
		name_ = name;
		child_ = child;
	}
	
	public String getName(){
		return name_;
	}
	
	public CheapTerm getChild(){
		return child_;
	}

	@Override
	public CheapTerm copy() {
		CheapTerm child = child_.copy();
		return new CheapAbst(name_, child);
	}

	@Override
	public boolean isSame(CheapTerm term) {
		if(term==this) return true;
		if(!(term instanceof CheapAbst)) return false;
		CheapAbst abst = (CheapAbst)term;
		return this.name_.equals(abst.name_) && this.child_.isSame(abst.child_);
	}
	
	@Override
	public String toString(){
		return "��"+name_+"."+child_.toString();
	}

	@Override
	public void splitTerm(List<CheapTerm> terms) {
		terms.add(this);
	}

	@Override
	public int nodeSize() {
		return 1 + child_.nodeSize();
	}
}
