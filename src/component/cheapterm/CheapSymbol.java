package component.cheapterm;

import java.util.List;

public class CheapSymbol implements CheapTerm{

	private String name_;

	public CheapSymbol(String name){
		name_ = name;
	}
	
	public String getName(){
		return name_;
	}

	@Override
	public CheapTerm copy() {
		return new CheapSymbol(name_);
	}

	@Override
	public boolean isSame(CheapTerm term) {
		if(term==this) return true;
		if(!(term instanceof CheapSymbol)) return false;
		CheapSymbol symbol = (CheapSymbol)term;
		return symbol.name_.equals(symbol.name_);
	}
	
	@Override
	public String toString(){
		return name_;
	}

	@Override
	public void splitTerm(List<CheapTerm> terms) {
		terms.add(this);
	}

	@Override
	public int nodeSize() {
		return 1;
	}
}
