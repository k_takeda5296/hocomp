package component.cheapterm;

import java.util.List;

public class CheapVar implements CheapTerm{

	private String name_;
	
	public CheapVar(String name){
		name_ = name;
	}
	
	public String getName(){
		return name_;
	}

	@Override
	public CheapTerm copy() {
		return new CheapVar(name_);
	}
	
	@Override
	public boolean isSame(CheapTerm term) {
		if(term==this) return true;
		if(!(term instanceof CheapVar)) return false;
		CheapVar var = (CheapVar)term;
		return var.name_.equals(var.name_);
	}
	
	@Override
	public String toString(){
		return name_;
	}

	@Override
	public void splitTerm(List<CheapTerm> terms) {
		terms.add(this);
	}

	@Override
	public int nodeSize() {
		return 1;
	}
}
