package component.cheapterm;

import java.util.List;

public interface CheapTerm {
	
	public CheapTerm copy();
	public boolean isSame(CheapTerm term);
	public void splitTerm(List<CheapTerm> terms);
	public String getName();
	public int nodeSize();
}
