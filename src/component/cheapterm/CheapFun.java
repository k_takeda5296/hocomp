package component.cheapterm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CheapFun implements CheapTerm{

	private String arg_;
	private CheapTerm body_;
	private Map<String, CheapTerm> env_;

	public CheapFun(String arg, CheapTerm body, Map<String, CheapTerm> env){
		arg_ = arg;
		body_ = body;
		env_ = new HashMap<String, CheapTerm>(env);
	}

	public String getArg(){
		return arg_;
	}

	public CheapTerm getBody(){
		return body_;
	}

	public Map<String, CheapTerm> getEnv(){
		return env_;
	}

	@Override
	public CheapTerm copy() {
		throw new IllegalArgumentException("not supported!!!");
	}

	@Override
	public boolean isSame(CheapTerm term) {
		if(!(term instanceof CheapFun)) return false;
		CheapFun fun  =(CheapFun)term;
		return this.arg_.equals(fun.arg_) && this.env_.equals(fun.env_) && this.body_.equals(fun.body_);
	}

	@Override
	public void splitTerm(List<CheapTerm> terms) {
		terms.add(this);
	}

	@Override
	public String getName() {
		return arg_;
	}

	@Override
	public int nodeSize() {
		return 1 + body_.nodeSize();
	}
	
	@Override
	public String toString(){
		return "fun " + arg_ + " -> " + body_;
	}
}
