package main;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.WeakHashMap;

import net.sourceforge.sizeof.SizeOf;

import com.javamex.classmexer.MemoryUtil;
import com.javamex.classmexer.MemoryUtil.VisibilityFilter;

import component.context.Context;
import component.occurrenceset.OccurrenceSet;
import component.tyterm.TyTermIterFunc;
import component.tyterm.term.TyAbst;
import component.tyterm.term.TyApp;
import component.tyterm.term.TyRoot;
import component.tyterm.term.TyTerm;
import component.tyterm.type.Type;
import compressor.converter.Pruner;
import compressor.loader.Loader;
import decompressor.decoder.ConsDecoder;
import decompressor.decoder.Decoder;
import decompressor.decoder.LabelDecoder;
import decompressor.decoder.TypeDecoder;



public final class Checker {

	public static void checkSize(Object obj){
		SizeOf.skipStaticField(true); // 初期化コード
		SizeOf.setMinSizeToLog(10); // 初期化コード
		long size0 = SizeOf.deepSizeOf(obj); // オブジェクトのサイズを取得
		System.out.println("size0:	" + size0);

		System.out.println("deepSize.all:		" + MemoryUtil.deepMemoryUsageOf(obj, VisibilityFilter.ALL));
		System.out.println("deepSize.non_public:	" + MemoryUtil.deepMemoryUsageOf(obj, VisibilityFilter.NON_PUBLIC));
		System.out.println("deepSize.none:		" + MemoryUtil.deepMemoryUsageOf(obj, VisibilityFilter.NONE));
		System.out.println("deepSize.private_only:	" + MemoryUtil.deepMemoryUsageOf(obj, VisibilityFilter.PRIVATE_ONLY));
		System.out.println("size:			" + MemoryUtil.memoryUsageOf(obj));
		System.out.println();
	}

	public static long MAX_MEMORY = 0;
	public static List<Integer> intList = new ArrayList<Integer>();
	private static int TIMES = 0;
	public static void updateMaxMemory(){
		System.gc();System.gc();System.gc();
		long total = Runtime.getRuntime().totalMemory();
		long free = Runtime.getRuntime().freeMemory();
		long used = total - free;
		boolean flag = used > MAX_MEMORY;
		MAX_MEMORY = flag ? used : MAX_MEMORY;
		if(flag) intList.add(TIMES);
		TIMES++;
	}

	public static boolean validTyTermCheck(TyRoot term){
		String b = term.checkBidrectional();
		boolean isBidirectional = b.equals("");
		//System.out.println("Checker#validerTyTermChecker:	isBidirectional:"+isBidirectional);
		if(!isBidirectional) return false;
		boolean isWellBinded = term.getChild().isWellBinded();
		//System.out.println("Checker#validerTyTermChecker:	isWellBinded:"+isWellBinded);
		if(!isWellBinded) return false;
		boolean isWellTyped = term.getChild().isWellTyped();
		//System.out.println("Checker#validerTyTermChecker:	isWellTyped:"+isWellTyped);
		return isWellTyped;
	}

	public static boolean termReduceCheck(TyRoot term, Loader loader){
		TyRoot loadedTerm = loader.parsedTerm();
		return term.getChild().isSame(loadedTerm.getChild());
	}

	public static boolean termDecodeCheck(TyTerm term, String labelFile, String typeFile, String consFile, boolean typeClassifying){
		TyRoot decodedTerm = Decoder.decodedTerm(labelFile, typeFile, consFile, typeClassifying);
		return term.isSame(decodedTerm.getChild());
	}

	public static boolean consDecodeCheck(List<String> consList, String consFile){
		List<String> decodedList = ConsDecoder.decodedConsList(consFile);
		return decodedList.equals(consList);
	}

	public static boolean typeDecodeCheck(List<Type> typeList, String typeFile){
		List<Type> decodedList = TypeDecoder.decodedTypeList(typeFile);
		return decodedList.equals(typeList);
	}

	public static boolean intSeqCheck(List<Integer> intList, String file){
		List<Integer> decodedList = LabelDecoder.decodedYrcList(file);
		/*
		System.out.println("Checker#intSeqCheck	iList:"+intList);
		System.out.println("Checker#intSeqCheck	dList:"+decodedList);
		 */
		return decodedList.equals(intList);
	}
	
	public static void checkHasExpansionInPatterns(TyTerm term, boolean inPattern, int pBorder){
		if(term instanceof TyApp){
			TyApp app = (TyApp)term;
			if(inPattern){//pattern内のapp
				if(app.getLeftChild() instanceof TyAbst){
					TyAbst abst = (TyAbst)app.getLeftChild();
					boolean isToBePruned = Pruner.isToBePruned(abst, pBorder);
					System.out.println("Checker#checkHasExpansionInPatterns	abst.isToBePruned:"+isToBePruned+"	abst.name:"+abst.idStr());
					throw new IllegalArgumentException();//アウト
				}
				checkHasExpansionInPatterns(app.getLeftChild(), true, pBorder);
				checkHasExpansionInPatterns(app.getRightChild(), true, pBorder);
			}else{//pattern外のapp
				if(app.getLeftChild() instanceof TyAbst){//右以下はパターン
					checkHasExpansionInPatterns(app.getLeftChild(), false, pBorder);
					checkHasExpansionInPatterns(app.getRightChild(), true, pBorder);
				}else{//body内部
					
				}
			}
		}else if(term instanceof TyAbst){
			checkHasExpansionInPatterns(((TyAbst)term).getChild(), inPattern, pBorder);
		}
	}


	public static void checkInRootPosterity(TyRoot root, Set<TyTerm> nodesToRemove){
		Set<TyTerm> posterity = root.getPosterity();
		posterity.retainAll(nodesToRemove);
		if(posterity.size() > 0) throw new IllegalArgumentException("term not removed:"+posterity.size());
		List<TyAbst> abstPost = root.getAllAbstChildren();
		abstPost.retainAll(nodesToRemove);
		if(abstPost.size() > 0) throw new IllegalArgumentException("abst not removed:"+abstPost.size());
	}

	public static Map<TyTerm, Integer> checkTyTermGC(TyRoot root){
		final Map<TyTerm, Integer> set = new WeakHashMap<TyTerm, Integer>();

		root.iterPostOreder(new TyTermIterFunc(){
			Integer index=0;
			@Override
			public void iterFunc(TyTerm term) {
				set.put(term, index++);
			}
		});
		return set;
	}

	public static Map<Context, Integer> checkContextGC(Set<Context> contexts){
		final Map<Context, Integer> set = new WeakHashMap<Context, Integer>();
		int index=0;
		for(Context c : contexts){
			set.put(c, index++);
		}
		return set;
	}


	public static Map<OccurrenceSet, Integer> checkOccListGC(Set<OccurrenceSet> occLists){
		final Map<OccurrenceSet, Integer> set = new WeakHashMap<OccurrenceSet, Integer>();
		int index=0;
		for(OccurrenceSet c : occLists){
			set.put(c, index++);
		}
		return set;
	}

	public static boolean checkB(TyTerm term, String name, String s0, String s1){
		String b = term.checkBidrectional();
		if(b.equals("")){
			return true;
		}else{
			System.out.println("YCompress#checkB:	preTerm:"+s0);
			System.out.println("YCompress#checkB:	newTerm:"+s1);
			System.out.println("YCompress#compress:	checkBidirectional:"+name);
			System.out.println(b);
			//return true;
			throw new IllegalArgumentException("ERROR!"+name);
		}
	}


	/*
	 * http://chat-messenger.net/blog-entry-49.html
	 */
	public static String getMemoryInfo() {
		DecimalFormat f1 = new DecimalFormat("#,###KB");
		DecimalFormat f2 = new DecimalFormat("##.#");
		long free = Runtime.getRuntime().freeMemory() / 1024;
		long total = Runtime.getRuntime().totalMemory() / 1024;
		long max = Runtime.getRuntime().maxMemory() / 1024;
		long used = total - free;
		double ratio = (used * 100 / (double)total);
		String info = 
				"Java メモリ情報 : 合計=" + f1.format(total) + "、" +
						"使用量=" + f1.format(used) + " (" + f2.format(ratio) + "%)、" +
						"使用可能最大="+f1.format(max);
		System.out.println("Checker#getMamoryInfo	:"+info);
		return info;
	}

}
