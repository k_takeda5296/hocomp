package main;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;

import util.DiffChecker;

import compressor.HOCompressor;
import compressor.HOCompressor.MODE;

import decompressor.HODecompressor;




public final class Main {

	public static final String BASE_DIR = "syuronnBench/";

	//圧縮するentextsのディレクトリ
	public static String ENTEXTS_DIR = BASE_DIR + "txt/en/cancer/cancer500";
	public static int ENTEXTS_NUM = 1 * 2;

	//圧縮するDNA(single)ファイル
	public static String DNA_TEXT_DIR = BASE_DIR + "txt/dna/";
	public static String DNA_TEXT_FILE = "TAIR10_1.ary";
	public static int DNA_NUM = 1 * 1 * 100;

	//圧縮するDNA(join)ファイル
	public static String DNA_JOIN_DIR = BASE_DIR + "xml/test/";
	public static String DNA_JOIN_FILE = "EXI-Array_Structural.xml";

	//圧縮するxmlファイル
	public static String XML_DIR = BASE_DIR + "xml/";
	public static String XML_FILE = "1998statistics.xml";

	//圧縮するプログラムファイル
	public final static String PROG_DIR = BASE_DIR + "prog/";
	public static String PROG_FILE = "progp.pas";

	/*
	 * オプション文字列
	 */
	private static final String OPT_FILE = HOCompressor.OPT_FILE;
	private static final String OPT_DNA_TEXT = HOCompressor.OPT_DNA_TEXT;
	private static final String OPT_EN_TEXT = HOCompressor.OPT_EN_TEXT;
	private static final String OPT_PROG_TEXT = HOCompressor.OPT_PROG_TEXT;
	private static final String OPT_CSIZE = HOCompressor.OPT_CSIZE;
	private static final String OPT_PBORDER = HOCompressor.OPT_PBORDER;
	private static final String OPT_LOG = HOCompressor.OPT_LOG;




	//Testerで扱うためpublic
	public static HOCompressor.MODE COMP_MODE = HOCompressor.MODE.XML;
	public static int CONTEXT_SIZE = 7;
	
	public static int PRUNE_BORDER = 0;

	public static boolean LOGGING = true;
	public static String LOG_FILE = (new SimpleDateFormat("yyyyMMddHHmmss")).format(Calendar.getInstance().getTime())+".log";


	public static final int ID = 6;


	private static HashMap<MODE, String[]> ARGS_MAP;
	@SuppressWarnings("serial")
	private static void init(){
		ARGS_MAP = new HashMap<MODE, String[]>(){{
			put(MODE.XML, new String[]{ OPT_FILE, XML_DIR+XML_FILE });
			put(MODE.DNA_JOIN, new String[]{ OPT_FILE, DNA_JOIN_DIR+DNA_JOIN_FILE });
			put(MODE.DNA_TEXT, new String[]{ OPT_FILE, DNA_TEXT_DIR+DNA_TEXT_FILE, OPT_DNA_TEXT, DNA_NUM+"" });
			put(MODE.EN_TEXT, new String[]{ OPT_FILE, ENTEXTS_DIR, OPT_EN_TEXT, ENTEXTS_NUM+"" });
			put(MODE.PROG_TEXT, new String[]{ OPT_FILE, PROG_DIR+PROG_FILE, OPT_PROG_TEXT });
		}};
	}
	private static String[] addCSizeOption(String[] args){
		String[] newArgs = new String[args.length + 2];
		System.arraycopy(args, 0, newArgs, 0, args.length);
		newArgs[newArgs.length - 2] = OPT_CSIZE;
		newArgs[newArgs.length - 1] = CONTEXT_SIZE+"";
		return newArgs;
	}
	private static String[] addPBOption(String[] args){
		String[] newArgs = new String[args.length + 2];
		System.arraycopy(args, 0, newArgs, 0, args.length);
		newArgs[newArgs.length - 2] = OPT_PBORDER;
		newArgs[newArgs.length - 1] = PRUNE_BORDER+"";
		return newArgs;
	}
	private static String[] addLogOption(String[] args){
		String[] newArgs = new String[args.length + 2];
		System.arraycopy(args, 0, newArgs, 0, args.length);
		newArgs[newArgs.length - 2] = OPT_LOG;
		newArgs[newArgs.length - 1] = LOG_FILE;
		return newArgs;
	}

	public static boolean SPECIFY_LOG = false;
	
	static int WAIT_MILLIS = 0 * 1000;
	static final boolean DUMMY = true;
	/*
	 * メイン関数
	 */
	public static void main(String[] args){
		sleepMillis(Main.WAIT_MILLIS);
		//if(Main.DUMMY) throw new IllegalArgumentException("DUMMY!");
		init();

		args = addCSizeOption(ARGS_MAP.get(COMP_MODE));
		args = addPBOption(args);
		if(SPECIFY_LOG) args = addLogOption(args);
		System.out.println(Arrays.toString(args));

		HOCompressor.main(args);

		if(false || true){
			HODecompressor.main(args);
			DiffChecker.main(args);
		}else{
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
		}
		System.out.println("		");
	}

	private static void sleepMillis(long millis){
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e1) {
			// TODO 自動生成された catch ブロック
			e1.printStackTrace();
		}
	}


}