package main;

import java.io.IOException;

import util.MyLogger;
import util.MyUtil;

import compressor.HOCompressor;
import compressor.HOCompressor.MODE;

public class Tester {

	private static final String BASE_DIR = "benchmark/";

	//圧縮するentextsのディレクトリ
	public static String ENTEXTS_DIR = BASE_DIR + "txt/en/cancer/cancer500";
	public static final int ENTEXTS_NUM = 50;

	//圧縮するDNA(single)ファイル
	public static String DNA_TEXT_DIR = BASE_DIR + "txt/dna/";
	public static String DNA_TEXT_FILE = "TAIR10_1.ary";
	public static int DNA_NUM = 1 * 40 * 1000;

	//圧縮するDNA(join)ファイル
	public static String DNA_JOIN_DIR = BASE_DIR + "xml/join20000/";
	public static String DNA_JOIN_FILE = "join20000_";

	private static int CSIZE = 7;
	private static int PBORDER = 0;
	private static final String LOG_SUFFIX = "_"+CSIZE+"_"+PBORDER+".log"; 

	private static int START = 1;
	private static int END = 8;


	private static String TYTERM_BASE_DIR = "ocaml/tyterm/";

	private static final MODE COMPRESS_MODE = MODE.DNA_TEXT;

	public static void main(String[] args){
		Main.CONTEXT_SIZE = CSIZE;
		Main.PRUNE_BORDER = PBORDER;
		Main.SPECIFY_LOG = true;
		Main.LOGGING = true;

		if(COMPRESS_MODE==MODE.XML) testXml();
		else if(COMPRESS_MODE==MODE.DNA_JOIN) testDNAJoin();
		else if(COMPRESS_MODE==MODE.DNA_TEXT) testDNAText();
		else if(COMPRESS_MODE==MODE.EN_TEXT) testEnText();
		else if(COMPRESS_MODE==MODE.PROG_TEXT) testProg();
	}

	private static void testDNAJoin(){
		Main.COMP_MODE = MODE.DNA_JOIN;
		Main.DNA_JOIN_DIR = DNA_JOIN_DIR;
		String logNameSuffix = MyUtil.getDate()+"_"+DNA_JOIN_FILE+START+"~"+END+LOG_SUFFIX;

		Main.LOG_FILE = "YAGUCHI_"+logNameSuffix;
		String tytermDir = TYTERM_BASE_DIR + DNA_JOIN_DIR;
		String scfgLogFile = MyLogger.getLogDir() + "SCFG_"+logNameSuffix;//実行中に日付をまたぐとバグるかも

		for(int i = START; i <= END; i++){
			String xmlFile = DNA_JOIN_FILE + i + ".xml";
			Main.DNA_JOIN_FILE = xmlFile;
			Main.main(null);

			String tytermFile = tytermDir+xmlFile+".tyterm";
			execSCFGRC(tytermFile, scfgLogFile);
		}
	}

	private static void testDNAText(){
		Main.COMP_MODE = MODE.DNA_TEXT;
		Main.DNA_TEXT_DIR = DNA_TEXT_DIR;
		Main.DNA_TEXT_FILE = DNA_TEXT_FILE;
		String logNameSuffix = MyUtil.getDate()+"_"+DNA_TEXT_FILE+"_"+START*DNA_NUM+"~"+END*DNA_NUM+"_"+LOG_SUFFIX;
		
		Main.LOG_FILE = "YAGUCHI_"+logNameSuffix;
		String tytermDir = TYTERM_BASE_DIR + DNA_TEXT_DIR;
		String scfgLogFile = MyLogger.getLogDir() + "SCFG_"+logNameSuffix;
		
		for(int i = START; i <= END; i++){
			int num = DNA_NUM * i;
			Main.DNA_NUM = num;
			Main.main(null);
			
			String tytermFile = tytermDir+DNA_TEXT_FILE+"_"+num+".xml.tyterm";
			execSCFGRC(tytermFile, scfgLogFile);
		}
	}

	private static void testEnText(){
		Main.COMP_MODE = MODE.EN_TEXT;
		Main.ENTEXTS_DIR = ENTEXTS_DIR;
		String dataset = ENTEXTS_DIR.split("/")[4];
		String logNameSuffix = MyUtil.getDate()+"_"+dataset+"_"+START*ENTEXTS_NUM+"~"+END*ENTEXTS_NUM+LOG_SUFFIX;
		
		Main.LOG_FILE = "YAGUCHI_"+logNameSuffix;
		String[] splitted = ENTEXTS_DIR.split("/");
		String tytermDir = TYTERM_BASE_DIR + splitted[0]+"/"+splitted[1]+"/"+splitted[2]+"/"+splitted[3]+"/";
		String scfgLogFile = MyLogger.getLogDir() + "SCFG_" + logNameSuffix;
		
		for(int i = START; i <= END; i++){
			int num = ENTEXTS_NUM * i;
			Main.ENTEXTS_NUM = num;
			Main.main(null);
			
			String tytermFile = tytermDir + splitted[4]+"_"+num+".txt"+(HOCompressor.SEQUENTIAL_XML_FOR_TEXTS ? ".seq" : "")+".xml.tyterm";
			execSCFGRC(tytermFile, scfgLogFile);
		}
	}

	static String[] PROGRAMS = {
		"cp.html",
		"fields.c",
		"grammar.lsp",
		"progc.c",
		"progl.lsp",
		"progp.pas"
	};
	private static void testProg(){
		Main.COMP_MODE = MODE.PROG_TEXT;
		String logNameSuffix = MyUtil.getDate()+"_"+"programs"+LOG_SUFFIX;
		
		Main.LOG_FILE = "YAGUCHI_"+logNameSuffix;
		String tytermDir = TYTERM_BASE_DIR + Main.PROG_DIR;
		String scfgLogFile = MyLogger.getLogDir() + "SCFG_" + logNameSuffix;
		for(String progFile : PROGRAMS){
			Main.PROG_FILE = progFile;
			Main.main(null);
			
			String tytermFile = tytermDir + progFile +(HOCompressor.SEQUENTIAL_XML_FOR_TEXTS ? ".seq" : "")+".xml.tyterm";
			execSCFGRC(tytermFile, scfgLogFile);
		}
	}
	static String[] XMLS = {
		"1998statistics.xml",
		"EXI-Array_Structural.xml",
		"EXI-factbook_Structural.xml",
		"EXI-GeogCoord_Structural.xml",
		"EXI-Invoice_Structural.xml",
		"EXI-Telecomp_Structural.xml",
		"EXI-weblog_Structural.xml"
	};
	private static void testXml(){
		Main.COMP_MODE = MODE.XML;
		String logNameSuffix = MyUtil.getDate()+"_XMLs"+LOG_SUFFIX;
		Main.LOG_FILE = "YAGUCHI_"+logNameSuffix;
		String tytermDir = TYTERM_BASE_DIR + Main.XML_DIR;
		String scfgLogFile = MyLogger.getLogDir() + "SCFG_" + logNameSuffix;
		for(String xmlFile : XMLS){
			Main.XML_FILE = xmlFile;
			Main.main(null);
			
			String tytermFile = tytermDir + xmlFile + ".tyterm";
			execSCFGRC(tytermFile, scfgLogFile);
		}
	}


	private static void execSCFGRC(String sourceFile, String logFile){
		String program = "ocaml/rangecoder";
		String cmd = program + " " + sourceFile + " " + logFile;
		System.out.println("Tester#execSCFGRC	cmd:"+cmd);
		Runtime r = Runtime.getRuntime();
		Process p;
		try {
			p = r.exec(cmd);
			p.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
