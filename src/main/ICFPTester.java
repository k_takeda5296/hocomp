package main;

import java.io.IOException;

import util.MyLogger;
import util.MyUtil;

import compressor.HOCompressor.MODE;

public class ICFPTester {

	private static final String BASE_DIR = "syuronnBench/all/";


	private static String TYTERM_BASE_DIR = "ocaml/tyterm/"+BASE_DIR;

	private static int CSIZE = 7;
	private static int PBORDER = 0;
	private static final String LOG_SUFFIX = "_"+CSIZE+"_"+PBORDER+".log"; 



	public static void main(String[] args){
		Main.CONTEXT_SIZE = CSIZE;
		Main.PRUNE_BORDER = PBORDER;
		Main.SPECIFY_LOG = true;
		Main.LOGGING = true;

		testSyuronn();
	}



	private static String[] XMLS = {

		"TAIR10_1.ary_3000.xml",
		"TAIR10_1.ary_200000.xml",
		"TAIR10_2.ary_200000.xml",
		"TAIR10_5.ary_200000.xml",
		
		"cancer500_5.txt.seq.xml",
		"cancer500_150.txt.seq.xml",
		"arabidopsis1500_150.txt.seq.xml",
		
		"cp.html.xml",
		"fields.c.xml",
		"grammar.lsp.xml",
		
		"EXI-Array_Structural.xml",
		"EXI-Telecomp_Structural.xml",
		"EXI-factbook_Structural.xml",
		"EXI-Invoice_Structural.xml",
		"EXI-weblog_Structural.xml",
		"1998statistics.xml"
	};

	private static void testSyuronn(){
		Main.COMP_MODE = MODE.XML;
		String logNameSuffix = MyUtil.getDate()+"_syuronnBench_"+CSIZE+".log";
		Main.LOG_FILE = "YAGUCHI_TROMP_"+logNameSuffix;
		String tytermDir = TYTERM_BASE_DIR + Main.XML_DIR;
		String scfgLogFile = MyLogger.getLogDir() + "SCFG_" + logNameSuffix;
		for(String xmlFile : XMLS){
			Main.XML_DIR = BASE_DIR;
			Main.XML_FILE = xmlFile;
			Main.main(null);

			String tytermFile = tytermDir + xmlFile + ".tyterm";
			execSCFGRC(tytermFile, scfgLogFile);
		}
	}


	private static void execSCFGRC(String sourceFile, String logFile){
		String program = "../rangecoder/rangecoder";
		String cmd = program + " " + sourceFile + " " + logFile;
		System.out.println("Tester#execSCFGRC	cmd:"+cmd);
		Runtime r = Runtime.getRuntime();
		Process p;
		try {
			p = r.exec(cmd);
			p.waitFor();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
